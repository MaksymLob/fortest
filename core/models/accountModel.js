
const Redis = __app('services/redis');
const redis = new Redis().connection;

const accountKeyModel = __app('models/accountKeyModel');

const AccountModel = {
  classMethods: {
    checkBalance({currency: currency, clientId: clientId, amount: amount}) {
      return Promise.all([
        accountKeyModel.findOne({where: {clientId: clientId, currency: currency}}),
        this.findByIdR(currency, clientId)
      ]).then(res => {
        if (Number(res[0].get('balance')) - Number(res[1].used) < amount) {
          return Promise.reject('VALIDATION_INSUFFICIENT_FUNDS');
        }
        return true;
      });
    },

    getClientAccounts(clientId) {
      return new Promise((resolve, reject) => {
        let accounts = {};
        redis.keys('clients:id:' + clientId + ':currency:*', (err, res) => {
          if(err){
            return reject(err);
          }
          if(res.length==0){
            return reject('NOT_FOUND_ACCOUNTS');
          }
          const multi = redis.multi();
          for (let i = 0; i < res.length; i++) {
            multi.hgetall(res[i]);
          }
          multi.exec((err, res) => {
            if(err){
              return reject(err);
            }
            for (let i = 0; i < res.length; i++) {
              accounts[res[i].currency] = {balance: Number(res[i].balance)};
            }
            return resolve(accounts);
          });
        });
      });
    }
  }

};

const accountSchema = __app('models/schemas/account');
const Model = __app('models/model');
const schema = new Model(accountSchema.schema);
AccountModel.validations = accountSchema.validations;
const accountModel = schema.defineDataBase('redis', 'clients:id:', AccountModel);

module.exports = accountModel;
