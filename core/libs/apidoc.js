
// let Schemas = __app('libs/schemas');

const fs = require('fs');
const exec = require('child_process').exec;
const errorCodes = __app('libs/errorCodes');

class DocsGenerator {
  constructor(routes) {
    this.toSave = '';
    // Methods
    for (let key in routes) {
      if (this.addMethod(key, routes[key].name)) {
        this.addGroup(routes[key].group);
        this.addDescription(routes[key].description);
        this.addVersion(routes[key].version);
        this.addPermission(routes[key].access);
        this.addParams(routes[key].params);
        this.addApiUse(routes[key].apiuse);
        this.addSuccess(routes[key].success);
        this.addError(routes[key].error);
        this.toSave += '*\/\n';
      }
    }
    // Models
    /* let shem = new Schemas();
    let tables = shem.getAllShemas();
    tables.forEach((table=>{
        if(this.addMethod("Model:/-"+table.table,table.table)){
            delete table.table;
            this.addGroup("_Models");
            this.addParams(table,true);
            this.toSave+='*\/\n';
        }
    })) */
    // errorCodes
    this.addMethod('System:/Error Codes');
    this.addGroup('System');
    this.addVersion();
    let params = {};
    for (let code in errorCodes) {
      params[code] = {type: errorCodes[code].errorCode, description: [errorCodes[code].errorMessage]};
    }
    this.addParams(params);
    this.toSave += '*\/\n';
    fs.writeFile(__dirroot + 'apidoc/DOCS/docs.js', this.toSave, () => {
      this.Generate();
    });
  }

  addMethod(name, comment) {
    comment = comment || '';
    let temp = '@api {';
    let i = name.indexOf('\/');
    if (i < 0) {
      return false;
    }
    let method = name.substr(0, i - 1) || 'socket.io';

    temp += method + '} ' + name.substr(i + 1);
    temp += ' ' + comment;
    temp += '\n';
    this.toSave += '\/**\n' + temp;
    return true;
  }

  addGroup(group) {
    group = group || 'Unknown';
    this.toSave += '@apiGroup ' + group + '\n';
  }

  addDescription(description) {
    if (description) {
      this.toSave += '@apiDescription ' + description + '\n';
    }
  }

  addVersion(version) {
    version = version || '1.0.0';
    this.toSave += '@apiVersion ' + version + '\n';
  }

  addPermission(perm) {
    let isFirst = true;
    let temp = '@apiPermission ';
    perm = perm || [];

    for (let i = 0; i < perm.length; i++) {
      if (!isFirst) {
        temp += ', ';
      }
      temp += perm[i];
      isFirst = false;
    }
    this.toSave += temp + '\n';
  }

  addParams(params, isModule) {
    params = params || {};
    let temp = '';
    for (let key in params) {
      if (isModule) {
        params[key].description = this.getModuleDescription(params[key]);
      }
      temp += '@apiParam ' + '{' + (params[key].type || '') + '} ' + key + ' ' + (params[key].description || '') + '\n';
    }
    this.toSave += temp;
  }

  addApiUse(api) {
    api = api || [];
    let temp = '';
    for (let i = 0; i < api.length; i++) {
      temp += '@apiUse ' + api[i] + '\n';
    }
    this.toSave += temp;
  }

  addSuccess(params) {
    params = params || {};
    let temp = '';
    for (let key in params) {
      temp += '@apiSuccess ' + '{' + (params[key].type || '') + '} ' + key + ' ' + (params[key].description || '') + '\n';
    }
    this.toSave += temp;
  }

  addError(params) {
    params = params || {};
    let temp = '';
    for (let key in params) {
      temp += '@apiError ' + '{' + (params[key].type || '') + '} ' + key + ' ' + (params[key].description || '') + '\n';
    }
    this.toSave += temp;
  }

  getModuleDescription(param) {
    let desc = '';
    let isFirst = true;
    for (let key in param) {
      if (key == 'type' || key == 'validate' || key == 'additionalKey') {
        continue;
      }
      if (isFirst) {
        isFirst = false;
      } else {
        desc += ', ';
      }
      desc += key + ': ' + param[key];
    }
    return desc;
  }

  Generate() {
    exec('sh ' + __dirroot + 'apidoc.bash',
      (error, stdout, stderr) => {
        if (error !== null) {
          console.warn('Docs generation error', error);
          return;
        }
        console.info('Docs generation done', '');
      });
  }
}

module.exports = routes => {
  return new DocsGenerator(routes);
};
