const config = require(__dirroot + 'config');

const RestMiddleware = __app('routes/middlewaresRest');
const rm = new RestMiddleware();
const SocketMiddleware = __app('routes/middlewaresSocket');
const sm = new SocketMiddleware();

const express = require('express');
const helmet = require('helmet');
const routerR = express();
const routerS = require('socket.io-events')();

const bodyParser = require('body-parser');

// ****************** REST middlewares **********************
routerR.use(express.static('views'));
routerR.use('/v1.0/docs', express.static(__dirroot + 'apidoc'));

routerR.use(bodyParser.json());
routerR.use(bodyParser.urlencoded({extended: true}));
routerR.use(helmet());

routerR.use((req, res, next) => {
  rm.setCommonMiddlewares(req, res, next);
});
routerR.use((req, res, next) => {
  rm.existMethod(req, res, next);
});
routerR.use((req, res, next) => {
  rm.checkToken(req, res, next);
});
routerR.use((req, res, next) => {
  rm.checkPermissions(req, res, next);
});
routerR.use((req, res, next) => {
  rm.callController(req, res, next);
});

// ***************** SOCKET middlewares *********************
routerS.use((socket, args, next) => {
  sm.setCommonMiddlewares(socket, args, next);
});
routerS.use((socket, args, next) => {
  sm.existMethod(args, socket, next);
});
routerS.use((socket, args, next) => {
  sm.checkPermissions(args, socket, next);
});
routerS.use((socket, args, next) => {
  sm.callController(args, socket, next);
});

// ********************** SERVER ****************************
const server = require('http').Server(routerR);
const socket = require('socket.io')(server);

socket.on('connection', sm.connectionHandler);
socket.use((socket, next) => {
  sm.checkToken(socket, next);
});
socket.use(routerS);

server.listen(config.appPort, err => {
  if (err) {
    return log.e('Server can\'t be created on port ' + config.appPort);
  }
  log.i('Server created on port ' + config.appPort);
});

sm.setProjectListeners(socket);

module.exports = server;
