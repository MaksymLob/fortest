const fs = require('fs');
const core_path = __dirroot + '/core/';
const custom_path = __dirroot + '/custom/';

module.exports = {
  gen(location) {
    try {
      return require(custom_path + location);
    } catch (err) {
      return require(core_path + location);
    }
  },
  locate(location) {
    if (fs.existsSync(custom_path + location)) {
      return custom_path + location;
    } else {
      return core_path + location;
    }
  }
};
