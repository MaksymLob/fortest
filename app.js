global.__dirroot = __dirname + '/';

let gen = require(__dirroot + 'core/libs/path').gen;
global.__app = gen;

const config = require(__dirroot + 'config');
global.log = __app('libs/logger');

const Redis = __app('services/redis');
const redis = new Redis(config.connections.REDIS);
redis.connect();

const Mysql = __app('services/mysql');
const mysql = new Mysql(config.connections.MYSQL);
mysql.connect();

const CryptoOrder = __app('services/exchange/cryptoOrders');
const cryptoOrder = new CryptoOrder();
cryptoOrder.init();

const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();

require(__dirroot + 'server');

process.on('uncaughtException', err => {
  log.e('uncaughtException', err);
  if (err.message.indexOf('EMFILE') != -1 || err.message.indexOf('EADDRINUSE') != -1) {
    process.exit(1);
  }
});
