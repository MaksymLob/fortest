/**
@api {socket.io} v1.0/2fa/deactivate 2FA deactivate
@apiGroup AUTH
@apiDescription deactivate2FA
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID unique id of request to identify response
@apiParam {String} code VALIDATION_2FA_CODE
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/2fa/login 2FA login
@apiGroup AUTH
@apiDescription Login by 2FA
@apiVersion 1.0.0
@apiPermission isPartAuth
@apiParam {String} extID unique id of request to identify response
@apiParam {String} code VALIDATION_2FA_CODE
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/2fa/confirm_activate 2FA ConfirmActivate
@apiGroup AUTH
@apiDescription Confirm Activate 2FA
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID unique id of request to identify response
@apiParam {String} code VALIDATION_2FA_CODE
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/2fa/activate 2FA Activate
@apiGroup AUTH
@apiDescription Request for 2FA activation
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID unique id of request to identify response
@apiParam {String} password User password
@apiParam {String} hash Hash user
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/resetPassword ResetPassword
@apiGroup AUTH
@apiDescription ResetPassword. Reset password by token from recovery email
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID unique id of request to identify response
@apiParam {String} password User password
@apiParam {String} hash Hash user
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/forgotPassword ForgotPassword
@apiGroup AUTH
@apiDescription ForgotPassword. Send email with recovery password
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID unique id of request to identify response
@apiParam {String} email User e-mail
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/logout Logout user
@apiGroup AUTH
@apiDescription Logout user
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID unique id of request to identify response
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/login Login user
@apiGroup AUTH
@apiDescription Login user. The first method if user hasn`t X-Token, return info about user with all accounts and updated X-Token. After this method you can receive stream of private user`s information: /balance/me/update, /orders/me/update, /trades/me/update, and public stream of quotes: /quotes/update ATTENTION: This method return success=true, if user has enabled 2FA (Complete login by sending /v1.0/2fa/login). After /v1.0/login user with 2FA has NO access to private methods.
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID unique id of request to identify response
@apiParam {String} email REQUIRED
@apiParam {String} password REQUIRED
@apiParam {String} recaptcha REQUIRED
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {String} data.extID 
@apiSuccess {String} data.X-Token 
@apiSuccess {String} data.email 
@apiSuccess {Object} data.accounts 
@apiSuccess {Object} data.accounts.BTC 
@apiSuccess {Number} data.accounts.BTC.balance 
@apiSuccess {Object} data.accounts.USD 
@apiSuccess {Number} data.accounts.USD.balance 
@apiSuccess {Object} data.accounts.ETH 
@apiSuccess {Number} data.accounts.ETH.balance 
@apiSuccess {String} data.success 
*/
/**
@api {POS} v1.0/crm/email/send Send email to user through platform
@apiGroup CRM
@apiDescription Send email to user through platform
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} text text for client
@apiParam {String} to Email to send
@apiParam {String} file File to send
@apiParam {String} subject Subject email
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/currencies Get list of all platforms currencies
@apiGroup CRM
@apiDescription Get list of all platforms currencies
@apiVersion 1.0.0
@apiPermission isCrm
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/withdrawals/approve Approve withdrawal(for crm)
@apiGroup CRM
@apiDescription Approve withdrawal(for crm)
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} id id  of withdrawal
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/orders Get all closed orders(for crm)
@apiGroup CRM
@apiDescription Get all closed orders(for crm)
@apiVersion 1.0.0
@apiPermission isCrm
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/user/docs Get documents for specific user(for crm)
@apiGroup CRM
@apiDescription Get documents for specific user(for crm)
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} email Email client
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/user/verified Verify or decline user
@apiGroup CRM
@apiDescription Verify or decline user
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} success 0 - reject users verification, 1 - approved
@apiParam {String} email Email client
@apiParam {String} reason if rejected
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/user/balance Get balance of user
@apiGroup CRM
@apiDescription Get balance of user
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} login login id of account
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/user/update Update user from crm
@apiGroup CRM
@apiDescription Update user from crm
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} email Email client
@apiParam {String} country User Country
@apiParam {String} code VALIDATION_2FA_CODE
@apiParam {string} address User Address
@apiParam {string} city User City
@apiParam {string} state User State
@apiParam {string} postalCode User PostalCode
@apiParam {integer} phone User Phone
@apiParam {string} birthday User Birthday
@apiParam {string} passportId User PassportId
@apiParam {string} photoPassport User Photo Passport
@apiParam {string} photoVerification User Photo Verification
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {POS} v1.0/crm/register Add new user from crm
@apiGroup CRM
@apiDescription Add new user from crm
@apiVersion 1.0.0
@apiPermission isCrm
@apiParam {String} country User Country
@apiParam {String} code VALIDATION_2FA_CODE
@apiParam {string} address User Address
@apiParam {string} city User City
@apiParam {string} state User State
@apiParam {string} postalCode User PostalCode
@apiParam {integer} phone User Phone
@apiParam {string} birthday User Birthday
@apiParam {string} passportId User PassportId
@apiParam {string} photoPassport User Photo Passport
@apiParam {string} photoVerification User Photo Verification
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/trades/history/current Get current trades history
@apiGroup ORDERS
@apiDescription Get current trades history
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID External ID
@apiParam {String} symbol symbol default - BTCUSD
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/orders/me/closed Get current users closed orders
@apiGroup ORDERS
@apiDescription Get current users closed orders
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID External ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/orders/me/opened Get current users opened orders
@apiGroup ORDERS
@apiDescription Get current users opened orders
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID External ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/orders/current/opened Get all current opened orders
@apiGroup ORDERS
@apiDescription Get all current opened orders
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID External ID
@apiParam {String} symbol BTCUSD by default
@apiParam {Number} rank 1 by default. It means precision
@apiParam {Number} count 20 by default - amount of lines for book
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/orders/cancel Cancel order
@apiGroup ORDERS
@apiDescription Cancel order
@apiVersion 1.0.0
@apiPermission isAuth, isBot
@apiParam {String} extID External ID
@apiParam {String} id Order ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/orders/open Open sell or buy order
@apiGroup ORDERS
@apiDescription Open sell or buy order
@apiVersion 1.0.0
@apiPermission isAuth, isBot
@apiParam {String} extID Externail ID
@apiParam {String} symbol Symbol order
@apiParam {String} type Type order buy or sell
@apiParam {Number} price Price order
@apiParam {Number} amount Amount order
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/quotes/chart/symbols Get symbol details
@apiGroup QUOTES
@apiDescription Get symbol details
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} symbol symbol Default BTCUSD
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/quotes/chart/search Get all symbols
@apiGroup QUOTES
@apiDescription Get all symbols
@apiVersion 1.0.0
@apiPermission isAll
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/quotes/chart/history Get quotes history
@apiGroup QUOTES
@apiDescription Get quotes history
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} symbol Symbol
@apiParam {String} resolution resolution ["5", "15", "30", "120", "240", "1D"]
@apiParam {TIMESTAMP} from from without ms
@apiParam {TIMESTAMP} to to without ms
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/quotes/chart/config Get chart config
@apiGroup QUOTES
@apiDescription Get chart config
@apiVersion 1.0.0
@apiPermission isAll
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/quotes/current Get current values of all currency pair
@apiGroup QUOTES
@apiDescription Get current values of all currency pair
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID External ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/docs Get index html of apiDoc
@apiGroup Resources
@apiDescription Get index html of apidoc
@apiVersion 1.0.0
@apiPermission isAll
*/
/**
@api {socket.io} v1.0/common-resources Get common resources
@apiGroup Resources
@apiDescription Returns info about countries, currency and currency pair reductions.
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID unique id of request to identify response
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {String} data.extID 
@apiSuccess {Array} data.countries 
@apiSuccess {Object} data.currencies 
@apiSuccess {Object} data.symbols 
*/
/**
@api {GE} v1.0/update_private_keys Update privateKeys from MySQL
@apiGroup SYSTEM
@apiDescription Import BTC, LTC, DASH private keys from MySQL (from accounts table) to blockchains.
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {Boolean} data.success 
*/
/**
@api {GE} v1.0/reimport-private-keys Import privateKeys from MySQL
@apiGroup SYSTEM
@apiDescription Import BTC, LTC, DASH private keys from MySQL to blockchains.
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {Boolean} data.success 
*/
/**
@api {GE} v1.0/accounts/check-redis-accounts Create accounts in Redis from MySQL
@apiGroup SYSTEM
@apiDescription Get accounts from MySQL and search them in Redis and app's memory. Adds accounts if necessary. Also check balances and correct them.
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {Boolean} data.success 
*/
/**
@api {POS} v1.0/init Initialization of system clients (create accounts for them)
@apiGroup SYSTEM
@apiDescription Description This method should be called only once time after the first running of application.Clients are created by migrations, but also it need create accounts for these clients. Current method get all system clients (role 5, 6, 3) and create accounts for them if it not exists. If return empty array then clients already initialized.If array is not empty then initialization was success
@apiVersion 1.0.0
@apiPermission isSystem
@apiParam {String} extID unique id of request to identify response
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/accounts/check Checking if clients haven`t any currency-accounts
@apiGroup SYSTEM
@apiDescription This method should be called after adding new currencies into config file. Method get all clients and check: if client hasn`t someone account, then this accounts is created
@apiVersion 1.0.0
@apiPermission isSystem
@apiParam {String} extID unique id of request to identify response
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
@apiSuccess {Boolean} data.success 
*/
/**
@api {GE} v1.0/status Checking status of platform
@apiGroup SYSTEM
@apiDescription This method check connections to databases. Return status 200 if connections to mysql and redis are established, return 500 if one of them has error
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} extID unique id of request to identify response
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {PU} v1.0/system_transaction Add new System Transaction
@apiGroup TRANSACTIONS
@apiDescription Add new System Transaction
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/withdrawals/list Get list of withdrawals (for scheduler service)
@apiGroup TRANSACTIONS
@apiDescription Get list of withdrawals (for scheduler service)
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {PU} v1.0/withdrawals Update withdrawal and users balance
@apiGroup TRANSACTIONS
@apiDescription Update withdrawal and users balance
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/withdrawals/fees Get withdrawal fees
@apiGroup TRANSACTIONS
@apiDescription Get withdrawal fees
@apiVersion 1.0.0
@apiPermission isAuth
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/withdrawals/me Create request for withdrawal
@apiGroup TRANSACTIONS
@apiDescription Create request for withdrawal
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID External ID
@apiParam {Number} amount Amount withdrawal
@apiParam {String} address Address for withdrawal
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/deposits/list Get list of deposits
@apiGroup TRANSACTIONS
@apiDescription Get list of deposits
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {PU} v1.0/deposits Update deposit and users balance
@apiGroup TRANSACTIONS
@apiDescription Update deposit and users balance
@apiVersion 1.0.0
@apiPermission isSystem
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/deposits/me Get address for deposit
@apiGroup TRANSACTIONS
@apiDescription Get address for deposit
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} extID External ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {PU} v1.0/users/:id/limits Get Users Limits
@apiGroup USER
@apiDescription Get Users Limits
@apiVersion 1.0.0
@apiPermission isSystem
@apiParam {String} clientId client ID
@apiParam {String} currency Currency
@apiParam {String} amount Amount
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/limits/me Get My Limits
@apiGroup AUTH
@apiDescription Get My Limits
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} clientId client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/balance/me/total Get Total Balance
@apiGroup AUTH
@apiDescription Get Total Balance
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} clientId client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/balances/me Get Account Balances
@apiGroup AUTH
@apiDescription Get Account Balances
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} clientId client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/unsubscribe Unset Subscribe to Symbol
@apiGroup AUTH
@apiDescription Unset Subscribe
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} symbol Symbol pair
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/subscribe Subscribe to Symbol
@apiGroup AUTH
@apiDescription Get subscribe
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} symbol Symbol pair
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/users/addresses Get Users Public Key
@apiGroup AUTH
@apiDescription Get Users Public Key
@apiVersion 1.0.0
@apiPermission isSystem
@apiParam {String} currency currency
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/users/me/token Get User Token
@apiGroup AUTH
@apiDescription Get User Token
@apiVersion 1.0.0
@apiPermission isAuth
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/users/me/verify Get User Verify
@apiGroup AUTH
@apiDescription Get User Verify
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} clientId Client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/users/me/details Get User Detail
@apiGroup AUTH
@apiDescription Get User Detail
@apiVersion 1.0.0
@apiPermission isAuth
@apiParam {String} clientId Client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/users/me Get User Info
@apiGroup AUTH
@apiDescription Get User Info
@apiVersion 1.0.0
@apiPermission isAuth, isBot
@apiParam {String} clientId Client ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {GE} v1.0/register/confirm Confirm Registration New User
@apiGroup AUTH
@apiDescription Register New User
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} hash Confirm Hash
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/check_referrer_id Check referrer id
@apiGroup USER
@apiDescription Check referrer id
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} referrer Referrer ID
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/get_referrer_id Get referrer id
@apiGroup USER
@apiDescription Get referrer id
@apiVersion 1.0.0
@apiPermission isAuth
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {socket.io} v1.0/register Register New User
@apiGroup USER
@apiDescription Register New User
@apiVersion 1.0.0
@apiPermission isAll
@apiParam {String} country User Country
@apiParam {String} code VALIDATION_2FA_CODE
@apiParam {string} address User Address
@apiParam {string} city User City
@apiParam {string} state User State
@apiParam {string} postalCode User PostalCode
@apiParam {integer} phone User Phone
@apiParam {string} birthday User Birthday
@apiParam {string} passportId User PassportId
@apiParam {string} photoPassport User Photo Passport
@apiParam {string} photoVerification User Photo Verification
@apiUse defErrorResp
@apiSuccess {Number} status 200
@apiSuccess {Object} data 
*/
/**
@api {System} Error Codes 
@apiGroup System
@apiVersion 1.0.0
@apiParam {1000} VALIDATION_AMOUNT_IS_NOT_FILLED amount is not filled
@apiParam {1001} VALIDATION_TOKEN_IS_NOT_FILLED token is not filled
@apiParam {1002} VALIDATION_ADDRESS_IS_NOT_FILLED address is not filled
@apiParam {1003} VALIDATION_EMAIL_IS_NOT_FILLED email is not filled
@apiParam {1004} VALIDATION_CLIENTID_IS_NOT_FILLED clientId is not filled
@apiParam {1005} VALIDATION_SYMBOL_IS_NOT_FILLED symbol is not filled
@apiParam {1008} VALIDATION_PASSWORD_IS_NOT_FILLED password is not filled
@apiParam {1009} VALIDATION_ACCOUNTID_IS_NOT_FILLED accountId is not filled
@apiParam {1010} VALIDATION_PRICE_IS_NOT_FILLED price is not filled
@apiParam {1011} VALIDATION_TYPE_IS_NOT_FILLED type is not filled
@apiParam {1012} VALIDATION_ORDERID_IS_NOT_FILLED id of order is not filled
@apiParam {1013} VALIDATION_CURRENCY_IS_NOT_FILLED currency is not filled
@apiParam {1014} VALIDATION_PHONE_IS_NOT_FILLED phone is not filled
@apiParam {1015} VALIDATION_COUNTRY_IS_NOT_FILLED country is not filled
@apiParam {1016} VALIDATION_FIRSTNAME_IS_NOT_FILLED firstname is not filled
@apiParam {1017} VALIDATION_LASTNAME_IS_NOT_FILLED lastname is not filled
@apiParam {1018} VALIDATION_CITY_IS_NOT_FILLED city is not filled
@apiParam {1019} VALIDATION_STATE_IS_NOT_FILLED state is not filled
@apiParam {1020} VALIDATION_POSTALCODE_IS_NOT_FILLED postalCode is not filled
@apiParam {1021} VALIDATION_BIRTHDAY_IS_NOT_FILLED birthday is not filled
@apiParam {1022} VALIDATION_PASSPORTID_IS_NOT_FILLED passportId is not filled
@apiParam {1023} VALIDATION_PHOTOPASSPORT_IS_NOT_FILLED photoPassport is not filled
@apiParam {1024} VALIDATION_PHOTOVERIFICATION_IS_NOT_FILLED photoVerification is not filled
@apiParam {1025} VALIDATION_HASH_IS_NOT_FILLED hash is not filled
@apiParam {1026} VALIDATION_INVALID_REFERRER Invalid referrer id.
@apiParam {3000} VALIDATION_PHONE_HAS_INCORRECT_TYPE phone has incorrect type
@apiParam {3001} VALIDATION_BIRTHDAY_HAS_INVALID_VALUE birthday has invalid value
@apiParam {3005} VALIDATION_COUNTRY_HAS_INVALID_VALUE country has invalid value
@apiParam {3006} VALIDATION_PRICE_HAS_INVALID_VALUE price has invalid value
@apiParam {3002} VALIDATION_PHOTOPASSPORT_HAS_INCORRECT_TYPE photoPassport should be jpg, png or bmp
@apiParam {3003} VALIDATION_PHOTOVERIFICATION_HAS_INCORRECT_TYPE photoVerification should be jpg, png or bmp
@apiParam {3004} VALIDATION_TOO_BIG_SIZE max size of images is 2Mb
@apiParam {1100} VALIDATION_NOT_ENOUGH_TIME_PASSED not enough time has passed for new faucet
@apiParam {1101} VALIDATION_WITHDRAWAL_IS_NOT_PENDING_OR_APPROVED wanted withdrawal must be pending or approved
@apiParam {1101} VALIDATION_WITHDRAWAL_IS_NOT_PENDING wanted withdrawal must be pending
@apiParam {1102} VALIDATION_WITHDRAWAL_STATUS_IS_NOT_PROCESSED withdrawal status must be processed
@apiParam {1103} VALIDATION_WITHDRAWAL_STATUS_IS_NOT_APPROVED_OR_DECLINED withdrawal status must be approved or declined
@apiParam {1104} VALIDATION_WITHDRAWAL_AMOUNT_IS_TOO_SMALL withdrawal amount is too small
@apiParam {1105} VALIDATION_INSUFFICIENT_FUNDS Insufficient funds
@apiParam {1106} VALIDATION_WRONG_PASSWORD wrong password
@apiParam {1107} VALIDATION_WRONG_2FA_CODE wrong second factor auth code
@apiParam {1108} VALIDATION_2FA_CODE_IS_NOT_FILLED second factor auth code is not filled
@apiParam {1109} VALIDATION_RECAPTCHA_ERROR reCAPTCHA validation failed.
@apiParam {1110} VALIDATION_WITHDRAWAL_EXCEEDS_LIMIT Withdrawal amount exceeds user's day limit.
@apiParam {1200} VALIDATION_TYPE_HAS_INVALID_VALUE type should be more than 1
@apiParam {1201} VALIDATION_SYMBOL_HAS_INVALID_VALUE symbol should be BTCUSD, LTCUSD or LTCBTC
@apiParam {1300} VALIDATION_EMAIL_ALREADY_EXIST this email already exist
@apiParam {1301} VALIDATION_PHONE_ALREADY_EXIST this phone already exist
@apiParam {1400} VALIDATION_CRM_NO_ID Parameter id is required
@apiParam {1401} VALIDATION_CRM_NO_ACCOUNTID Parameter accountId is required
@apiParam {1402} VALIDATION_CRM_NO_STATUS Parameter status is required
@apiParam {1403} VALIDATION_CRM_NO_AMOUNT Parameter amount is required
@apiParam {1404} VALIDATION_CRM_NO_PAYMENT_SYSTEM Parameter payment_system is required
@apiParam {1405} VALIDATION_CRM_NO_LOGIN Parameter login is required
@apiParam {1406} VALIDATION_CRM_NO_EMAIL Parameter email is required
@apiParam {1407} VALIDATION_CRM_NO_PHONE Parameter phone is required
@apiParam {1500} INSUFFICIENT_FUNDS_IN_SYSTEM_WALLET Insufficient funds in system wallet
@apiParam {1501} BAD_SYSTEM_WALLET_BALANCE Can't parse number from system wallet balance
@apiParam {1600} VALIDATION_ORDER_BAD_PARAMETER Bad order parameter.
@apiParam {1601} VALIDATION_ORDER_BAD_OFFSET_PARAMETER Bad offset parameter.
@apiParam {1602} VALIDATION_ORDER_BAD_DATE_PARAMETER Bad date parameter (must be datetime).
@apiParam {1603} VALIDATION_ORDER_BAD_AMOUNT_PARAMETER Bad amount parameter
@apiParam {1604} VALIDATION_ORDER_BAD_PRICE_PARAMETER Bad price parameter
@apiParam {1605} VALIDATION_BAD_AMOUNT Bad amount
@apiParam {1700} USER_ALREADY_HAS_2FA 2FA is already activated on this account.
@apiParam {1701} USER_HAS_NO_2FA 2FA is deactivated on this account.
@apiParam {1702} USER_IS_NOT_ACTIVATED User is not activate. Please, confirm your email.
@apiParam {1703} USER_ALREADY_ACTIVATED User already activated.
@apiParam {1800} CONFIRMATION_TIME_IS_UP Confirmation time is up.
@apiParam {1900} INVALID_HASH Invalid hash.
@apiParam {2000} SOCIAL_NET_ERROR social net error
@apiParam {2001} SOCIAL_NET_INVALID_TOKEN invalid token
@apiParam {2002} SOCIAL_NET_TOKEN_EXPIRED token expired
@apiParam {3002} FORBIDDEN_CANNOT_PARSE_TOKEN Can't parse token
@apiParam {3003} FORBIDDEN_NO_ACCESS_RIGHTS No access rights
@apiParam {3004} FORBIDDEN_XTOKEN_EXPIRED X-Token is expired
@apiParam {3005} FORBIDDEN_NO_AUTH You are not authorized
@apiParam {4000} NOT_FOUND_METHOD Method is not found
@apiParam {4001} NOT_FOUND_CLIENT Client is not found
@apiParam {4002} NOT_FOUND_ORDER Order is not found
@apiParam {4003} NOT_FOUND_ACCOUNT Account is not found
@apiParam {4003} NOT_FOUND_ACCOUNTS Accounts are not found for this client
@apiParam {4004} NOT_FOUND_WITHDRAWAL Withdrawal is not found
@apiParam {5000} SEND_EMAIL_FILE_ERROR Error with file in send Email.
@apiParam {5001} SEND_EMAIL_FAILED Failed send Email.
@apiParam {5002} SEND_EMAIL_MISSING_PARAMS Request MUST contains parameters `text`, `to`.
@apiParam {5003} SEND_EMAIL_TOO_LARGE_FILE File size is too large.
@apiParam {5100} REDIS_TRANSACTION_ERROR redis transaction error
@apiParam {5101} REDIS_INTERNAL_ERROR internal redis error
@apiParam {5102} S3_UPLOADING_ERROR error with s3 uploading
@apiParam {5103} S3_DELETING_ERROR error with s3 deleting
@apiParam {5120} MAIL_SENDING_ERROR error with mail sending
@apiParam {9999} UNKNOWN_ERROR Unknown error
@apiParam {7020} USER_ALREADY_VERIFIED user already verified
@apiParam {7021} WAITING_VERIFICATION_CONFIRMATION request already is sent. Wait please confirmation
*/
