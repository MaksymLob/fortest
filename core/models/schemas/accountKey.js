
const config = require(__dirroot+'config.json');
const currencies = Object.keys(config.currencies);

const account = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  currency: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [currencies]},
    defaultValue: 'BTC',
    parentKey: 'clientId'
  },
  clientId: {
    type: 'integer',
    allowNull: false
  },
  balance: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  },
  privateKey: {
    type: 'string',
    allowNull: true
  },
  publicKey: {
    type: 'string',
    allowNull: true
  },
  address: {
    type: 'string',
    allowNull: true
  }
};

module.exports = {
  schema: account
};
