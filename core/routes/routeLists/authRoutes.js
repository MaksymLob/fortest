
module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/login': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'login',

    name: 'Login user',
    group: 'AUTH',
    description: 'Login user. The first method if user hasn`t X-Token, return info about user with all accounts and updated X-Token. After this method you can receive stream of private user`s information: /balance/me/update, /orders/me/update, /trades/me/update, and public stream of quotes: /quotes/update ATTENTION: This method return success=true, if user has enabled 2FA (Complete login by sending /v1.0/2fa/login). After /v1.0/login user with 2FA has NO access to private methods.',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      email: {
        type: 'String',
        description: 'REQUIRED'
      },
      password: {
        type: 'String',
        description: 'REQUIRED'
      },
      recaptcha: {
        type: 'String',
        description: 'REQUIRED'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.extID': {
        type: 'String',
      },
      'data.X-Token': {
        type: 'String',
      },
      'data.email': {
        type: 'String',
      },
      'data.accounts': {
        type: 'Object',
      },
      'data.accounts.BTC': {
        type: 'Object'
      },
      'data.accounts.BTC.balance': {
        type: 'Number'
      },
      'data.accounts.USD': {
        type: 'Object'
      },
      'data.accounts.USD.balance': {
        type: 'Number'
      },
      'data.accounts.ETH': {
        type: 'Object'
      },
      'data.accounts.ETH.balance': {
        type: 'Number'
      },
      'data.success': {
        type: 'String'
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/logout': {
    access: ['isAuth'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'logout',

    name: 'Logout user',
    group: 'AUTH',
    description: 'Logout user',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/forgotPassword': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'forgotPassword',

    name: 'ForgotPassword',
    group: 'AUTH',
    description: 'ForgotPassword. Send email with recovery password',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      email: {
        type: 'String',
        description: 'User e-mail'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/resetPassword': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'resetPassword',

    name: 'ResetPassword',
    group: 'AUTH',
    description: 'ResetPassword. Reset password by token from recovery email',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      password: {
        type: 'String',
        description: 'User password'
      },
      hash: {
        type: 'String',
        description: 'Hash user'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/2fa/activate': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'activate2FA',

    name: '2FA Activate',
    group: 'AUTH',
    description: 'Request for 2FA activation',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      password: {
        type: 'String',
        description: 'User password'
      },
      hash: {
        type: 'String',
        description: 'Hash user'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']

  },

  '/v1.0/2fa/confirm_activate': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'confirmActivate2FA',

    name: '2FA ConfirmActivate',
    group: 'AUTH',
    description: 'Confirm Activate 2FA',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']

  },

  '/v1.0/2fa/login': {
    access: ['isPartAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'login2FA',

    name: '2FA login',
    group: 'AUTH',
    description: 'Login by 2FA',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/2fa/deactivate': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'deactivate2FA',

    name: '2FA deactivate',
    group: 'AUTH',
    description: 'deactivate2FA',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  }
};
