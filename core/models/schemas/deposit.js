
const config = require(__dirroot+'config.json');
const constants = __app('libs/constants');
const currencies = Object.keys(config.currencies);
currencies.splice(currencies.indexOf('USD'), 1);

let transactionStatuses = [];
Object.keys(constants.TRANSACTION_STATUSES.DEPOSIT).forEach(item => {
  transactionStatuses.push(constants.TRANSACTION_STATUSES.DEPOSIT[item]);
});

const deposit = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  clientId: {
    type: 'integer',
    allowNull: false
  },
  currency: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [currencies]}
  },
  amount: {
    type: 'double',
    allowNull: true
  },
  fee: {
    type: 'double',
    allowNull: false,
    defaultValue: 0.0001
  },
  status: {
    type: 'string',
    allowNull: false,
    defaultValue: constants.TRANSACTION_STATUSES.DEPOSIT.PENDING,
    validate: {isIn: [transactionStatuses]}
  },
  txId: {
    type: 'string',
    allowNull: true
  }
};

module.exports = {
  schema: deposit
};
