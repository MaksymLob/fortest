

/***************************  COMMON methods  *************************/

/**
 * @api {socket.io} /v1.0/common-resources Get common resources
 *
 * @apiPermission all
 *
 * @apiDescription Returns info about countries, currency and currency pair reductions.
 *
 * @apiVersion 1.0.0
 * @apiGroup COMMON RESOURCES
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.countries
 * @apiSuccess {Object} data.currencies
 * @apiSuccess {Object} data.symbols
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *   socket
 *    .emit('/v1.0/common-resources', {extID: 'qwrw'})
 *    .on('/response', res => {
 *      status: 200,
 *      data: {
 *        extID: 'qwrw',
 *        countries: [
 *          {
 *            code: 'RU',
 *            name: 'Russian Federation'
 *          }
 *        ],
 *        currencies: {
 *          BTC: "BTC"
 *        },
 *        symbols: {
 *          DSHBTC: {
 *            fullName: "DASHBTC",
 *            digits: 6
 *          }
 *        },
 *      }
 *    });
 */

/**
 * @api {GET} /v1.0/status Checking status of platform
 *
 * @apiPermission all
 *
 * @apiDescription This method check connections to databases. Return status 200 if connections to
 * mysql and redis are established, return 500 if one of them has error
 *
 * @apiVersion 1.0.0
 * @apiGroup SYSTEM
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {String} data OK
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/***************************  SYSTEM methods  *************************/

/**
 * @api {POST} /v1.0/init Initialization of system clients (create accounts for them)
 *
 * @apiPermission system
 *
 * @apiHeader {String} X-Token It should be special system token for system-user with role 6
 *
 * @apiDescription This method should be called only once time after the first running of application.
 * Clients are created by migrations, but also it need create accounts for these clients. Current method get all
 * system clients (role 5, 6, 3) and create accounts for them if it not exists. If return empty array then clients already initialized.
 * If array is not empty then initialization was success
 *
 * @apiVersion 1.0.0
 * @apiGroup SYSTEM
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/accounts/check Checking if clients haven't any currency-accounts
 *
 * @apiPermission system
 *
 * @apiHeader {String} X-Token It should be special system token for system-user with role 6
 *
 * @apiDescription This method should be called after adding new currencies into config file.
 * Method get all clients and check: if client hasn't someone account, then this accounts is created
 *
 * @apiVersion 1.0.0
 * @apiGroup SYSTEM
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 * @apiSuccess {Boolean} data.success true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/***************************  USERS methods  *************************/

/**
 * @api {socket.io} /v1.0/users/me Get info about user and his accounts
 *
 * @apiPermission auth
 *
 * @apiDescription If user have already connected to socket with actual X-Token, then this request should be the first.
 * Method return info about user with all accounts and updated X-Token. After this method you can receive stream of
 * private user's information: /balance/me/update, /orders/me/update, /trades/me/update, and public stream of quotes: /quotes/update
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {String} data.X-Token updated token
 * @apiSuccess {String} data.email user's email
 * @apiSuccess {Object} data.accounts user's accounts
 * @apiSuccess {Object} data.accounts.BTC
 * @apiSuccess {Number} data.accounts.BTC.balance
 * @apiSuccess {Object} data.accounts.USD
 * @apiSuccess {Number} data.accounts.USD.balance
 * @apiSuccess {Object} data.accounts.ETH
 * @apiSuccess {Number} data.accounts.ETH.balance
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/users/me', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  email: 'test@test.com,
 *                  X-Token: 'dsrgdrshdshdsghdsgbd...',
 *                  accounts: {
 *                      BTC: {
 *                          balance: 1000
 *                      },
 *                      USD: {
 *                          balance: 20000
 *                      }
 *                  }
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/users/me/details Get detalized info about user
 *
 * @apiPermission auth
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {String} data.email user's email
 * @apiSuccess {String} data.phone user's phone
 * @apiSuccess {String} data.firstname user's firstname
 * @apiSuccess {String} data.lastname user's lastname
 * @apiSuccess {String} data.country user's country
 * @apiSuccess {String} data.verified user's verified
 * @apiSuccess {String} data.address user's address
 * @apiSuccess {String} data.city user's city
 * @apiSuccess {String} data.state user's state
 * @apiSuccess {String} data.postalCode user's postalCode
 * @apiSuccess {String} data.birthday user's birthday
 * @apiSuccess {String} data.passportId user's passportId
 * @apiSuccess {String} data.photoPassport user's photoPassport
 * @apiSuccess {String} data.photoVerification user's photoVerification
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {socket.io} /v1.0/users/me/verify User verification
 *
 * @apiPermission auth
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {String} address
 * @apiParam {String} city
 * @apiParam {String} state
 * @apiParam {String} postalCode
 * @apiParam {String} passportId
 * @apiParam {String} photoVerification picture in base64 format
 * @apiParam {String} photoPassport picture in base64 format
 * @apiParam {String} birthday
 * @apiParam {String} phone
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {String} data.sucess true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {socket.io} /v1.0/limits/me/ Get user day limits
 *
 * @apiPermission auth
 *
 * @apiDescription Get user day limits (dayUsdLimit and remainedDayUsd)
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Number} data.dayUsdLimit user's max withdrawal limit
 * @apiSuccess {Number} data.remainedDayUsd user's excess withdrawal limit
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/limits/me/', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  dayUsdLimit: 1000
 *                  remainedDayUsd: 666
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/balance/me/total Get user total balance
 *
 * @apiPermission auth
 *
 * @apiDescription Get user total balance
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Number} data.balanceUSD user's balance in USD
 * @apiSuccess {Number} data.balanceBTC user's balance in BTC
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/balance/me/total', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  balanceUSD: 10000
 *                  balanceBTC: 2
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/balances/me Get user balances
 *
 * @apiPermission auth
 *
 * @apiDescription Get user balances by currencies
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Object} data.balances Array of balances (Every element of array consist of:)
 * @apiSuccess {String} data.balances.coin Currency abbreviation
 * @apiSuccess {String} data.balances.name Currency name
 * @apiSuccess {Number} data.balances.totalBalance Balance
 * @apiSuccess {Number} data.balances.onOrders Used (on orders) part of balance
 * @apiSuccess {Number} data.balances.btcValue Balance in BTC
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/balances/me/', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  balances: [
 *                      {
 *                          coin: 'BTC',
 *                          name: 'Bitcoin',
 *                          totalBalance: 3,
 *                          onOrders: 0,
 *                          btcValue: 3
 *                      }
 *                  ]
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/register Create new user
 *
 * @apiPermission allUsers
 *
 * @apiDescription Create new user. Method return success=true. After this method user must confirm email and login to receive X-token.
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} recaptcha REQUIRED
 * @apiParam {String} email REQUIRED, should be unique
 * @apiParam {String} password REQUIRED
 * @apiParam {String} country REQUIRED, should be a country code (Register doesn't matter)
 * @apiParam {String} firstname
 * @apiParam {String} secondname
 * @apiParam {String} phone
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {Boolean} data.success true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/register', {email: 'test@tes.com', password: 'test', recaptcha: 'sfsdg', country: 'RU', extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  success: true
 *              }
 *         );
 */

/**
 * @api {GET} /v1.0/register/confirm Confirm user's email
 *
 * @apiPermission allUsers
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} clientHash REQUIRED, query parameter, hash from link
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 * @apiSuccess {Array} data.success true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {socket.io} /v1.0/login Login
 *
 * @apiPermission allUsers
 *
 * @apiDescription Login user. The first method if user hasn't X-Token,  return info about user with all accounts and updated X-Token. After this method you can receive stream of
 * private user's information: /balance/me/update, /orders/me/update, /trades/me/update, and public stream of quotes: /quotes/update
 * ATTENTION: This method return success=true, if user has enabled 2FA (Complete login by sending /v1.0/2fa/login). After /v1.0/login user with 2FA has NO access to private methods.
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} email REQUIRED
 * @apiParam {String} password REQUIRED
 * @apiParam {String} recaptcha REQUIRED
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {String} data.X-Token updated token
 * @apiSuccess {String} data.email user's email
 * @apiSuccess {Object} data.accounts user's accounts
 * @apiSuccess {Object} data.accounts.BTC
 * @apiSuccess {Number} data.accounts.BTC.balance
 * @apiSuccess {Object} data.accounts.USD
 * @apiSuccess {Number} data.accounts.USD.balance
 * @apiSuccess {Object} data.accounts.ETH
 * @apiSuccess {Number} data.accounts.ETH.balance
 * @apiSuccess {String} data.success If user has enabled 2FA
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/login', {email: 'test@tes.com', password: 'test', extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  email: 'test@test.com,
 *                  X-Token: 'dsrgdrshdshdsghdsgbd...',
 *                  accounts: {
 *                      BTC: {
 *                          balance: 1000
 *                      },
 *                      USD: {
 *                          balance: 20000
 *                      }
 *                  }
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/2fa/login Login 2FA
 *
 * @apiPermission auth (after /v1.0/login)
 *
 * @apiDescription Login user with 2 factor auth. The second method to login with 2FA. (Returns same info as basic login for users without 2FA)
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} code REQUIRED 6-digit token. Available formatting: 666666, '666666', '666 666'
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {String} data.X-Token updated token
 * @apiSuccess {String} data.email user's email
 * @apiSuccess {Object} data.accounts user's accounts
 * @apiSuccess {Object} data.accounts.BTC
 * @apiSuccess {Number} data.accounts.BTC.balance
 * @apiSuccess {Object} data.accounts.USD
 * @apiSuccess {Number} data.accounts.USD.balance
 * @apiSuccess {Object} data.accounts.ETH
 * @apiSuccess {Number} data.accounts.ETH.balance
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/2fa/login', {code: '666 666'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  email: 'test@test.com,
 *                  X-Token: 'dsrgdrshdshdsghdsgbd...',
 *                  accounts: {
 *                      BTC: {
 *                          balance: 1000
 *                      },
 *                      USD: {
 *                          balance: 20000
 *                      }
 *                  }
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/2fa/activate Activate 2FA
 *
 * @apiPermission auth
 *
 * @apiDescription Activate 2 factor auth. After this step user must confirm activation (/v1.0/2fa/confirm_activate).
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} password REQUIRED
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.key Unique user key for 2FA (Show it to user)
 * @apiSuccess {String} data.OTPAuth Use it for QR-codes
 * @apiSuccess {String} data.extID
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/2fa/activate', {password: 'password'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  key: 'acqo ua72 d3yf a4e5 uorx ztkh j2xl 3wiz',
 *                  OTPAuth: 'otpauth://totp/Test%20Company:test2@test?secret=WLLL7Z3PSPPDVM2T26SDTGHEVQ45VDTT&issuer=Test%20Company&algorithm=SHA1&digits=6&period=30'
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/2fa/confirm_activate Confirm activation 2FA
 *
 * @apiPermission auth
 *
 * @apiDescription Confirm activation 2FA. (Call this after activate 2FA)
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} code REQUIRED 6-digit token. Available formatting: 666666, '666666', '666 666'
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {Boolean} data.success Indicates successful method execution
 * @apiSuccess {String} data.extID
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/2fa/confirm_activate', {code: '666 666'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  success: true
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/2fa/deactivate Deactivate 2FA
 *
 * @apiPermission auth
 *
 * @apiDescription Deactivate 2FA
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} password REQUIRED user's password
 * @apiParam {String} code REQUIRED 6-digit token. Available formatting: 666666, '666666', '666 666'
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {Boolean} data.success Indicates successful method execution
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 * @apiSuccess {String} data.extID
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/2fa/deactivate', {password: 'password', code: '666 666'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  success: true
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/subscribe Subscribe to stream by symbol
 *
 * @apiPermission allUsers
 *
 * @apiDescription After this method you can receive stream of information by symbol: /orders/books/update, /trades/history/update
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} symbol default 'BTCUSD'
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Boolean} data.access true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/subscribe', {symbol: 'BTCUSD', extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  success: true,
 *                  extID: 'extID'
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/unsubscribe Unsubscribe from stream by symbol
 *
 * @apiPermission allUsers
 *
 * @apiDescription After this method you can't receive stream of information by symbol: /orders/books/update, /trades/history/update
 *
 * @apiVersion 1.0.0
 * @apiGroup USERS
 *
 * @apiParam {String} symbol default 'BTCUSD'
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Boolean} data.access true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/unsubscribe', {symbol: 'BTCUSD', extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  success: true,
 *                  extID: 'extID'
 *              }
 *         );
 */

/***************************  STREAMS  *******************************/

/**
 * @api {socket.io} /balance/me/update Update balance
 *
 * @apiDescription You can receive this event only after /v1.0/login, /v1.0/users/me methods.
 * Balance is updated every time after opening and closing orders.
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 * @apiExample {js} Listen events:
 *     socket.on('/balance/me/update', res => {
 *          currency: 'BTC',
 *          balance: 235235
 *     );
 */

/**
 * @api {socket.io} /limits/me/update Update day withdrawal limits
 *
 * @apiDescription You can receive this event only after /v1.0/login, /v1.0/users/me methods.
 * Balance is updated every time after opening and closing orders.
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 * @apiExample {js} Listen events:
 *     socket.on('/limits/me/update', res => {
 *          dayUsdLimit: 1000,
 *          remainedDayUsd: 666
 *     );
 */

/**
 * @api {socket.io} /orders/me/update Update open orders list
 *
 * @apiDescription You can receive this event only after /v1.0/login, /v1.0/users/me methods.
 * Event is emited when new order is added or remove. Parameter action = 'remove' or action = 'add'
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 * @apiExample {js} Listen events:
 *     socket.on('/orders/me/update', res => {
 *          action: 'remove',
 *          id: 34,
 *          price: 1232,
 *          ...
 *     );
 */

/**
 * @api {socket.io} /quotes/update Update quotes
 *
 * @apiDescription You can receive this event only after /v1.0/login, /v1.0/users/me methods.
 * For init values you need call request /v1.0/quotes/current. After that you can listen this event.
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 * @apiExample {js} Listen events:
 *     socket.on('/quotes/update', res => {
 *          symbol: 'BTCUSD',
 *          baseVolume: 23,
 *          quoteVolume: 235235,
 *          currentPrice: 2352,
 *          percentChange: '-34.3,
 *          high: 2352,
 *          low: 2333
 *     );
 */

/**
 * @api {socket.io} /orders/update Update orders for order book
 *
 * @apiPermission allUsers
 *
 * @apiDescription You can receive this event only after /v1.0/subscribe method.
 * For init values you need call request /v1.0/orders/current/opened. After that you can listen this event.
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 */

/**
 * @api {socket.io} /trades/history/update Update trades history
 *
 * @apiDescription You can receive this event only after /v1.0/subscribe method.
 * For init values you need call request /v1.0/trades/history/current. After that you can listen this event.
 *
 * @apiVersion 1.0.0
 * @apiGroup STREAM
 *
 * @apiExample {js} Listen events:
 *     socket.on('/trades/history/update', res => {
 *          symbol: 'BTCUSD',
 *          amount: 2.3,
 *          price: 1000,
 *          volume: 46346,
 *          type: 'buy',
 *          time: 2353464365
 *     );
 */

/***************************  ORDERS methods  ************************/

/**
 * @api {socket.io} /v1.0/orders/open Open new order
 *
 * @apiPermission auth
 *
 * @apiDescription Open new order, return true if success
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {String} symbol REQUIRED
 * @apiParam {String} type REQUIRED [buy, sell]
 * @apiParam {Number} price REQUIRED
 * @apiParam {Number} amount REQUIRED
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Boolean} data.success
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/orders/open', {extID: 'extID', symbol: 'BTCUSD', price: 2148.3, amount: 0.3, type: 'buy'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  success: true
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/orders/cancel Cancel order by id
 *
 * @apiPermission auth
 *
 * @apiDescription Cancel order by id, return true if success
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {String} id REQUIRED
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Boolean} data.success
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/orders/cancel', {extID: 'extID', id: 234})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  success: true
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/orders/current/opened Get all current opened orders
 *
 * @apiPermission auth
 *
 * @apiDescription Return array of opened orders
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {String} symbol BTCUSD by default
 * @apiParam {Number} rank 1 by default. It means precision
 * @apiParam {Number} count 20 by default - amount of lines for book
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.orders
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 *
 */

/**
 * @api {socket.io} /v1.0/orders/me/opened Get current user's opened orders
 *
 * @apiPermission auth
 *
 * @apiDescription Return array of opened orders of this user
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.orders
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/orders/me/opened', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  orders: [
 *                      {
 *                          symbol: 'BTCUSD',
 *                          amount: '2.3',
 *                          price: '1000',
 *                          type: 'buy',
 *                          ....
 *                      }
 *                  ]
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/orders/me/closed Get current user's closed orders
 *
 * @apiPermission auth
 *
 * @apiDescription Return array of closed orders of this user
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.orders
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/orders/me/closed', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  orders: [
 *                      {
 *                          symbol: 'BTCUSD',
 *                          amount: '2.3',
 *                          price: '1000',
 *                          type: 'buy',
 *                          ....
 *                      }
 *                  ]
 *              }
 *         );
 */

/**
 * @api {socket.io} /v1.0/trades/history/current Get current trades history
 *
 * @apiPermission allUsers
 *
 * @apiDescription When new client is connecting you should call this request. In answer you receive list of closed
 * orders for current moment. After this you can listen stream: /trades/history/update
 *
 * @apiVersion 1.0.0
 * @apiGroup ORDERS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {String} symbol default - BTCUSD
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.history
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/trades/history/current', {extID: 'extID', symbol: 'BTCUSD'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  history: [
 *                      {
 *                          symbol: 'BTCUSD',
 *                          amount: '2.3',
 *                          price: '1010',
 *                          status: 'close',
 *                          type: 'sell',
 *                          timeClose: '46546464'
 *                      }
 *                  ]
 *              }
 *         );
 */

/***************************  QUOTES methods  ************************/

/**
 * @api {socket.io} /v1.0/quotes/current Get current values of all currency pair
 *
 * @apiPermission allUsers
 *
 * @apiDescription When new client is connecting you should call this request. In answer you receive current values of all
 * currency pair. After this you can listen stream: /quotes/update
 *
 * @apiVersion 1.0.0
 * @apiGroup QUOTES
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.extID
 * @apiSuccess {Array} data.quotes
 * @apiSuccess {String} data.quotes.symbol
 * @apiSuccess {Number} data.quotes.baseVolume
 * @apiSuccess {Number} data.quotes.quoteVolume
 * @apiSuccess {Number} data.quotes.currentPrice
 * @apiSuccess {String} data.quotes.percentChange
 * @apiSuccess {Number} data.quotes.high
 * @apiSuccess {Number} data.quotes.low
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example emit:
 *     socket
 *         .emit('/v1.0/quotes/current', {extID: 'extID'})
 *         .on('/response', res => {
 *              status: 200,
 *              data: {
 *                  extID: 'extID',
 *                  quotes: [
 *                      {
 *                          symbol: 'BTCUSD',
 *                          baseVolume: 234,
 *                          quoteVolume: 235675,
 *                          currentPrice: 2147,
 *                          high: 2150.3,
 *                          low: 2136.7,
 *                          percentChange: '-23.6'
 *                      },
 *                      {
 *                          symbol: 'LTCUSD',
 *                          baseVolume: 0,
 *                          quoteVolume: 0,
 *                          currentPrice: 0,
 *                          high: 0,
 *                          low: 0,
 *                          percentChange: 0
 *                      }
 *                  ]
 *              }
 *         );
 */

/**
 * @api {GET} /v1.0/quotes/chart/history Get quotes history
 *
 * @apiPermission allUsers
 *
 * @apiVersion 1.0.0
 * @apiGroup QUOTES
 *
 * @apiParam {String} symbol
 * @apiParam {String} resolution ["5", "15", "30", "120", "240", "1D"]
 * @apiParam {Timestamp} from without ms
 * @apiParam {Timestamp} to without ms
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.s 'ok' - status
 * @apiSuccess {Array} data.c array of close prices
 * @apiSuccess {Array} data.o array of open prices
 * @apiSuccess {Array} data.h array of highest prices
 * @apiSuccess {Array} data.l array of lowest prices
 * @apiSuccess {Array} data.t array of times
 * @apiSuccess {Array} data.v array of volume
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/quotes/chart/config Get chart config
 *
 * @apiPermission allUsers
 *
 * @apiVersion 1.0.0
 * @apiGroup QUOTES
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data JSON config
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/quotes/chart/search Get all symbols
 *
 * @apiPermission allUsers
 *
 * @apiVersion 1.0.0
 * @apiGroup QUOTES
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data JSON all symbols
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/quotes/chart/symbols Get symbol details
 *
 * @apiPermission allUsers
 *
 * @apiVersion 1.0.0
 * @apiGroup QUOTES
 *
 * @apiParam {String} symbol Default 'BTCUSD'
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data JSON symbol info
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/************************  TRANSACTIONS methods  *********************/

/**
 * @api {socket.io} /v1.0/withdrawals/me Create request for withdrawal
 *
 * @apiPermission auth
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiParam {String} extID unique id of request to identify response
 * @apiParam {Number} amount
 * @apiParam {String} address
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {PUT} /v1.0/withdrawals Update withdrawal and user's balance
 *
 * @apiPermission system
 *
 * @apiDescription This method is called from scheduler service for updating withdrawals in database and user's balances.
 * Should be callse with system-token.
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiHeader {String} X-Token It should be special system token for system-user with role 6
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/withdrawals/list Get list of withdrawals (for scheduler service)
 *
 * @apiPermission system
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {socket.io} /v1.0/deposits/me Get address for deposit
 *
 * @apiPermission auth
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiParam {String} extID unique id of request to identify response
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.address
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {PUT} /v1.0/deposits Update deposit and user's balance
 *
 * @apiPermission system
 *
 * @apiDescription This method is called from scheduler service for updating deposits in database and user's balances.
 * Should be callse with system-token.
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiHeader {String} X-Token It should be special system token for system-user with role 6
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {GET} /v1.0/deposits/list Get list of deposits
 *
 * @apiPermission system
 *
 * @apiDescription This method is called from scheduler service for getting deposits in database.
 * Should be callse with system-token.
 *
 * @apiVersion 1.0.0
 * @apiGroup TRANSACTIONS
 *
 * @apiHeader {String} X-Token It should be special system token for system-user with role 6
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/****************************  CRM methods  *************************/

/**
 * @api {POST} /v1.0/crm/withdrawals/approve Approve withdrawal(for crm)
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiParam {String} id  of withdrawal
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {POST} /v1.0/crm/currencies Get list of all platform's currencies
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {Array} data.currencies
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 * @apiExample {js} Example response:
 *  "status": 200,
 *  "data": {
 *    "currencies": [
 *      "BTC",
 *      "DSH",
 *      "LTC",
 *      "ETH",
 *      "USD",
 *      "XRP"
 *     ]
 *   }
 */

/**
 * @api {POST} /v1.0/crm/email/send Send email to user through platform
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiParam {String} text
 * @apiParam {String} subject
 * @apiParam {String} to email
 * @apiParam {String} file
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {POST} /v1.0/crm/orders Get all closed orders(for crm)
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Array} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {POST} /v1.0/crm/user/docs Get documents for specific user(for crm)
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiParam {String} email
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {String} data.photoPassport
 * @apiSuccess {String} data.photoVerification
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {POST} /v1.0/crm/user/verified Verify or decline user
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiParam {String} success 0 - reject user's verification, 1 - approved
 * @apiParam {String} email
 * @apiParam {String} reason if rejected
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 * @apiSuccess {Boolean} success true
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

/**
 * @api {POST} /v1.0/crm/user/balance Get balance of user
 *
 * @apiPermission crm
 *
 * @apiVersion 1.0.0
 * @apiGroup CRM
 *
 * @apiHeader {String} X-Token It should be special crm token for crm-user with role 3
 *
 * @apiParam {String} login id of account
 *
 * @apiSuccess {Number} status 200
 * @apiSuccess {Object} data
 *
 * @apiError status not 200. Can be 400, 401, 403, 404, 500
 * @apiError data
 * @apiError data.code number of error
 * @apiError data.message text description of error
 *
 */

