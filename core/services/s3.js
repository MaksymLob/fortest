
const config = require(__dirroot+'config').connections.s3;

const AWS = require('aws-sdk');
AWS.config.update(config);
const s3 = new AWS.S3({apiVersion: config.apiVersion});

const url = `https://s3.${config.region}.amazonaws.com/${config.bucketName}/`;

const s3service = {

  uploadFile(filename, data, filetype) {
    let type;
    let buf;
    if (Buffer.isBuffer(data)) {
      buf = data;
    } else {
      buf = new Buffer(data.replace(/^data:image\/\w+;base64,/, ''), 'base64');
    }
    if (filetype) {
      type = filetype;
    } else {
      type = data.split(';')[0].replace('data:', '');
    }
    let ext = '.' + type.split('/')[1];
    return this._uploadFile(filename + ext, buf, type);
  },

  _uploadFile(filename, data, type) {
    return new Promise((resolve, reject) => {
      s3.putObject({
        Bucket: config.bucketName,
        Key: filename,
        Body: data,
        ACL: 'private',
        ContentEncoding: 'base64',
        ContentType: type,
        Metadata: {'ContentType': type}
      }, (err, data) => {
        if (err) {
          log.e(err);
          return reject('S3_UPLOADING_ERROR');
        }
        return resolve(url + filename);
      });
    });
  },

  removeImage(filename) {
    return new Promise((resolve, reject) => {
      s3.deleteObject({
        Bucket: config.bucketName,
        Key: fileName
      }, (err, res) => {
        if (err) {
          log.e(err);
          return reject('S3_DELETING_ERROR');
        }
        return resolve(true);
      });
    });
  },

};

module.exports = s3service;
