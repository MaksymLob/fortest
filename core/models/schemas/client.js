

const constants = __app('libs/constants');
const countries = __app('libs/countries.json').countries;
let countryCodes = [];

countries.forEach(item => {
  countryCodes.push(item.code);
});

const client = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  email: {
    type: 'string',
    allowNull: false,
    unique: true
  },
  password: {
    type: 'string',
    allowNull: false
  },
  phone: {
    type: 'integer',
    allowNull: false
  },
  country: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [countryCodes]}
  },
  firstname: {
    type: 'string',
    allowNull: true
  },
  lastname: {
    type: 'string',
    allowNull: true
  },
  address: {
    type: 'string',
    allowNull: true
  },
  city: {
    type: 'string',
    allowNull: true
  },
  state: {
    type: 'string',
    allowNull: true
  },
  postalCode: {
    type: 'string',
    allowNull: true
  },
  birthday: {
    type: 'string',
    allowNull: true
  },
  passportId: {
    type: 'string',
    allowNull: true
  },
  photoPassport: {
    type: 'string',
    allowNull: true
  },
  photoVerification: {
    type: 'string',
    allowNull: true
  },
  active: {
    type: 'boolean',
    defaultValue: false
  },
  key2fa: {
    type: 'string',
    allowNull: true
  },
  active2fa: {
    type: 'boolean',
    defaultValue: false
  },
  dayUsdLimit: {
    type: 'double',
    allowNull: false,
    defaultValue: constants.WITHDRAWAL_LIMITS.NOT_VERIFIED
  },
  remainedDayUsd: {
    type: 'double',
    allowNull: false,
    defaultValue: constants.WITHDRAWAL_LIMITS.NOT_VERIFIED
  },
  verified: {
    type: 'string',
    defaultValue: constants.VERIFICATION.NOT
  },
  role: {
    type: 'integer',
    defaultValue: 0
  }
};

module.exports = {
  schema: client,
  validations: {
    verification: {
      address: {type: 'string', allowNull: false},
      city: {type: 'string', allowNull: false},
      state: {type: 'string', allowNull: false},
      postalCode: {type: 'string', allowNull: false},
      phone: {type: 'integer', allowNull: false},
      birthday: {type: 'string', allowNull: false, validateData: {regexp: /^\d\d.\d\d.\d\d\d\d$/}},
      passportId: {type: 'string', allowNull: false},
      photoPassport: {type: 'string', allowNull: false},
      photoVerification: {type: 'string', allowNull: false},
    }
  }
};
