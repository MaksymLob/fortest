
const SystemModel = {};

const systemSchema = __app('models/schemas/system');
const Model = __app('models/model');
const schema = new Model(systemSchema.schema);
SystemModel.validations = systemSchema.validations;
const systemModel = schema.defineDataBase('mysql-redis', 'system', SystemModel);

module.exports = systemModel;
