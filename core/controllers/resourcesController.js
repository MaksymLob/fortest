const config = require(__dirroot + 'config.json');
const countriesData = __app('libs/countries.json').countries;
const currencyReductions = __app('libs/cryptoDefinitions/currencyReductions');
const symbolReductions = __app('libs/cryptoDefinitions/symbolReductions');

const ResourcesCtrl = {
  getDocs(req, res) {
    res.sendFile(__dirroot + 'apidoc/deploy/index.html');
  },

  getCommonResources(req, res) {
    let countries = [];
    countriesData.forEach(item => {
      countries.push({
        code: item.code,
        name: item.name
      });
    });
    res.answer({
      status: 200, data: {
        countries: countries,
        currencies: currencyReductions,
        symbols: symbolReductions
      }
    });
  },

  getCurrencies(req, res) {
    return res.answer({
      status: 200,
      data: {
        code: 200,
        data: {
          currencies: Object.keys(config.currencies)
        }
      }
    });
  }
};

module.exports = ResourcesCtrl;
