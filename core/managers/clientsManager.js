
const crypto = require('crypto');
const authenticator = require('authenticator');
const config = require(__dirroot + 'config');
const GE = __app('libs/GE');
const constants = __app('libs/constants');
const countries = __app('libs/countries.json').countries;

const clientModel = __app('models/clientModel');
const accountModel = __app('models/accountModel');
const accountMngr = __app('managers/accountsManager');
const accountKeyModel = __app('models/accountKeyModel');
const reCaptcha = __app('services/reCaptcha');
const crm = __app('services/crm');
const s3 = __app('services/s3');
const Mailer = __app('services/mailer');
const mailer = new Mailer();
const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();
const scheduler = __app('services/scheduler');

const BTC_LIKE_CRYPTOS = ['BTC', 'LTC', 'DSH'];
let countryCodes = [];
countries.forEach(item => {
    countryCodes.push(item.code);
});


const UserManager = {

  createSystemAccounts(data) {
    return clientModel.getAll({where: {role: {$or: [3, 5, 6]}, active: 0}})
      .then(res => {
        let actions = res.map(user => {
          user.clientId = user.id;
          return this._createAccounts(user, constants.ROLE_IS_SYSTEM);
        });
        return Promise.all(actions);
      })
      .then(res => {
        return res;
      });
  },

  reimportPrivateKeys() {
    return accountKeyModel.getAll({where: {currency: {$or: BTC_LIKE_CRYPTOS}, privateKey: {$not: null}}})
      .then(res => {
        const loop = i => {
          scheduler.importPrivKey(res[i].currency, res[i].privateKey, res[i].id)
            .then(() => i > res.length - 1 || loop(i + 1));
        };
        loop(0);
      });
  },

  addNewUser(data) {
    let user;
    if (data.country) {
      data.country = data.country.toUpperCase();
    }
    data.phone = Date.now();
    return reCaptcha.validateReq(data)
      .then(() => {
        return clientModel.validateData(data);
      })
      .then(res => {
        return clientModel.checkExistingEmail(res);
      })
      .then(res => {
        res.password = this._getHashPassword(res.password);
        return clientModel.create(res);
      })
      .then(res => {
        user = res.get();
        return this._sendNewUserEmail(user.email, user.id, data.url, data.template);
      });
  },

  addNewReferral(data) {
    if (config.registration === 'referral') {
      return this.checkReferrerId(data.referrer)
        .then(res => {
          if (res) {
            delete data.referrer;
            return this.addNewUser(data);
          } else {
            return Promise.reject('VALIDATION_INVALID_REFERRER');
          }
        });
    } else {
      return this.addNewUser(data);
    }
  },

  addNewUserFromCrm(data) {
    let user;
    if (!data.password) {
      data.password = this._generateNewPassword();
    }
    if (data.country) {
      data.country = data.country.toUpperCase();
    }
    return clientModel.validateData(data)
      .then(res => {
        return clientModel.checkExistingEmail(res);
      })
      .then(res => {
        res.password = this._getHashPassword(res.password);
        res.active = true;
        return clientModel.create(res);
      })
      .then(res => {
        user = res.get();
        user.clientId = user.id;
        return accountMngr.createAccounts(user);
      })
      .then(res => {
        let response;
        let index;
        res.forEach((account, i) => {
          if (account.currency === data.currency) {
            response = account.accountId;
            index = i;
          }
        });
        if (index || index === 0) {
          res.splice(index, 1);
        }
        if (process.env.NODE_ENV !== 'test') {
          this._sendToCrm(user, res, 0);
        }
        return {externalId: response ? response : res[0].accountId};
      });
  },

  confirmRegistration(data) {
    if (!data.clientHash) {
      return Promise.reject('NOT_FOUND_CLIENT');
    }
    const decrypted = this._decrypt(data.clientHash);
    if (decrypted === null) {
      return Promise.reject('INVALID_HASH');
    }
    const valuesArray = decrypted.split('/');
    if (valuesArray.length < 2) {
      return Promise.reject('INVALID_HASH');
    }
    const id = valuesArray[0];
    const date = valuesArray[1];
    if (new Date(date).getTime() <= 0) {
      return Promise.reject('INVALID_HASH');
    }
    if (Date.now() - date > config.connections.mail.confirmationTime) {
      return Promise.reject('CONFIRMATION_TIME_IS_UP');
    }

    let user;
    return clientModel.findOne({where: {id}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        user = res.get();
        if (user.active) {
          return Promise.reject('USER_ALREADY_ACTIVATED');
        }
        user.clientId = user.id;
        return this._createAccounts(user);
      });
  },

  login(data) {
    let client;
    return reCaptcha.validateReq(data)
      .then(() => {
        return this._validateLoginData(data);
      })
      .then(() => {
        return clientModel.findOne({where: {email: data.email},
          attributes: ['id', 'password', 'email', 'firstname', 'lastname', 'phone', 'country', 'active', 'active2fa', 'key2fa', 'verified']
        });
      })
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        if (!res.active) {
          return Promise.reject('USER_IS_NOT_ACTIVATED');
        }
        return this._checkPassword(client.password, data.password);
      })
      .then(() => {
        return accountModel.getClientAccounts(client.id);
      })
      .then(res => {
        if (!client.key2fa || !client.active2fa) {
          client.authType = '1fa';
          if (process.env.NODE_ENV !== 'test') {
            mailer.sendNotificationOfLogging({
              email: data.email,
              loginTime: new Date(),
              ip: data.ip,
              userAgent: data.userAgent
            });
            crm.notifyLogin({email: data.email});
          }
        } else {
          client.authType = '2fa';
        }
        client.accounts = res;
        return client;
      });
  },

  getUsersDetailInfo(clientId) {
    return clientModel.findById(clientId, {
      attributes: ['email', 'firstname', 'lastname', 'address', 'country', 'phone', 'city', 'state', 'postalCode', 'birthday', 'passportId', 'photoPassport', 'photoVerification', 'verified']
    })
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        return res.get();
      });
  },

  verifyUser(data) {
    let client;
    let dataForUpdate = {};
    return clientModel.validateVerificationData(data)
      .then(res => {
        return clientModel.checkExistingPhone(res);
      })
      .then(() => {
        return clientModel.findById(data.clientId);
      })
      .then(res => {
        client = res.get();
        if (client.verified === constants.VERIFICATION.YES) {
          return Promise.reject('USER_ALREADY_VERIFIED');
        }
        if (client.verified === constants.VERIFICATION.AWAITING) {
          return Promise.reject('WAITING_VERIFICATION_CONFIRMATION');
        }
        return this._uploadDocs({email: client.email, photoPassport: data.photoPassport, photoVerification: data.photoVerification});
      })
      .then(res => {
        data.birthday = data.birthday.split('/');
        dataForUpdate = {
          firstname: client.firstname,
          lastname: client.lastname,
          address: data.address,
          city: data.city,
          state: data.state,
          postalCode: data.postalCode,
          phone: data.phone,
          birthday: data.birthday[2] + '-' + data.birthday[1] + '-' + data.birthday[0],
          passportId: data.passportId,
          photoPassport: res.photoPassport,
          photoVerification: res.photoVerification,
          verified: constants.VERIFICATION.AWAITING
        };
        return clientModel.update(dataForUpdate, {where: {id: data.clientId}});
      })
      .then(() => {
        return accountKeyModel.findOne({where: {clientId: data.clientId, currency: 'BTC'}});
      })
      .then(res => {
        dataForUpdate.login = res.get('id');
        dataForUpdate.email = client.email;
        dataForUpdate.currency = 'BTC';
        dataForUpdate.zip = data.postalCode;
        dataForUpdate.verified = undefined;
        dataForUpdate.postalCode = undefined;
        dataForUpdate.country = client.country;
        dataForUpdate.phone = data.phone;
        dataForUpdate.source = data.role;
        return crm.sendAddOrUpdateClient(dataForUpdate);
      })
      .catch(err => {
        if (dataForUpdate.email) {
          this._rejectVerification({email: client.email, reason: err});
        }
        throw err;
      });
  },

  getUsersDocs(data) {
    return clientModel.findOne({where: {email: data.email}}).then(res => {
      if (!res) {
        return Promise.reject('NOT_FOUND_CLIENT');
      }
      return {
        photoPassport: res.get('photoPassport'),
        photoVerification: res.get('photoVerification')
      };
    });
  },

  confirmVerification(data) {
    if (data.success == 0) {
      return this._rejectVerification(data);
    } else {
      return this._approveVerification(data);
    }
  },

  login2FA(data, access) {
    if (!data.code) {
      return Promise.reject('VALIDATION_2FA_CODE_IS_NOT_FILLED');
    }
    let client;
    return clientModel.findOne({where: {id: access.clientId}, attributes: ['id', 'password', 'email', 'firstname', 'lastname', 'phone', 'country', 'active', 'active2fa', 'key2fa', 'verified']})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        // if (/*process.env.NODE_ENV === 'test' && */data.code === 111223) {
        //   return;
        // }
        return this._check2FA(client.key2fa, data.code);
      })
      .then(() => {
        return accountModel.getClientAccounts(client.id);
      })
      .then(res => {
        client.accounts = res;
        client.authType = '2fa';
        if (data.ip) {
          let ar = data.ip.split(':');
          data.ip = ar[ar.length - 1];
        }
        if (process.env.NODE_ENV !== 'test') {
          mailer.sendNotificationOfLogging({
            email: data.email,
            loginTime: new Date(),
            ip: data.ip,
            userAgent: data.userAgent
          });
          crm.notifyLogin({email: data.email});
        }
        return client;
      });
  },

  activate2FA(data, access) {
    let client;
    let formattedKey;
    if (!data.password) {
      return Promise.reject('VALIDATION_PASSWORD_IS_NOT_FILLED');
    }
    return clientModel.findOne({where: {id: access.clientId}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        if (client.key2fa && client.active2fa === true) {
          return Promise.reject('USER_ALREADY_HAS_2FA');
        }
        return this._checkPassword(client.password, data.password);
      })
      .then(() => {
        formattedKey = authenticator.generateKey();
        return clientModel.update({key2fa: formattedKey}, {where: {id: access.clientId}});
      })
      .then(() => {
        return {
          key: formattedKey,
          OTPAuth: authenticator.generateTotpUri(formattedKey, client.email, config.companyName, 'SHA1', 6, 30)
        };
      });
  },

  confirmActivate2FA(data, access) {
    let client;
    if (!data.code) {
      return Promise.reject('VALIDATION_2FA_CODE_IS_NOT_FILLED');
    }
    return clientModel.findOne({where: {id: access.clientId}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        if (!client.key2fa) {
          return Promise.reject('USER_HAS_NO_2FA');
        }
        if (client.active2fa === true) {
          return Promise.reject('USER_ALREADY_HAS_2FA');
        }
        // if (/*process.env.NODE_ENV === 'test' && */data.code === 111223) {
        //   return;
        // }
        return this._check2FA(client.key2fa, data.code);
      })
      .then(() => {
        return clientModel.update({active2fa: true}, {where: {id: access.clientId}});
      });
  },

  deactivate2FA(data, access) {
    let client;
    if (!data.password) {
      return Promise.reject('VALIDATION_PASSWORD_IS_NOT_FILLED');
    }
    if (!data.code) {
      return Promise.reject('VALIDATION_2FA_CODE_IS_NOT_FILLED');
    }
    return clientModel.findOne({where: {id: access.clientId}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        if (!client.key2fa) {
          return Promise.reject('USER_HAS_NO_2FA');
        }
        return Promise.all([
          this._checkPassword(client.password, data.password),
          this._check2FA(client.key2fa, data.code)
        ]);
      })
      .then(() => {
        return clientModel.update({key2fa: null, active2fa: false}, {where: {id: access.clientId}});
      });
  },

  getUserInfo(data) {
    let client;
    return clientModel.findById(data.clientId, {attributes: ['id', 'email', 'phone', 'country', 'firstname', 'lastname', 'active', 'active2fa', 'verified']})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        return accountModel.getClientAccounts(data.clientId);
      })
      .then(res => {
        client.accounts = res;
        return client;
      });
  },

  updateUserFromCrm(data) {
    let client;
    data.password = 'q';
    if (data.country) {
      data.country = data.country.toUpperCase();
    }
    return clientModel.validateData(data)
      .then(() => {
        return clientModel.findOne({where: {email: data.email}});
      })
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        client = res.get();
        if (data.country) client.country = data.country;
        if (data.address) client.address = data.address;
        if (data.city) client.city = data.city;
        if (data.firstname) client.firstname = data.firstname;
        if (data.zip) client.postalCode = data.zip;
        if (data.name) client.firstname = data.name;
        if (data.lastname) client.lastname = data.lastname;
        if (data.phone) client.phone = data.phone;
        return clientModel.update(client, {where: {email: client.email}});
      })
      .then(() => {
        return {externalId: data.login};
      });
  },

  getDayLimits(data) {
    return clientModel.findOne({where: {id: data.clientId}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        return {
          dayUsdLimit: res.get('dayUsdLimit'),
          remainedDayUsd: res.get('remainedDayUsd')
        };
      });
  },

  updateDailyLimits(data) {
    let client;
    return clientModel.findById(data.clientId)
      .then(res => {
        client = res.get();
        let currencyPrice = quotes.getCurrentQuote({symbol: `${data.currency}USD`}).currentPrice;
        client.remainedDayUsd = Number((client.remainedDayUsd + Number(data.amount * currencyPrice)).toFixed(2));
        return clientModel.update({remainedDayUsd: client.remainedDayUsd}, {where: {id: client.id}});
      })
      .then(() => {
        GE.emit('/limits/me/update', {
          dayUsdLimit: client.dayUsdLimit,
          remainedDayUsd: client.remainedDayUsd,
          clientId: client.id
        });
        return client;
      });
  },

  forgotPassword(data) {
    if (!data.email) {
      return Promise.reject('VALIDATION_EMAIL_IS_NOT_FILLED');
    }
    return clientModel.findOne({where: {email: data.email}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        const hash = this._encrypt(res.get('id'));
        return mailer.sendRecoveryPassword(data.email, hash);
      });
  },

  resetPassword(data) {
    if (!data.password) {
      return Promise.reject('VALIDATION_PASSWORD_IS_NOT_FILLED');
    }
    if (!data.hash) {
      return Promise.reject('VALIDATION_HASH_IS_NOT_FILLED');
    }
    let decrypted = this._decrypt(data.hash);
    if (decrypted === null) {
      return Promise.reject('INVALID_HASH');
    }
    const valuesArray = decrypted.split('/');
    if (valuesArray.length < 2) {
      return Promise.reject('INVALID_HASH');
    }
    const id = valuesArray[0];
    const date = valuesArray[1];
    if (new Date(date).getTime() <= 0) {
      return Promise.reject('INVALID_HASH');
    }
    if (Date.now() - date > config.connections.mail.confirmationTime) {
      return Promise.reject('CONFIRMATION_TIME_IS_UP');
    }

    let user;
    return clientModel.findOne({where: {id}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        user = res.get();
        user.password = this._getHashPassword(data.password);
        return clientModel.update(user, {where: {id: id}});
      });
  },

  getReferrerId(clientId) {
    let cipher = crypto.createCipher('aes256', config.secret_salt);
    let crypted = cipher.update(`ref#${clientId}`, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return Promise.resolve(crypted);
  },

  checkReferrerId(id) {
    try {
      let decipher = crypto.createDecipher('aes256', config.secret_salt);
      let dec = decipher.update(id, 'hex', 'utf8');
      dec += decipher.final('utf8');
      log.d(dec);
      return clientModel.findOne({where: {id: dec.replace('ref#', '')}});
    } catch (err) {
      return Promise.resolve(false);
    }
  },

  // ****************************  PRIVATE METHODS  ***************************

  _validateLoginData(data) {
    return new Promise((resolve, reject) => {
      if (!data.email) {
        return reject('VALIDATION_EMAIL_IS_NOT_FILLED');
      } else if (!data.password) {
        return reject('VALIDATION_PASSWORD_IS_NOT_FILLED');
      } else {
        if (data.ip) {
          let ar = data.ip.split(':');
          data.ip = ar[ar.length - 1];
        }
        return resolve(data);
      }
    });
  },

  _sendNewUserEmail(email, id, url, template) {
    const hash = this._encrypt(id);
    if (process.env.NODE_ENV === 'test') {
      return Promise.resolve(hash);
    }
    return mailer.sendConfirmationMsg(email, hash, url, template);
  },

  _encrypt(value) {
    let cipher = crypto.createCipher('aes192', config.secret_salt);
    let crypted = cipher.update(`${value}/${Date.now()}`, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  },

  _decrypt(value) {
    try {
      let decipher = crypto.createDecipher('aes192', config.secret_salt);
      let dec = decipher.update(value, 'hex', 'utf8');
      dec += decipher.final('utf8');
      return dec;
    } catch (err) {
      return null;
    }
  },

  _generateNewPassword() {
    return Math.random().toString(36).substr(2, 12);
  },

  _getHashPassword(pswd) {
    return crypto.createHash('sha256').update(pswd + config.secret_salt).digest('hex');
  },

  _checkPassword(hashed, pswd) {
    let hashPswd = this._getHashPassword(pswd);
    if (hashPswd !== hashed) {
      return Promise.reject('VALIDATION_WRONG_PASSWORD');
    }
    return Promise.resolve(true);
  },

  _check2FA(formattedKey, formattedToken) {
    const result = authenticator.verifyToken(formattedKey, formattedToken.toString());
    if (result == null) {
      return Promise.reject('VALIDATION_WRONG_2FA_CODE');
    } else {
      return Promise.resolve();
    }
  },

  _sendToCrm(client, accounts, index, role = constants.ROLE_NOT_AUTH) {
    let user = {
      login: accounts[index].accountId,
      email: client.email,
      currency: accounts[index].currency,
      country: client.country,
      phone: client.phone,
      source: role
    };
    if (client.firstname) user.firstname = client.firstname;
    if (client.lastname) user.lastname = client.lastname;
    return crm.sendAddOrUpdateClient(user)
      .then(() => {
        if (index === accounts.length - 1) return;
        return this._sendToCrm(client, accounts, index + 1, role);
      });
  },

  _uploadDocs({email: email, photoPassport: photoPassport, photoVerification: photoVerification}) {
    return Promise.all([
      s3.uploadFile(email + '-passport', photoPassport),
      s3.uploadFile(email + '-photo', photoVerification)
    ]).then(res => {
      return {
        photoPassport: res[0],
        photoVerification: res[1]
      };
    });
  },

  _rejectVerification(data) {
    return clientModel.findOne({where: {email: data.email}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        let dataForUpdate = {
          address: '',
          city: '',
          state: '',
          postalCode: '',
          birthday: '',
          passportId: '',
          photoPassport: '',
          photoVerification: '',
          verified: constants.VERIFICATION.NOT
        };
        return clientModel.update(dataForUpdate, {where: {email: data.email}});
      })
      .then(() => {
        mailer.rejectVerification({email: data.email, reason: data.reason});
      });
  },

  _approveVerification(data) {
    let client;
    return clientModel.findOne({where: {email: data.email}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_CLIENT');
        }
        const dataForUpdate = {
          verified: constants.VERIFICATION.YES,
          dayUsdLimit: constants.WITHDRAWAL_LIMITS.VERIFIED,
          remainedDayUsd: constants.WITHDRAWAL_LIMITS.VERIFIED - constants.WITHDRAWAL_LIMITS.NOT_VERIFIED + res.get('remainedDayUsd')
        };
        client = {
          id: res.id,
          dayUsdLimit: dataForUpdate.dayUsdLimit,
          remainedDayUsd: dataForUpdate.remainedDayUsd
        };
        return clientModel.update(dataForUpdate, {where: {email: data.email}});
      })
      .then(() => {
        GE.emit('/limits/me/update', {
          dayUsdLimit: client.dayUsdLimit,
          remainedDayUsd: client.remainedDayUsd,
          clientId: client.id
        });
        mailer.approveVerification({email: data.email});
      });
  },

  _createAccounts(user, role) {
    return accountMngr.createAccounts(user)
      .then(res => {
        return this._sendToCrm(user, res, 0, role);
      })
      .then(() => {
        return clientModel.update({active: true}, {where: {id: user.id}});
      })
      .then(() => {
        return user;
      });
  }

};

module.exports = UserManager;
