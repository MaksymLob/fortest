
module.exports = {
  /* **************************  Common controller  *************************/

  'GET/v1.0/status': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'systemController',
    action: 'getPlatformStatus',

    name: 'Checking status of platform',
    group: 'SYSTEM',
    description: 'This method check connections to databases. Return status 200 if connections to mysql and redis are established, return 500 if one of them has error',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/accounts/check': {
    access: ['isSystem'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'checkAllAccounts',

    name: 'Checking if clients haven`t any currency-accounts',
    group: 'SYSTEM',
    description: 'This method should be called after adding new currencies into config file. Method get all clients and check: if client hasn`t someone account, then this accounts is created',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
    },
    headers: { // TODO @apiHeader
      'X-Token': {
        type: 'String',
        description: 'X-Token It should be special system token for system-user with role 6'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.success': {
        type: 'Boolean'
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/init': {
    access: ['isSystem'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'createSystemAccounts',

    name: 'Initialization of system clients (create accounts for them)',
    group: 'SYSTEM',
    description: 'Description This method should be called only once time after the first running of application.Clients are created by migrations, but also it need create accounts for these clients. Current method get all system clients (role 5, 6, 3) and create accounts for them if it not exists. If return empty array then clients already initialized.If array is not empty then initialization was success',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
    },
    headers: { // TODO @apiHeader
      'X-Token': {
        type: 'String',
        description: 'X-Token It should be special system token for system-user with role 6'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/accounts/check-redis-accounts': {
    access: ['isSystem'],
    tokenRequired: false,
    controllerName: 'systemController',
    action: 'createRedisAccountsFromMysql',

    name: 'Create accounts in Redis from MySQL',
    group: 'SYSTEM',
    description: 'Get accounts from MySQL and search them in Redis and app\'s memory. Adds accounts if necessary. Also check balances and correct them.',
    headers: {
      'X-Token': {
        type: 'String',
        description: 'X-Token It should be special system token for system-user with role 6'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.success': {
        type: 'Boolean'
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/reimport-private-keys': {
    access: ['isSystem'],
    tokenRequired: false,
    controllerName: 'systemController',
    action: 'createRedisAccountsFromMysql',

    name: 'Import privateKeys from MySQL',
    group: 'SYSTEM',
    description: 'Import BTC, LTC, DASH private keys from MySQL to blockchains.',
    headers: {
      'X-Token': {
        type: 'String',
        description: 'X-Token It should be special system token for system-user with role 6'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.success': {
        type: 'Boolean'
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/update_private_keys': {
    access: ['isSystem'],
    tokenRequired: false,
    controllerName: 'systemController',
    action: 'createRedisAccountsFromMysql',

    name: 'Update privateKeys from MySQL',
    group: 'SYSTEM',
    description: 'Import BTC, LTC, DASH private keys from MySQL (from accounts table) to blockchains.',
    headers: {
      'X-Token': {
        type: 'String',
        description: 'X-Token It should be special system token for system-user with role 6'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.success': {
        type: 'Boolean'
      }
    },
    apiuse: ['defErrorResp']
  }
};
