
const ClientModel = {
  classMethods: {
    checkExistingEmail(data) {
      return this.findAll({where: {email: data.email}})
        .then(user => {
          if (user.length) {
            return Promise.reject('VALIDATION_EMAIL_ALREADY_EXIST');
          }
          return Promise.resolve(data);
        });
    },

    checkExistingPhone(data) {
      return this.findAll({where: {phone: data.phone}})
        .then(user => {
          if (user.length) {
            return Promise.reject('VALIDATION_PHONE_ALREADY_EXIST');
          }
          return Promise.resolve(data);
        });
    },

    checkWithdrawalLimit(clientId, amount, price) {
      let limit;
      return this.findOne({where: {id: clientId}})
        .then(res => {
          limit = res.get('remainedDayUsd');
          if (price * amount > limit) {
            return Promise.reject('VALIDATION_WITHDRAWAL_EXCEEDS_LIMIT');
          } else {
            return {remainedDayUsd: limit, dayUsdLimit: res.get('dayUsdLimit')};
          }
        });
    },

    validateVerificationData(data) {
      return this.validateData(data, 'verification').then(res => {
        if (-1 === data.photoPassport.indexOf('data:image/jpeg;base64,')
          && -1 === data.photoPassport.indexOf('data:image/jpg;base64,')
          && -1 === data.photoPassport.indexOf('data:image/png;base64,')
          && -1 === data.photoPassport.indexOf('data:image/bmp;base64,')
        ) {
          return Promise.reject('VALIDATION_PHOTOPASSPORT_HAS_INCORRECT_TYPE');
        }
        if (-1 === data.photoVerification.indexOf('data:image/jpeg;base64,')
          && -1 === data.photoVerification.indexOf('data:image/jpg;base64,')
          && -1 === data.photoVerification.indexOf('data:image/png;base64,')
          && -1 === data.photoVerification.indexOf('data:image/bmp;base64,')
        ) {
          return Promise.reject('VALIDATION_PHOTOVERIFICATION_HAS_INCORRECT_TYPE');
        }

        if (new Buffer(data.photoPassport, 'base64').length > 2097150) {
          return Promise.reject('VALIDATION_TOO_BIG_SIZE');
        }
        if (new Buffer(data.photoVerification, 'base64').length > 2097150) {
          return Promise.reject('VALIDATION_TOO_BIG_SIZE');
        }
        return res;
      });
    }
  }
};

const clientSchema = __app('models/schemas/client');
const Model = __app('models/model');
const schema = new Model(clientSchema.schema);
ClientModel.validations = clientSchema.validations;
const clientModel = schema.defineDataBase('mysql', 'clients', ClientModel);

module.exports = clientModel;
