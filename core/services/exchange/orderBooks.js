

const config = require(__dirroot + 'config');
let Redis = __app('services/redis');
Redis = new Redis();
const redis = Redis.connection;
let BOOKS = {};

config.symbols.forEach(symbol => BOOKS[symbol] = {buy: {}, sell: {}});

redis.keys('orders:id:*', (err, res) => {
  let count = 0;
  res.forEach(item => {
    redis.hgetall(item, (err, order) => {
      let price = Number(order.price).toFixed(8);
      if (BOOKS[order.symbol][order.type][price]) {
        BOOKS[order.symbol][order.type][price].count++;
        BOOKS[order.symbol][order.type][price].amount = Number((BOOKS[order.symbol][order.type][price].amount + Number(order.amount)).toFixed(8));
      } else {
        BOOKS[order.symbol][order.type][price] = {
          count: 1,
          amount: Number(Number(order.amount).toFixed(8)),
          total: 0,
          price: price,
          symbol: order.symbol,
          type: order.type
        };
      }
      count++;
      if (count == res.length) {
        config.symbols.forEach(symbol => {
          let buy = orderBooks.recalcBooks({type: 'buy', symbol: symbol});
          let sell = orderBooks.recalcBooks({type: 'sell', symbol: symbol});
        });
      }
    });
  });
});

const orderBooks = {

  addToBook(order) {
    let price = order.price.toFixed(8);
    let oldLine = BOOKS[order.symbol][order.type][price];
    let newLine = {
      price: price,
      symbol: order.symbol,
      type: order.type,
      count: oldLine ? Number(oldLine.count) + 1 : 1,
      amount: oldLine ? Number(oldLine.amount) + order.amount : order.amount,
      total: oldLine ? Number(oldLine.total) + Number(order.amount) : Number(order.amount)
    };
    BOOKS[order.symbol][order.type][price] = newLine;
    return newLine;
  },

  deleteFromBook(order, isPartOfOrder) {
    let price = Number(order.price).toFixed(8);
    if (!BOOKS[order.symbol] || !BOOKS[order.symbol][order.type] || !BOOKS[order.symbol][order.type][price]) {
      return;
    }
    let line = BOOKS[order.symbol][order.type][price];

    line.count = isPartOfOrder ? line.count : line.count - 1;
    line.amount = Number((line.amount - order.amount).toFixed(8));
    line.total = line.amount ? line.total - order.amount : 0;

    if (BOOKS[order.symbol][order.type][price].count == 0) {
      delete BOOKS[order.symbol][order.type][price];
    }
    return BOOKS[order.symbol][order.type][price];
  },

  recalcBooks({type, symbol}) {
    let total = 0;
    let keys = Object.keys(BOOKS[symbol][type]).sort();
    let book = BOOKS[symbol][type];
    let sorted = [];

    if (type == 'buy') {
      for (let i = keys.length - 1; i >= 0; i--) {
        if (!book[keys[i]].count) {
          delete BOOKS[symbol][type][keys[i]];
        }
        book[keys[i]].total = Number((total + Number(book[keys[i]].amount)).toFixed(8));
        total = book[keys[i]].total;
        sorted.push(book[keys[i]]);
      }
    } else {
      for (let i = 0; i < keys.length; i++) {
        if (!book[keys[i]].count) {
          delete BOOKS[symbol][type][keys[i]];
        }
        book[keys[i]].total = Number((total + Number(book[keys[i]].amount)).toFixed(8));
        total = book[keys[i]].total;
        sorted.push(book[keys[i]]);
      }
    }

    return sorted;
  },

  getSortedBook({symbol, type}) {
    let sorted = [];
    let keys = Object.keys(BOOKS[symbol][type]).sort();
    if (type == 'buy') {
      for (let i = keys.length - 1; i >= 0; i--) {
        sorted.push(BOOKS[symbol][type][keys[i]]);
      }
    } else {
      for (let i = 0; i < keys.length; i++) {
        sorted.push(BOOKS[symbol][type][keys[i]]);
      }
    }
    return sorted;
  }

  // ****************************  PRIVATE METHODS  ***************************

};

module.exports = orderBooks;
