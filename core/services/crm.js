
const request = require('request');
const configMain = require(__dirroot+'config');
const config = configMain.connections.CRM;

const checkObjectKeys = (data, keys) => {
  return new Promise((resolve, reject) => {
    keys.forEach(key => {
      if (typeof data[key] === 'undefined') {
        return reject(`VALIDATION_CRM_NO_${key.toUpperCase()}`);
      }
    });
    resolve();
  });
};

const Crm = {

  sendAddOrUpdateClient(data) {
    return checkObjectKeys(data, ['login', 'email', 'phone'])
      .then(() => {
        let url = `${config.protocol}://${config.host}:${config.port}${config.path}/client/check`;
        return this._sendRequest(url, 'POST', data);
      })
      .then(res => {
        log.i('Account id=' + res.account.id + ', currency=' + res.account.currency + ' is saved in CRM');
        return res;
      });
  },

  sendAddOrUpdateDeposit(data) {
    return checkObjectKeys(data, ['id', 'accountId', 'clientId', 'amount', 'payment_system'])
      .then(() => {
        const url = `${config.protocol}://${config.host}:${config.port}${config.path}/client/${data.clientId}/deposit/${data.id}`;
        return this._sendRequest(url, 'POST', data);
      })
      .then(res => {
        log.i(`Deposit id=${data.id}, accountId=${data.accountId}, amount=${data.amount} is saved in CRM`);
        return res;
      });
  },

  sendAddOrUpdateWithdrawal(data) {
    return checkObjectKeys(data, ['id', 'accountId', 'clientId', 'status', 'amount'])
      .then(() => {
        const url = `${config.protocol}://${config.host}:${config.port}${config.path}/client/${data.clientId}/withdrawal/${data.id}`;
        return this._sendRequest(url, 'POST', data);
      })
      .then(res => {
        log.i(`Withdrawal id=${data.id}, accountId=${data.accountId}, status=${data.status} is saved in CRM`);
        return res;
      });
  },

  getClientInfo(data) {
    return checkObjectKeys(data, ['email'])
      .then(() => {
        let query = `email=${data.email}`;
        if (data.login) {
          query = query + `&login=${data.login}`;
        }
        let url = `${config.protocol}://${config.host}:${config.port}${config.path}/client/by?${query}`;
        return this._sendRequest(url, 'GET', {});
      });
  },

  notifyLogin(data) {
    return checkObjectKeys(data, ['email'])
      .then(() => {
        let url = `${config.protocol}://${config.host}:${config.port}${config.path}/client/setlogin`;
        return this._sendRequest(url, 'POST', {email: data.email});
      });
  },

  _sendRequest(url, method, data) {
    log.d('CRM:sendRequest', url, method, data);
    return new Promise((resolve, reject) => {
      let options = {
        url: url,
        method: method,
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Bearer ' + config.token
        },
        json: data
      };
      request(options, (err, res, body) => {
        if (err) {
          log.e('Error with crm:', err);
          return reject(err);
        }
        if (res.statusCode !== 200) {
          log.e('Error with crm:', res.statusMessage);
          return reject(body);
        }
        if (body && typeof body == 'string') {
          try {
            body = JSON.parse(body);
          } catch (err) {
            log.e('Error with crm:', err);
            return reject(body);
          }
        }
        return resolve(body.data ? body.data : body);
      });
    });
  }
};

module.exports = Crm;
