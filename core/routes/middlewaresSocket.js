
const _ = require('lodash');
const jwt = require('jwt-simple');

const config = require(__dirroot + 'config');
const Middlewares = __app('routes/middlewares');
const GE = __app('libs/GE');
const AppError = __app('libs/errors');
const constants = __app('libs/constants');

const clientModel = __app('models/clientModel');
const orderMngr = __app('managers/ordersManager');
const accountMngr = __app('managers/accountsManager');
const SocketCacheService = __app('services/socketCache');

class Socket extends Middlewares {
  constructor() {
    super();
  }

  connectionHandler(socket) {
    socket.on('disconnect', () => {
      SocketCacheService.unsetById(socket.id);
      socket.leave(constants.ROOMS.ORDERBOOKS);
      socket.leave(constants.ROOMS.ORDERS);
      socket.leave(constants.ROOMS.TRADESHISTORY);
      socket.leave(constants.ROOMS.QUOTES);
      socket.disconnect();
    });
    socket.on('error', err => {
      log.e('socket-client error', err);
    });
  }

  checkToken(socket, next) {
    let token = socket.handshake.query.Authorization;
    let parsed = {};
    if (!token) {
      let access = super._setDefaultAccess({});
      SocketCacheService.setById(socket.id, access);
      socket.join(constants.ROOMS.QUOTES);
      return next(null);
    }

    super._parseToken(token, parsed);
    if (parsed.access.error) {
      let response = this._prepareResponseError(parsed.access.error);
      return next(response);
    }

    this._isTokenTimeExpired(parsed.access);
    if (parsed.access.error) {
      let response = this._prepareResponseError(parsed.access.error);
      return next(response);
    }

    return clientModel.findById(parsed.access.clientId)
      .then(client => {
        if (!client) {
          let error = new AppError('NOT_FOUND_CLIENT');
          let response = this._prepareResponseError(error);
          return next(response);
        }
        SocketCacheService.setById(socket.id, parsed.access);
        // socket.join(constants.ROOMS.ORDERBOOKS);
        // socket.join(constants.ROOMS.TRADESHISTORY);
        // socket.join(constants.ROOMS.QUOTES);
        // socket.join('userId=' + parsed.access.clientId);
        return next(null);
      });
  }

  setCommonMiddlewares(socket, args, next) {
    this._setRequestFormat(socket, args);
    this._setResponseFormat(socket, args);
    next(null);
  }

  setProjectListeners(socket) {
    // ---------- private user's information -------------------------
    GE.on('/balance/me/update', data => {
      socket.to('userId=' + data.clientId).emit('/balance/me/update', data.accounts);
    });
    GE.on('/orders/me/update', data => {
      let data2 = _.cloneDeep(data);
      delete data2.clientId;
      socket.to('userId=' + data.clientId + ':' + data.symbol).emit('/orders/me/update', data2);
    });
    GE.on('/limits/me/update', data => {
      socket.to('userId=' + data.clientId).emit('/limits/me/update', {
        dayUsdLimit: data.dayUsdLimit,
        remainedDayUsd: data.remainedDayUsd
      });
    });
    // ---------- public information by symbol -----------------------
    GE.on('/orders/books/update', data => {
      socket.to(constants.ROOMS.ORDERBOOKS + ':' + data.symbol).emit('/orders/books/update', data.books);
    });
    GE.on('/orders/update', data => {
      socket.to(constants.ROOMS.ORDERS + ':' + data.symbol).emit('/orders/update', data);
    });
    GE.on('/trades/history/update', data => {
      socket.to(constants.ROOMS.TRADESHISTORY + ':' + data.symbol).emit('/trades/history/update', data);
    });
    // ---------- public information -------------------------
    GE.on('/quotes/update', data => {
      socket.to(constants.ROOMS.QUOTES).emit('/quotes/update', data);
    });

    GE.on('/orders/freeze', data => {
      orderMngr.freezeOrder(_.cloneDeep(data));
    });
    GE.on('/balance/synchronize', data => {
      accountMngr.synchronizeBalance(data);
    });
  }

  // ****************************  PRIVATE METHODS  ***************************

  _isTokenTimeExpired(token) {
    if (!token.expiry_time || Date.now() >= token.expiry_time) {
      token.error = new AppError('FORBIDDEN_XTOKEN_EXPIRED');
    }
    return token;
  }

  _prepareResponseError(data) {
    return {
      status: data.status ? data.status : 500,
      data: {
        code: data.errorCode ? data.errorCode : 0,
        message: data.message ? data.message : data
      }
    };
  }

  _parseRequestData(args) {
    if (typeof args[1] === 'string') {
      try {
        args[1] = JSON.parse(args[1]);
      } catch (err) {
        args[1] = {error: new AppError('VALIDATION_PARSE_ERROR')};
      }
    }
    return args[1] || {};
  }

  _setRequestFormat(socket, req) {
    req.body = this._parseRequestData(req);
    req.methodFullName = req[0];
    req.access = SocketCacheService.getById(socket.id);

    let reqForLog = super._prepareRequestForLogging(req);
    log.writeRequest('SOCKET'+req.methodFullName, reqForLog);
  }

  _setResponseFormat(socket, req) {
    socket.answer = ({status: status, data: data}) => {
      if (req.access && req.access.needUpdateToken) {
        data['X-Token'] = jwt.encode({
          clientId: req.access.clientId,
          role: req.access.role,
          expiry_time: Date.now() + config.maxAge * 1000
        }, config.secret_salt_api_tokens);
        SocketCacheService.setById(socket.id, {
          clientId: req.access.clientId,
          role: req.access.role,
          expiry_time: Date.now() + config.maxAge * 1000
        });
        // socket.sock.join(constants.ROOMS.ORDERBOOKS + ':BTCUSD');
        // socket.sock.join(constants.ROOMS.TRADESHISTORY);
        // socket.sock.join(constants.ROOMS.QUOTES);
        // socket.sock.join('userId=' + req.access.clientId);
      }
      data.extID = req.body.extID;
      // socket.emit(req.methodFullName + '.response', {status: status, data: data});
      socket.emit('/response', {status: status, data: data});

      let resForLog = super._prepareResponseForLogging(req.methodFullName, data);
      log.writeResponse('SOCKET'+req.methodFullName, resForLog);
    };
    super.setErrorResponseFormat(socket);
  }

}

module.exports = Socket;
