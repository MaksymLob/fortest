
const config = require(__dirroot+'config.json');

const order = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true,
    additionalKey: {clientId: 'set', symbol: 'set'}
  },
  clientId: {
    type: 'integer',
    allowNull: false
  },
  symbol: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [config.symbols]}
  },
  price: {
    type: 'double',
    allowNull: false,
    validate: {min: 0.000000001}
  },
  amount: {
    type: 'double',
    allowNull: false,
    validate: {min: 0.000001}
  },
  volume: {
    type: 'double',
    allowNull: true
  },
  type: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [['buy', 'sell']]}
  },
  status: {
    type: 'string',
    allowNull: false,
    defaultValue: 'open'
  },
  timeOpen: {
    type: 'integer',
    allowNull: false
  },
  timeClose: {
    type: 'integer',
    allowNull: false,
    defaultValue: 0
  }
};

module.exports = {
  schema: order
};
