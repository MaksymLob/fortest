
const DepositModel = {};

const depositSchema = __app('models/schemas/deposit');
const Model = __app('models/model');
const schema = new Model(depositSchema.schema);
DepositModel.validations = depositSchema.validations;
const depositModel = schema.defineDataBase('mysql', 'deposits', DepositModel);

module.exports = depositModel;
