

const _ = require('lodash');
const config = require(__dirroot + 'config');
const GE = __app('libs/GE');
const symbolsD = __app('libs/cryptoDefinitions/symbolReductions.json');

const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();
let Redis = __app('services/redis');
Redis = new Redis();
const redis = Redis.connection;
const redlock = Redis.redlock;

class Crypto {

  constructor() {
    if (Crypto._instance) {
      return Crypto._instance;
    }

    this.accounts = {};
    this.books = {};
    this.QUEUE = [];

    Crypto._instance = this;
  }

  init() {
    // save all user's balances in cache
    redis.keys('clients:id:*', (err, res) => {
      for (let i = 0; i < res.length; i++) {
        redis.hgetall(res[i], (err, res) => {
          if (err) {
            return;
          }
          this.accounts[res.clientId + ':' + res.currency] = {
            clientId: Number(res.clientId),
            balance: Number(res.balance),
            used: Number(res.used),
            currency: res.currency
          };
        });
      }
    });

    redis.smembers('orders:close', (err, res) => {
      if (err) return;
      for (let item of res) {
        redis.hgetall('orders:id:' + item, (err, order) => {
          if (err) return;
          // GE.emit('/orders/freeze', order);
        });
      }
    });

    // this.checkQueue();
  }

  addAccount(data) {
    this.accounts[data.clientId + ':' + data.currency] = {
      clientId: Number(data.clientId),
      balance: Number(data.balance),
      used: 0,
      currency: data.currency
    };
    return this.accounts[data.clientId + ':' + data.currency];
  }

  updateBalance({clientId: clientId, currency: currency, amount: amount}) {
    let multi = redis.multi();
    this.accounts[clientId + ':' + currency].balance += amount;
    multi.del('clients:id:' + clientId + ':currency:' + currency);
    multi.hmset('clients:id:' + clientId + ':currency:' + currency, this.accounts[clientId + ':' + currency]);
    multi.exec((err, res) => {
      if (err) {
        log.e('REDIS ERROR: ', err);
        return;
      }
      GE.emit('/balance/me/update', {
        clientId: clientId,
        accounts: {
          balance: this.accounts[clientId + ':' + currency].balance,
          used: this.accounts[clientId + ':' + currency].used,
          currency: currency
        }
      });
    });
  }

  balanceAdjustment({clientId, currency, newBalance, newUsed}) {
    this.accounts[clientId + ':' + currency].used = newUsed;
    this.accounts[clientId + ':' + currency].balance = newBalance;
    GE.emit('/balance/me/update', {
      clientId: clientId,
      accounts: {
        balance: this.accounts[clientId + ':' + currency].balance,
        used: this.accounts[clientId + ':' + currency].used,
        currency: currency
      }
    });
  }

  putIntoQueue(order) {
    this.QUEUE.push(order);
    // return Promise.resolve();
    // console.time('opening order');
    return this.handleOrder(order)
      .then(res => {
        // console.timeEnd('opening order');
        return res;
      });
  }

  checkQueue() {
    // console.time('opening order');
    // log.d(this.QUEUE.length);
    // log.d(this.QUEUE[0]);
    if (this.QUEUE.length) {
      return this.handleOrder(this.QUEUE[0])
        .then(() => {
          // console.timeEnd('opening order');
          this.QUEUE.shift();
          this.checkQueue();
        })
        .catch(() => {
          // log.e('Error with order from BITFINEX, id = ' + this.queueOrders[0].id, err);
          this.QUEUE.shift();
          this.checkQueue();
        });
    } else {
      setTimeout(() => {
        this.checkQueue();
      }, 200);
    }
  }

  handleOrder(order) {
    return new Promise(resolve => {
      switch (order.act) {
        case 'open':
          delete order.act;
          return resolve(this.openOrder(order));
        case 'update':
          delete order.act;
          return resolve(this.updateOrder(order));
        case 'cancel':
          delete order.act;
          return resolve(this.deleteOrder(order));
        default:
          return;
      }
    });
  }

  updateOrder(order) {
    return new Promise(resolve => {
      let multi = redis.multi();
      multi.del('orders:id:' + order.id);
      multi.hmset('orders:id:' + order.id, order);
      redis.hgetall('orders:id:' + order.id, (err, res) => {
        if (!res) {
          return resolve(this.openOrder(order));
        }
        order.currency = res.currency;
        order.status = res.status;
        order.timeOpen = res.timeOpen;
        order.timeClose = res.timeClose;
        if (!order.volume) delete order.volume;
        let currencyPay = order.type == 'buy' ? order.symbol.substr(3) : order.symbol.substr(0, 3);
        let amount = order.type == 'buy'
          ? order.amount * order.price - (Number(res.amount) * Number(res.price))
          : order.amount - Number(res.amount);
        this._freezeBalance(multi, order.clientId, currencyPay, Number(amount.toFixed(8)));

        return resolve(Redis.execTransaction(multi)
          .then(() => {
            GE.emit('/orders/me/update', _.assign(order, {action: 'update'}));
            GE.emit('/orders/update', _.assign(order, {action: 'update'}));
            GE.emit('/balance/me/update', {
              clientId: order.clientId,
              accounts: {
                balance: this.accounts[order.clientId + ':' + currencyPay].balance,
                used: this.accounts[order.clientId + ':' + currencyPay].used,
                currency: currencyPay
              }
            });
            return order;
          }));
      });
    });
  }

  deleteOrder(order) {
    let multi = redis.multi();
    let currencyPay = order.type == 'buy' ? order.symbol.substr(3) : order.symbol.substr(0, 3);
    this._freezeBalance(multi, order.clientId, currencyPay, order.type == 'buy' ? -order.amount * order.price : -order.amount);
    multi.del('orders:id:' + order.id);
    multi.srem('orders:symbol:' + order.symbol, order.id);
    multi.srem('orders:clientId:' + order.clientId, order.id);
    multi.zrem('order_book:list:' + order.symbol + ':' + order.type, order.id);
    return Redis.execTransaction(multi)
      .then(() => {
        GE.emit('/orders/me/update', _.assign(order, {action: 'cancel'}));
        GE.emit('/orders/update', _.assign(order, {action: 'cancel'}));
        GE.emit('/balance/me/update', {
          clientId: order.clientId,
          accounts: {
            balance: this.accounts[order.clientId + ':' + currencyPay].balance,
            used: this.accounts[order.clientId + ':' + currencyPay].used,
            currency: currencyPay
          }
        });
        return order;
      })
      .catch(err => {
        if (err == 'REDIS_TRANSACTION_ERROR') {
          this.accounts[order.clientId + ':' + currencyPay].balance += order.type == 'buy' ? order.amount * order.price : order.amount;
        }
        throw err;
      });
  }

  openOrder(data) {
    let lock;
    let typeOpposite = data.type === 'sell' ? 'buy' : 'sell';
    return redlock.lock('locks:processing:' + data.symbol, 2000)
      .then(res => {
        lock = res;
        return this._findOppositeOrdersFromRange({type: typeOpposite, symbol: data.symbol, price: data.price});
      })
      .then(res => {
        if (res.length) {
          return this._closeOrders(data, res);
        } else {
          return this._saveOrder(data);
        }
      })
      .then(() => {
        return lock.unlock();
      })
      .then(() => {
        return data;
      })
      .catch(err => {
        log.e('Error with opening order id = ' + data.id + ', order = ' + JSON.stringify(data));
        throw err;
      });
  }

  getOpenedOrders(data) {
    let from;
    let to;
    if (data.type == 'buy') {
      from = data.lastBuy ? data.lastBuy : 'inf';
      to = '-inf';
    } else {
      from = data.lastSell ? data.lastSell : '-inf';
      to = 'inf';
    }
    return this._getOpenedOrdersIds(_.assign(data, {from: from, to: to}))
      .then(res => {
        return this._getOpenedOrdersFull(res);
      })
      .then(res => {
        res = res.map(item => {
          return {
            amount: item.amount,
            id: item.id,
            price: item.price,
            symbol: item.symbol,
            type: item.type
          };
        });
        return res;
      });
  }

  // ****************************  PRIVATE METHODS  ***************************

  _getNeededOrdersFromBook(amount, opposites) {
    let actions = opposites.map(item => {
      return new Promise(resolve => {
        redis.hgetall('orders:id:' + item, (err, res) => resolve(res));
      });
    });

    return Promise.all(actions)
      .then(res => {
        opposites = res;
        for (let i = 0; i < opposites.length; i++) {
          opposites[i].price = Number(Number(opposites[i].price).toFixed(8));
          opposites[i].amount = Number(Number(opposites[i].amount).toFixed(8));
          amount = Number((amount - Number(opposites[i].amount)).toFixed(8));
          if (amount <= 0) {
            opposites.splice(i + 1, opposites.length - i - 1);
            break;
          }
        }
        return opposites;
      });
  }

  _closeOrders(order, opposites) {
    let timeClose = Date.now();
    return this._getNeededOrdersFromBook(order.amount, opposites)
      .then(res => {
        opposites = res;
        return this._mergeOurOrders(_.cloneDeep(order), opposites, timeClose);
      })
      .then(res => {
        let {closedNew, remainedNew, paymentAmounts, opposites} = res;
        let currencyPay = order.type == 'buy' ? order.symbol.substr(3) : order.symbol.substr(0, 3);
        let currencyGet = order.type == 'buy' ? order.symbol.substr(0, 3) : order.symbol.substr(3);
        let price = opposites[opposites.length - 1].closed.price;
        closedNew.volume = 0;
        opposites.forEach(item => {
          closedNew.volume += item.closed.amount * item.closed.price;
        });
        closedNew.price = Number((closedNew.volume / closedNew.amount).toFixed(8));
        quotes.completeCandle({symbol: order.symbol, price: price, amount: closedNew.amount, volume: closedNew.volume});

        for (let i = 0; i < opposites.length; i++) {
          GE.emit('/balance/synchronize', [
            {clientId: paymentAmounts[i].clientId, currency: currencyPay, amount: paymentAmounts[i].getAmountFromBook},
            {clientId: paymentAmounts[i].clientId, currency: currencyGet, amount: -paymentAmounts[i].getAmountNew},
          ]);
          this._sendNotifications(opposites[i], currencyPay, currencyGet);
        }
        let payNew = closedNew.type === 'buy' ? closedNew.volume : closedNew.amount;
        let getNew = closedNew.type === 'buy' ? closedNew.amount : closedNew.volume;
        GE.emit('/balance/synchronize', [
          {clientId: closedNew.clientId, currency: currencyPay, amount: -payNew},
          {clientId: closedNew.clientId, currency: currencyGet, amount: getNew * (1 - config.fee)}
        ]);
        if (remainedNew) remainedNew.action = 'add';
        closedNew.new = true;
        this._sendNotifications({closed: closedNew, remained: remainedNew}, currencyPay, currencyGet);

        return order;
      });
  }

  _calcPaymentAmounts(orderFromBook, orderNew) {
    let getAmountFromBook;
    let getAmountNew;
    let amount = orderNew.amount;

    if (orderFromBook.amount <= orderNew.amount) { // ордер из стакана закрылся полностью
      getAmountFromBook = orderFromBook.type == 'sell' ? orderFromBook.amount * orderFromBook.price : orderFromBook.amount;
      orderFromBook.closedAmount = orderFromBook.amount;
      orderFromBook.remainedAmount = 0;
      orderNew.amount = Number((orderNew.amount - orderFromBook.amount).toFixed(8));
    } else {
      getAmountFromBook = orderFromBook.type == 'sell' ? orderNew.amount * orderFromBook.price : orderNew.amount;
      orderFromBook.closedAmount = orderNew.amount;
      orderFromBook.remainedAmount = Number((orderFromBook.amount - orderNew.amount).toFixed(8));
      orderFromBook.amount = orderNew.amount;
      orderNew.amount = 0;
    }

    if (!orderNew.amount) { // новый ордер закрылся полностью
      getAmountNew = orderNew.type == 'sell' ? Number((amount * orderFromBook.price).toFixed(8)) : orderFromBook.amount;
    } else {
      getAmountNew = orderNew.type == 'sell' ? orderFromBook.amount * orderFromBook.price : orderFromBook.amount;
    }

    return {getAmountFromBook: getAmountFromBook, getAmountNew: getAmountNew};
  }

  _mergeOurOrders(orderNew, opposites, timeClose) {
    let multi = redis.multi();
    let paymentAmounts;
    let amount = orderNew.amount;
    let opposites2;
    let closed;
    let remained;
    let currencyPay = orderNew.type == 'buy' ? orderNew.symbol.substr(3) : orderNew.symbol.substr(0, 3);
    let currencyGet = orderNew.type == 'buy' ? orderNew.symbol.substr(0, 3) : orderNew.symbol.substr(3);

    let actions = opposites.map(item => {
      return this._mergeOrders(orderNew, item, multi, timeClose, currencyPay, currencyGet);
    });
    // todo возможно стоит заменить all на цикл for
    return Promise.all(actions)
      .then(res => {
        opposites2 = res;
        paymentAmounts = res.map(item => item.amounts);

        orderNew.remainedAmount = orderNew.amount;
        orderNew.closedAmount = amount - orderNew.amount;
        orderNew.amount = orderNew.closedAmount;

        if (orderNew.remainedAmount) {
          remained = _.cloneDeep(orderNew);
          remained.amount = orderNew.remainedAmount;
          delete remained.closedAmount;
          delete remained.remainedAmount;
        }
        closed = _.cloneDeep(orderNew);
        closed.amount = orderNew.closedAmount;
        closed.status = 'close';
        closed.timeClose = timeClose;
        delete closed.closedAmount;
        delete closed.remainedAmount;

        if (orderNew.remainedAmount) {
          return Redis._getNewId('orders', true, remained, 'id')
            .then(() => {
              this._freezeBalance(multi, remained.clientId, currencyPay, remained.amount);
              multi.hmset('orders:id:' + remained.id, remained);
              multi.sadd('orders:clientId:' + remained.clientId, remained.id);
              multi.sadd('orders:symbol:' + remained.symbol, remained.id);
              multi.zadd('order_book:list:' + remained.symbol + ':' + remained.type, remained.price, remained.id);
              return Redis._getNewId('orders', true, closed, 'id');
            })
            .then(() => {
              multi.hmset('orders:id:' + closed.id, closed);
              // multi.sadd('orders:clientId:' + closed.clientId, closed.id);
              // multi.sadd('orders:symbol:' + closed.symbol, closed.id);
              multi.sadd('orders:close', closed.id);
              return;
            });
        } else {
          return Redis._getNewId('orders', true, closed, 'id')
            .then(() => {
              multi.hmset('orders:id:' + closed.id, closed);
              // multi.sadd('orders:clientId:' + closed.clientId, closed.id);
              // multi.sadd('orders:symbol:' + closed.symbol, closed.id);
              multi.sadd('orders:close', closed.id);
              return;
            });
        }
      })
      .then(() => {
        return Redis.execTransaction(multi);
      })
      .then(() => {
        return {closedNew: closed, remainedNew: remained, opposites: opposites2, paymentAmounts: paymentAmounts};
      })
      .catch(err => {
        throw err;
      });
  }

  _mergeOrders(orderNew, orderFromBook, multi, timeClose, currencyPay, currencyGet) {
    let closed;
    let remained;
    orderFromBook.amount = Number(orderFromBook.amount);
    orderFromBook.price = Number(orderFromBook.price);
    let paymentAmounts = this._calcPaymentAmounts(orderFromBook, orderNew);

    // кладем на счет пользователю из стакана искомую валюту
    this._updateBalance(multi, orderFromBook.clientId, currencyPay, paymentAmounts.getAmountFromBook);
    this._updateBalance(multi, orderFromBook.clientId, currencyGet, -paymentAmounts.getAmountNew);
    this._freezeBalance(multi, orderFromBook.clientId, currencyGet, -paymentAmounts.getAmountNew);
    // снимаем со счета у другого пользователя искомую валюту
    this._updateBalance(multi, orderNew.clientId, currencyPay, -paymentAmounts.getAmountFromBook);
    this._updateBalance(multi, orderNew.clientId, currencyGet, paymentAmounts.getAmountNew * (1 - config.fee));

    if (orderFromBook.remainedAmount) {
      remained = _.cloneDeep(orderFromBook);
      remained.amount = orderFromBook.remainedAmount;
      delete remained.closedAmount;
      delete remained.remainedAmount;
    }
    closed = _.cloneDeep(orderFromBook);
    closed.amount = orderFromBook.closedAmount;
    closed.status = 'close';
    closed.timeClose = timeClose;
    delete closed.closedAmount;
    delete closed.remainedAmount;

    multi.del('orders:id:' + orderFromBook.id);
    if (orderFromBook.remainedAmount) {
      multi.hmset('orders:id:' + remained.id, remained);
      delete closed.id;
      return Redis._getNewId('orders', true, closed, 'id')
        .then(() => {
          multi.hmset('orders:id:' + closed.id, closed);
          // multi.sadd('orders:clientId:' + closed.clientId, closed.id);
          // multi.sadd('orders:symbol:' + closed.symbol, closed.id);
          multi.sadd('orders:close', closed.id);
          return {closed: closed, remained: remained, amounts: _.assign(paymentAmounts, {clientId: orderFromBook.clientId})};
        });
    } else {
      multi.hmset('orders:id:' + closed.id, closed);
      multi.sadd('orders:close', closed.id);
      multi.zrem('order_book:list:' + closed.symbol + ':' + closed.type, closed.id);
      orderFromBook.status = 'close';
      orderFromBook.timeClose = timeClose;
      return Promise.resolve({closed: closed, remained: remained, amounts: _.assign(paymentAmounts, {clientId: orderFromBook.clientId})});
    }
  }

  _findOppositeOrdersFromRange({type: type, symbol: symbol, price: price}) {
    return new Promise((resolve, reject) => {
      let from = type == 'buy' ? 'infinity' : '-infinity';
      let to = type == 'buy' ? price : price;

      // TODO inside the same side sorting is by lex order. Therefore we need add sorting by order of adding
      if (type == 'buy') {
        redis.zrevrangebyscore('order_book:list:' + symbol + ':' + type, from, to, 'WITHSCORES', (err, res) => {
          let list = [];
          // *********************************************
          // This block is for reverting order of id for the same price (because zREVrangebyscore)
          let same = [];
          for (let i = 1; i <= res.length - 1; i += 2) {
            same.unshift(res[i - 1]);
            if (res[i] != res[i + 2]) {
              list = list.concat(same);
              same = [];
            }
          }
          // *********************************************
          return resolve(list);
        });
      } else {
        redis.zrangebyscore('order_book:list:' + symbol + ':' + type, from, to, (err, list) => {
          return resolve(list);
        });
      }
    });
  }

  _saveOrder(order) {
    let multi = redis.multi();
    let currencyPay = order.type == 'buy' ? order.symbol.substr(3) : order.symbol.substr(0, 3);
    this._freezeBalance(multi, order.clientId, currencyPay, order.type == 'buy' ? order.amount * order.price : order.amount);

    return Redis._getNewId('orders', true, order, 'id')
      .then(() => {
        multi.hmset('orders:id:' + order.id, order);
        multi.sadd('orders:clientId:' + order.clientId, order.id);
        multi.sadd('orders:symbol:' + order.symbol, order.id);
        multi.zadd('order_book:list:' + order.symbol + ':' + order.type, order.price.toFixed(8), order.id);
        return Redis.execTransaction(multi);
      })
      .then(() => {
        GE.emit('/orders/me/update', _.assign(order, {action: 'add'}));
        GE.emit('/orders/update', _.assign(order, {action: 'add'}));
        GE.emit('/balance/me/update', {
          clientId: order.clientId,
          accounts: {
            balance: this.accounts[order.clientId + ':' + currencyPay].balance,
            used: this.accounts[order.clientId + ':' + currencyPay].used,
            currency: currencyPay
          }
        });
        return order;
      })
      .catch(err => {
        if (err == 'REDIS_TRANSACTION_ERROR') {
          this.accounts[order.clientId + ':' + currencyPay].used -= order.type == 'buy' ? order.amount * order.price : order.amount;
        }
        throw err;
      });
  }

  _updateBalance(multi, clientId, currency, amount) {
    this.accounts[clientId + ':' + currency].balance = Number((this.accounts[clientId + ':' + currency].balance + amount).toFixed(8));
    multi.del('clients:id:' + clientId + ':currency:' + currency);
    multi.hmset('clients:id:' + clientId + ':currency:' + currency, this.accounts[clientId + ':' + currency]);
    return multi;
  }

  _freezeBalance(multi, clientId, currency, amount) {
    let atomic = false;
    if (!multi) {
      atomic = true;
      multi = redis.multi();
    }
    this.accounts[clientId + ':' + currency].used += Number(amount);
    this.accounts[clientId + ':' + currency].used = Number(this.accounts[clientId + ':' + currency].used.toFixed(8));
    multi.del('clients:id:' + clientId + ':currency:' + currency);
    multi.hmset('clients:id:' + clientId + ':currency:' + currency, this.accounts[clientId + ':' + currency]);
    if (atomic) {
      multi.exec();
    } else {
      return multi;
    }
  }

  _getBalanceAndUsed(clientId, currency) {
    try {
      return {
        balance: this.accounts[clientId + ':' + currency].balance,
        used: this.accounts[clientId + ':' + currency].used
      };
    } catch (err) {
      return {
        balance: null,
        used: null
      };
    }
  }

  _sendNotifications(order, currencyPay, currencyGet) {
    let payAccount = this.accounts[order.closed.clientId + ':' + currencyPay];
    let getAccount = this.accounts[order.closed.clientId + ':' + currencyGet];
    GE.emit('/balance/me/update', {
      clientId: order.closed.clientId,
      accounts: {balance: payAccount.balance, used: payAccount.used, currency: currencyPay}
    });
    GE.emit('/balance/me/update', {
      clientId: order.closed.clientId,
      accounts: {balance: getAccount.balance, used: getAccount.used, currency: currencyGet}
    });

    if (order.remained) {
      GE.emit('/orders/me/update', _.assign(order.remained, {action: order.remained.action ? order.remained.action : 'update'}));
      GE.emit('/orders/update', _.assign(order.remained, {action: order.remained.action ? order.remained.action : 'update'}));
    } else {
      if (!order.closed.new) {
        GE.emit('/orders/me/update', _.assign(order.closed, {action: 'remove'}));
        GE.emit('/orders/update', _.assign(order.closed, {action: 'cancel'}));
      }
    }
    GE.emit('/trades/history/update', {
      symbol: order.closed.symbol,
      price: order.closed.price,
      amount: order.closed.amount,
      volume: order.closed.volume ? order.closed.volume : order.closed.amount * order.closed.price,
      timeClose: order.closed.timeClose,
      type: order.closed.type
    });
    GE.emit('/orders/freeze', order.closed);
  }

  _getOpenedOrdersIds({from, to, symbol, type, count, rank}) {
    return new Promise(resolve => {
      if (type == 'buy') {
        redis.zrevrangebyscore('order_book:list:' + symbol + ':' + type, from, to, 'WITHSCORES', (err, res) => {
          let prices = res.filter((item, index) => index % 2 != 0 && (index == 0 || res[index] != res[index - 2]));
          let lastPrice = this._choosePricesByRank(prices, rank, count, symbol);
          let ids = res.filter((item, index) => index % 2 == 0 && Number(res[index + 1]) >= Number(lastPrice));
          return resolve(ids);
        });
      } else {
        redis.zrangebyscore('order_book:list:' + symbol + ':' + type, from, to, 'withscores', (err, res) => {
          let prices = res.filter((item, index) => index % 2 != 0 && (index == 0 || res[index] != res[index - 2]));
          let lastPrice = this._choosePricesByRank(prices, rank, count, symbol);
          let ids = res.filter((item, index) => index % 2 == 0 && Number(res[index + 1]) <= Number(lastPrice));
          return resolve(ids);
        });
      }
    });
  }

  _getOpenedOrdersFull(ids) {
    return new Promise(resolve => {
      let multi = redis.multi();
      ids.forEach(id => multi.hgetall('orders:id:' + id));
      multi.exec((err, res) => {
        return resolve(res);
      });
    });
  }

  _choosePricesByRank(prices, rank, count, symbol) {
    let newPrices = [];
    let digits = symbolsD[symbol].digits;
    if (digits - rank + 1 >= 0) {
      prices.forEach((item, i) => {
        newPrices[i] = Number(item).toFixed(digits - rank + 1);
        /* Math.floor(item / rank) * rank */
      });
    } else {
      let k;
      let m = (digits - rank + 1) * -1;
      if (m == 1) k = 10;
      if (m == 2) k = 100;
      if (m == 3) k = 1000;
      if (m == 4) k = 10000;
      prices.forEach((item, i) => {
        newPrices[i] = Math.floor(item / k) * k;
      });
    }
    let k = 0;
    let i = 0;
    for (i; i < newPrices.length; i++) {
      if (i == 0 || newPrices[i] != newPrices[i - 1]) k++;
      if (k == count + 1) break;
    }
    return prices[i - 1];
  }
}

module.exports = Crypto;
