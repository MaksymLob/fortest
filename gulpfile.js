const gulp = require('gulp');
const mochaSpawn = require('gulp-spawn-mocha');
const eslint = require('gulp-eslint');

const LINT_PATH = [
  '**/*.js',
  '!node_modules/**'
];

const bindTests = (name, path) => {
  gulp.task(name, () => {
    process.env.NODE_ENV = 'test';

    return gulp
      .src(path, {read: false})
      .pipe(mochaSpawn({reporter: 'spec'}));
  });
};



gulp.task('lint', () => {
  return gulp
    .src(LINT_PATH)
    .pipe(eslint({useEslintrc: true}))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});
