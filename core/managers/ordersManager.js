
const _ = require('lodash');
const constants = __app('libs/constants');
const orderModel = __app('models/orderModel');
const accountModel = __app('models/accountModel');
const accountKeyModel = __app('models/accountKeyModel');
const CryptoOrder = __app('services/exchange/cryptoOrders');
const cryptoOrder = new CryptoOrder();
const orderSchema = __app('models/schemas/order').schema;

let Redis = __app('services/redis');
Redis = new Redis();
const redis = Redis.connection;

const OrderManager = {

  openOrder(data) {
    if (data.action == 'close' && data.clientId == 2) data.clientId = 3;
    let order;
    data.timeOpen = Date.now();
    return orderModel.validateData(data)
      .then(res => {
        order = res;
        if (data.btfxId) order.btfxId = data.btfxId;
        order.currency = order.type == 'buy' ? res.symbol.substr(3) : res.symbol.substr(0, 3);
        order.act = 'open';
        if (order.id) {
          return this._putOrderFromBFX(order);
        } else {
          return this._saveOrder(order);
        }
      });
  },

  cancelOrder(data) {
    if (!data.id) {
      return Promise.reject('VALIDATION_ORDERID_IS_NOT_FILLED');
    }
    return orderModel.findByIdR(data.id)
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_ORDER');
        }
        if (res.clientId != data.clientId) {
          return Promise.reject('FORBIDDEN_NO_ACCESS_RIGHTS');
        }
        res.act = 'cancel';
        return cryptoOrder.putIntoQueue(res);
      });
  },

  freezeOrder(data) {
    let order = _.cloneDeep(data);
    delete order.id;
    order.volume = order.amount * order.price;
    return orderModel.create(order)
      .then(() => {
        redis.srem('orders:close', data.id);
        return orderModel.deleteR(data);
      })
      .catch(err => {
        log.e('Error with freezing of order id = ' + data.id, err);
      });
  },

  getOrderBooks(data) {
    if (!data.symbol) {
      data.symbol = 'BTCUSD';
    }
    const book = cryptoOrder.getOrderBooks({symbol: data.symbol});
    return Promise.resolve(book);
  },

  getCurrentOpenedOrders(data) {
    if (!data.symbol) data.symbol = 'BTCUSD';
    if (!data.count) data.count = 20;
    if (!data.rank) data.rank = 1;
    return Promise.all([
      cryptoOrder.getOpenedOrders(_.assign(data, {type: 'buy'})),
      cryptoOrder.getOpenedOrders(_.assign(data, {type: 'sell'}))
    ])
      .then(res => {
        res[1].forEach(item => {
          res[0].push(item);
        });
        return {orders: res[0]};
      });
  },

  getMyOpenedOrders(data) {
    if (!data.symbol) {
      data.symbol = 'BTCUSD';
    }
    return orderModel.findAllR({where: {clientId: data.clientId, status: 'open'}})
      .then(res => {
        let result = [];
        for (let i = 0; i < res.length; i++) {
          if (res[i].symbol == data.symbol) {
            delete res[i].clientId;
            result.push(res[i]);
          }
        }
        return result;
      });
  },

  getMyClosedOrders(data) {
    if (!data.limit) {
      data.limit = constants.ORDER.DEFAULT_LIMIT;
    }
    if (!data.symbol) {
      data.symbol = 'BTCUSD';
    }
    return orderModel.findAll({
      where: {clientId: data.clientId, status: 'close', symbol: data.symbol},
      order: [['timeClose', 'DESC']],
      limit: data.limit
    })
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i] = res[i].get();
          delete res[i].clientId;
        }
        return res;
      });
  },

  getAllClosedOrders(data) {
    let searchParams;
    return this._prepareSearchParams(data)
      .then(res => {
        searchParams = res;
        if (searchParams.where.logins) {
          return accountKeyModel.findAll({where: {id: searchParams.where.logins}})
            .then(res => {
              delete searchParams.where.logins;
              if (res) {
                res.forEach(item => {
                  searchParams.where.clientId
                    ? searchParams.where.clientId['$in'].push(item.get('clientId'))
                    : searchParams.where.clientId = {$in: [item.get('clientId')]};
                });
                return orderModel.findAll(searchParams);
              }
              return [];
            });
        } else {
          return orderModel.findAll(searchParams);
        }
      })
      .then(res => {
        let result = res.map(item => {
          return item.get();
        });
        if (data.filters && data.filters.logins) {
          result.forEach((item, i) => {
            item.login = data.filters.logins;
            delete result[i].clientId;
          });
        }
        return result;
      })
      .then(res => {
        return this._prepareResponse(res);
      });
  },

  _prepareSearchParams(data) {
    return new Promise((resolve, reject) => {
      if (!data.limit) {
        data.limit = constants.ORDER.DEFAULT_LIMIT;
      } else if (data.limit > constants.ORDER.MAX_LIMIT) {
        data.limit = constants.ORDER.MAX_LIMIT;
      }

      let searchParams = {
        limit: parseInt(data.limit)
      };

      if (data.offset) {
        const offset = parseInt(data.offset);
        if (!isNaN(offset) && offset >= 0) {
          searchParams.offset = offset;
        } else {
          return reject('VALIDATION_ORDER_BAD_OFFSET_PARAMETER');
        }
      }

      let order = [];
      if (data.order) {
        if (typeof data.order.column === 'string' && typeof data.order.dir === 'string') {
          order.push([data.order.column, data.order.dir]);
        } else if (typeof data.order.column === 'object' && typeof data.order.dir === 'object') {
          if (data.order.column.length === data.order.dir.length) {
            data.order.column.forEach((item, i) => {
              order.push([item, data.order.dir[i]]);
            });
          } else {
            return reject('VALIDATION_ORDER_BAD_PARAMETER');
          }
        } else {
          return reject('VALIDATION_ORDER_BAD_PARAMETER');
        }
      }
      searchParams.order = order;

      let where = {
        status: 'close'
      };
      if (data.filters) {
        let date;
        let amount;
        Object.keys(data.filters).forEach(item => {
          switch (item) {
            case 'createdFrom':
              date = new Date(data.filters[item]);
              if (date != 'Invalid Date') {
                where.timeOpen = where.timeOpen ? _.assign(where.timeOpen, {$gte: date.getTime()}) : {$gte: date.getTime()};
              } else {
                return reject('VALIDATION_ORDER_BAD_DATE_PARAMETER');
              }
              break;
            case 'createdTo':
              date = new Date(data.filters[item]);
              if (date != 'Invalid Date') {
                where.timeOpen = where.timeOpen ? _.assign(where.timeOpen, {$lte: date.getTime()}) : {$lte: date.getTime()};
              } else {
                return reject('VALIDATION_ORDER_BAD_DATE_PARAMETER');
              }
              break;
            case 'amountFrom':
              amount = parseFloat(data.filters[item]);
              if (!isNaN(amount)) {
                where.amount = where.amount ? _.assign(where.amount, {$gte: amount}) : {$gte: amount};
              } else {
                return reject('VALIDATION_ORDER_BAD_AMOUNT_PARAMETER');
              }
              break;
            case 'amountTo':
              amount = parseFloat(data.filters[item]);
              if (!isNaN(amount)) {
                where.amount = where.amount ? _.assign(where.amount, {$lte: amount}) : {$lte: amount};
              } else {
                return reject('VALIDATION_ORDER_BAD_AMOUNT_PARAMETER');
              }
              break;
            case 'logins':
              where.logins = {$in: data.filters[item]};
              break;
            case 'currency':
              where.symbol = {$in: data.filters[item]};
              break;
          }
        });
      }
      searchParams.where = where;
      resolve(searchParams);
    });
  },

  _prepareResponse(result) {
    let columns = {};
    let filters = {};
    Object.keys(orderSchema).forEach(item => {
      columns[item] = {
        filtered: false
      };
      if (orderSchema[item].validate && orderSchema[item].validate.isIn) {
        filters[item] = {
          type: 'multilist',
          values: orderSchema[item].validate.isIn[0]
        };
      } else {
        filters[item] = {type: orderSchema[item].type};
      }
    });

    return orderModel.findAll({where: {status: 'close'}})
      .then(res => {
        return {
          columns: columns,
          filters: filters,
          rows: result,
          filtered: result.length,
          total: res.length
        };
      });
  },

  getOrdersHistory(data) {
    if (!data.symbol) {
      data.symbol = 'BTCUSD';
    }
    if (!data.limit) {
      data.limit = constants.ORDER.DEFAULT_LIMIT;
    }
    return orderModel.findAll({
      where: {symbol: data.symbol, status: 'close'},
      order: [['timeClose', 'DESC']],
      limit: data.limit
    })
      .then(res => {
        for (let i = 0; i < res.length; i++) {
          res[i] = res[i].get();
          delete res[i].clientId;
        }
        return res;
      });
  },

  _saveOrder(order) {
    let amount = order.type == 'buy' ? order.amount * order.price : order.amount;
    if (!order.volume) delete order.volume;
    return accountModel.checkBalance({currency: order.currency, clientId: order.clientId, amount: amount})
    // .then(() => {
    //   return orderModel.createR(order);
    // })
      .then(() => {
        return cryptoOrder.putIntoQueue(order);
      });
  },

  _putOrderFromBFX(order) {
    return orderModel.findByIdR(order.id).then(res => {
      if (!res) {
        return this._saveOrder(order);
      }
      order.currency = res.currency;
      order.status = res.status;
      order.timeOpen = res.timeOpen;
      order.timeClose = res.timeClose;
      if (!order.volume) delete order.volume;
      order.act = 'update';
      return cryptoOrder.putIntoQueue(order);
    });
  }

};

module.exports = OrderManager;
