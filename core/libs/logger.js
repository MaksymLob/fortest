const util = require('util');
const config = require(__dirroot + 'config');

// const chalk = require('chalk');
const Raven = require('raven');

class Logger {

  constructor() {
    this.opts = config.logger || {};
    this.opts.sentryOn = config.connections.sentry.enabled;
    console.log('LOG OPTIONS', this.opts);

    if (this.opts.sentryOn) {
      Raven.config(config.connections.sentry.dsn, {
        environment: config.connections.sentry.env,
        captureUnhandledRejections: true
      }).install();
    }
  }

  d(message, ...data) {
    if (this.opts.level !== 'debug') return;

    return this.log('debug', message, data);
  }

  i(message, ...data) {
    return this.log('info', message, data);
  }

  w(message, ...data) {
    return this.log('warn', message, data);
  }

  e(message, ...data) {
    this.log('error', message, data);
    return this.sentrySend(message, data);
  }

  c(message, ...data) {
    return this.log('critical', message, data);
  }

  sentrySend(message, data) {
    if (!this.opts.sentryOn) return;
    let stack = data ? data.stack : '';
    data = JSON.stringify(data);
    Raven.captureException(data, (err, eventId) => {
      if (err) {
        this.log('error', 'Failed to send captured exception to Sentry.', err);
      }
    });
  }

  log(type, message, data) {
    let date = new Date();
    let pre = date.getDate()+' '+
        date.toLocaleString("en-us", { month: "short" })+' '+
        date.getHours()+':'+
        ("0" + date.getMinutes()).slice(-2)+':'+
        ("0" + date.getSeconds()).slice(-2)+'.'+
        ("00" + date.getMilliseconds()).slice(-3)+' -';

    if (!data || data.length==0){
      data = '';
    }
    else if (data.length>1) {
      data = data.map((item) => { return JSON.stringify(item); });
      data = data.join(', '); //'\t'
    }
    else{
      data = data[0];
    }

    if (type=='debug') {
      type = 'log';
    }
    else if (type=='info') {
      // pre+=chalk.blue(' INFO:');
      pre+=' INFO:';
    }
    else if (type=='warn') {
      // pre+=chalk.bold.yellow(' WARN:');
      pre+=' WARN:';
    }
    else if (type=='error') {
      // pre+=chalk.bold.red(' ERROR:');
      pre+=' ERROR:';
    }
    else if (type=='critical') {
      type = 'error';
      // pre+=chalk.bold.bgRed.black(' !CRITICAL:');
      pre+=' !CRITICAL:';
    }

    return console[type](pre, message, data);
  }

  writeRequest(method, request) {
    if (request.environment && request.environment == 'test'
      || method === 'GET/v1.0/quotes/chart/history'
      || method === '/v1.0/common-resources'
      || (request.symbol != 'BTCUSD' || request.action != 'close') && (method == '/v1.0/orders/open' || method == '/v1.0/orders/cancel') && (request.clientId == 2 || request.clientId == 3)
    ) {
      return;
    }
    if (request.photoVerification) delete request.photoVerification;
    if (request.photoPassport) delete request.photoPassport;
    if ('object' === typeof request) request = JSON.stringify(request);
    this.d('REQ [' + method + ']:', request);
  }

  writeResponse(method, response) {
    if (method === 'GET/v1.0/quotes/chart/history'
      || method === '/v1.0/common-resources'
      || /*response.success && */(method == '/v1.0/orders/open' || method == '/v1.0/orders/cancel')
    ) {
      return;
    }
    if (method === 'GET/v1.0/users/addresses') {
      response.forEach(item => {
        delete item.privateKey;
      });
    }
    if ('object' === typeof response) response = JSON.stringify(response);
    this.d('RESP [' + method + ']:', response);
  }

}

let logger = new Logger();

module.exports = logger;
