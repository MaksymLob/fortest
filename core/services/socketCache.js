
class SocketCacheService {
  constructor() {
    this._store = {};
  }

  getById(id) {
    return this._store[id];
  }

  getByIdByProperty(id, property) {
    if (this._store[id] && this._store[id][property]) {
      return this._store[id][property];
    } else {
      return null;
    }
  }

  setById(id, value) {
    this._store[id] = value;
    return this;
  }

  setByIdByProperty(id, property, value) {
    if (!this._store[id]) {
      this._store[id] = {};
    }

    this._store[id][property] = value;
    return this;
  }

  unsetById(id) {
    delete this._store[id];
    return this;
  }

  unsetByIds(ids) {
    if (Array.isArray(ids)) {
      ids.forEach(id => this.unsetById(id));
    }
    return this;
  }

  unsetByIdByProperty(id, property) {
    if (this._store[id] && this._store[id][property]) {
      delete this._store[id][property];
    }
    return this;
  }
}

module.exports = new SocketCacheService();
