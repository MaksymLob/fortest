

module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/deposits/me': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'doDeposit',

    name: 'Get address for deposit',
    group: 'TRANSACTIONS',
    description: 'Get address for deposit',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'PUT/v1.0/deposits': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'updateDeposit',

    name: 'Update deposit and users balance',
    group: 'TRANSACTIONS',
    description: 'Update deposit and users balance',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/deposits/list': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'getAllDeposits',


    name: 'Get list of deposits',
    group: 'TRANSACTIONS',
    description: 'Get list of deposits',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/withdrawals/me': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'doWithdrawal',

    name: 'Create request for withdrawal',
    group: 'TRANSACTIONS',
    description: 'Create request for withdrawal',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      },
      amount: {
        type: 'Number',
        description: 'Amount withdrawal'
      },
      address: {
        type: 'String',
        description: 'Address for withdrawal'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/withdrawals/fees': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'getWithdrawalFees',


    name: 'Get withdrawal fees',
    group: 'TRANSACTIONS',
    description: 'Get withdrawal fees',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'PUT/v1.0/withdrawals': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'updateWithdrawal',

    name: 'Update withdrawal and users balance',
    group: 'TRANSACTIONS',
    description: 'Update withdrawal and users balance',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/withdrawals/list': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'getAllWithdrawals',

    name: 'Get list of withdrawals (for scheduler service)',
    group: 'TRANSACTIONS',
    description: 'Get list of withdrawals (for scheduler service)',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },
  'PUT/v1.0/system_transaction': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'addSystemTransaction',

    name: 'Add new System Transaction',
    group: 'TRANSACTIONS',
    description: 'Add new System Transaction',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  }
};
