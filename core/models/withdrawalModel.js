
const WithdrawalModel = {};

const withdrawalSchema = __app('models/schemas/withdrawal');
const Model = __app('models/model');
const schema = new Model(withdrawalSchema.schema);
WithdrawalModel.validations = withdrawalSchema.validations;
const withdrawalModel = schema.defineDataBase('mysql', 'withdrawals', WithdrawalModel);

module.exports = withdrawalModel;
