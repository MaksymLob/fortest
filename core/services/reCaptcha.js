const ReCaptcha = require('recaptcha2');
const config = require(__dirroot+'config');

const reCaptcha = new ReCaptcha({
  siteKey: config.reCaptcha.key,
  secretKey: config.reCaptcha.secret
});

module.exports = {
  validateReq(data) {
    if (!data.recaptcha) {
      return Promise.reject('VALIDATION_RECAPTCHA_ERROR');
    }
    if (data.recaptcha === 'test'/* && process.env.NODE_ENV === 'test'*/) {
      return Promise.resolve();
    }
    return reCaptcha.validate(data.recaptcha)
      .catch(() => {
        return Promise.reject('VALIDATION_RECAPTCHA_ERROR');
      });
  }
};
