
const SymbolModel = {};

const symbolSchema = __app('models/schemas/symbol');
const Model = __app('models/model');
const schema = new Model(symbolSchema.schema);
SymbolModel.validations = symbolSchema.validations;
const symbolModel = schema.defineDataBase('mysql', 'symbols', SymbolModel);

module.exports = symbolModel;
