

module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/quotes/current': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'quotesController',
    action: 'getCurrentQuotes',

    name: 'Get current values of all currency pair',
    group: 'QUOTES',
    description: 'Get current values of all currency pair',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/quotes/chart/config': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'quotesController',
    action: 'getConfigForChart',

    name: 'Get chart config',
    group: 'QUOTES',
    description: 'Get chart config',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/quotes/chart/history': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'quotesController',
    action: 'getQuotesHistory',

    name: 'Get quotes history',
    group: 'QUOTES',
    description: 'Get quotes history',
    params: {
      symbol: {
        type: 'String',
        description: 'Symbol'
      },
      resolution: {
        type: 'String',
        description: 'resolution ["5", "15", "30", "120", "240", "1D"]'
      },
      from: {
        type: 'TIMESTAMP',
        description: 'from without ms'
      },
      to: {
        type: 'TIMESTAMP',
        description: 'to without ms'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/quotes/chart/search': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'quotesController',
    action: 'getAllSymbols',

    name: 'Get all symbols',
    group: 'QUOTES',
    description: 'Get all symbols',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/quotes/chart/symbols': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'quotesController',
    action: 'getSymbolDetail',


    name: 'Get symbol details',
    group: 'QUOTES',
    description: 'Get symbol details',
    params: {
      symbol: {
        type: 'String',
        description: 'symbol Default BTCUSD'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  }
};
