define({ "api": [
  {
    "type": "GE",
    "url": "v1.0/register/confirm",
    "title": "Confirm Registration New User",
    "group": "AUTH",
    "description": "<p>Register New User</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Confirm Hash</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "GeV10RegisterConfirm",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/users/addresses",
    "title": "Get Users Public Key",
    "group": "AUTH",
    "description": "<p>Get Users Public Key</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>currency</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "GeV10UsersAddresses",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/2fa/activate",
    "title": "2FA Activate",
    "group": "AUTH",
    "description": "<p>Request for 2FA activation</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV102faActivate",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/2fa/confirm_activate",
    "title": "2FA ConfirmActivate",
    "group": "AUTH",
    "description": "<p>Confirm Activate 2FA</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV102faConfirm_activate",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/2fa/deactivate",
    "title": "2FA deactivate",
    "group": "AUTH",
    "description": "<p>deactivate2FA</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV102faDeactivate",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/2fa/login",
    "title": "2FA login",
    "group": "AUTH",
    "description": "<p>Login by 2FA</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isPartAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV102faLogin",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/balance/me/total",
    "title": "Get Total Balance",
    "group": "AUTH",
    "description": "<p>Get Total Balance</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10BalanceMeTotal",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/balances/me",
    "title": "Get Account Balances",
    "group": "AUTH",
    "description": "<p>Get Account Balances</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10BalancesMe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/forgotPassword",
    "title": "ForgotPassword",
    "group": "AUTH",
    "description": "<p>ForgotPassword. Send email with recovery password</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User e-mail</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Forgotpassword",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/limits/me",
    "title": "Get My Limits",
    "group": "AUTH",
    "description": "<p>Get My Limits</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10LimitsMe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/login",
    "title": "Login user",
    "group": "AUTH",
    "description": "<p>Login user. The first method if user hasn<code>t X-Token, return info about user with all accounts and updated X-Token. After this method you can receive stream of private user</code>s information: /balance/me/update, /orders/me/update, /trades/me/update, and public stream of quotes: /quotes/update ATTENTION: This method return success=true, if user has enabled 2FA (Complete login by sending /v1.0/2fa/login). After /v1.0/login user with 2FA has NO access to private methods.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>REQUIRED</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>REQUIRED</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "recaptcha",
            "description": "<p>REQUIRED</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.extID",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.X-Token",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.accounts",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.accounts.BTC",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.accounts.BTC.balance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.accounts.USD",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.accounts.USD.balance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.accounts.ETH",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.accounts.ETH.balance",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.success",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Login",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/logout",
    "title": "Logout user",
    "group": "AUTH",
    "description": "<p>Logout user</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Logout",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/resetPassword",
    "title": "ResetPassword",
    "group": "AUTH",
    "description": "<p>ResetPassword. Reset password by token from recovery email</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "hash",
            "description": "<p>Hash user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Resetpassword",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/subscribe",
    "title": "Subscribe to Symbol",
    "group": "AUTH",
    "description": "<p>Get subscribe</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>Symbol pair</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Subscribe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/unsubscribe",
    "title": "Unset Subscribe to Symbol",
    "group": "AUTH",
    "description": "<p>Unset Subscribe</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>Symbol pair</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10Unsubscribe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/users/me",
    "title": "Get User Info",
    "group": "AUTH",
    "description": "<p>Get User Info</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth, isBot"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>Client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10UsersMe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/users/me/details",
    "title": "Get User Detail",
    "group": "AUTH",
    "description": "<p>Get User Detail</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>Client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10UsersMeDetails",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/users/me/token",
    "title": "Get User Token",
    "group": "AUTH",
    "description": "<p>Get User Token</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10UsersMeToken",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/users/me/verify",
    "title": "Get User Verify",
    "group": "AUTH",
    "description": "<p>Get User Verify</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>Client ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "AUTH",
    "name": "Socket_ioV10UsersMeVerify",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/currencies",
    "title": "Get list of all platforms currencies",
    "group": "CRM",
    "description": "<p>Get list of all platforms currencies</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmCurrencies",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/email/send",
    "title": "Send email to user through platform",
    "group": "CRM",
    "description": "<p>Send email to user through platform</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "text",
            "description": "<p>text for client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "to",
            "description": "<p>Email to send</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "file",
            "description": "<p>File to send</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subject",
            "description": "<p>Subject email</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmEmailSend",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/orders",
    "title": "Get all closed orders(for crm)",
    "group": "CRM",
    "description": "<p>Get all closed orders(for crm)</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmOrders",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/register",
    "title": "Add new user from crm",
    "group": "CRM",
    "description": "<p>Add new user from crm</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>User Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>User Address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>User City</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>User State</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postalCode",
            "description": "<p>User PostalCode</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "phone",
            "description": "<p>User Phone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "birthday",
            "description": "<p>User Birthday</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "passportId",
            "description": "<p>User PassportId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoPassport",
            "description": "<p>User Photo Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoVerification",
            "description": "<p>User Photo Verification</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmRegister",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/user/balance",
    "title": "Get balance of user",
    "group": "CRM",
    "description": "<p>Get balance of user</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>login id of account</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmUserBalance",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/user/docs",
    "title": "Get documents for specific user(for crm)",
    "group": "CRM",
    "description": "<p>Get documents for specific user(for crm)</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email client</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmUserDocs",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/user/update",
    "title": "Update user from crm",
    "group": "CRM",
    "description": "<p>Update user from crm</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>User Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>User Address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>User City</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>User State</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postalCode",
            "description": "<p>User PostalCode</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "phone",
            "description": "<p>User Phone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "birthday",
            "description": "<p>User Birthday</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "passportId",
            "description": "<p>User PassportId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoPassport",
            "description": "<p>User Photo Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoVerification",
            "description": "<p>User Photo Verification</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmUserUpdate",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/user/verified",
    "title": "Verify or decline user",
    "group": "CRM",
    "description": "<p>Verify or decline user</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "success",
            "description": "<p>0 - reject users verification, 1 - approved</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reason",
            "description": "<p>if rejected</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmUserVerified",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/crm/withdrawals/approve",
    "title": "Approve withdrawal(for crm)",
    "group": "CRM",
    "description": "<p>Approve withdrawal(for crm)</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isCrm"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id  of withdrawal</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "CRM",
    "name": "PosV10CrmWithdrawalsApprove",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/orders/cancel",
    "title": "Cancel order",
    "group": "ORDERS",
    "description": "<p>Cancel order</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth, isBot"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Order ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10OrdersCancel",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/orders/current/opened",
    "title": "Get all current opened orders",
    "group": "ORDERS",
    "description": "<p>Get all current opened orders</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>BTCUSD by default</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "rank",
            "description": "<p>1 by default. It means precision</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>20 by default - amount of lines for book</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10OrdersCurrentOpened",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/orders/me/closed",
    "title": "Get current users closed orders",
    "group": "ORDERS",
    "description": "<p>Get current users closed orders</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10OrdersMeClosed",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/orders/me/opened",
    "title": "Get current users opened orders",
    "group": "ORDERS",
    "description": "<p>Get current users opened orders</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10OrdersMeOpened",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/orders/open",
    "title": "Open sell or buy order",
    "group": "ORDERS",
    "description": "<p>Open sell or buy order</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth, isBot"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>Externail ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>Symbol order</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type order buy or sell</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "price",
            "description": "<p>Price order</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Amount order</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10OrdersOpen",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/trades/history/current",
    "title": "Get current trades history",
    "group": "ORDERS",
    "description": "<p>Get current trades history</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>symbol default - BTCUSD</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "ORDERS",
    "name": "Socket_ioV10TradesHistoryCurrent",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/quotes/chart/config",
    "title": "Get chart config",
    "group": "QUOTES",
    "description": "<p>Get chart config</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "QUOTES",
    "name": "GeV10QuotesChartConfig",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/quotes/chart/history",
    "title": "Get quotes history",
    "group": "QUOTES",
    "description": "<p>Get quotes history</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>Symbol</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "resolution",
            "description": "<p>resolution [&quot;5&quot;, &quot;15&quot;, &quot;30&quot;, &quot;120&quot;, &quot;240&quot;, &quot;1D&quot;]</p>"
          },
          {
            "group": "Parameter",
            "type": "TIMESTAMP",
            "optional": false,
            "field": "from",
            "description": "<p>from without ms</p>"
          },
          {
            "group": "Parameter",
            "type": "TIMESTAMP",
            "optional": false,
            "field": "to",
            "description": "<p>to without ms</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "QUOTES",
    "name": "GeV10QuotesChartHistory",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/quotes/chart/search",
    "title": "Get all symbols",
    "group": "QUOTES",
    "description": "<p>Get all symbols</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "QUOTES",
    "name": "GeV10QuotesChartSearch",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/quotes/chart/symbols",
    "title": "Get symbol details",
    "group": "QUOTES",
    "description": "<p>Get symbol details</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "symbol",
            "description": "<p>symbol Default BTCUSD</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "QUOTES",
    "name": "GeV10QuotesChartSymbols",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/quotes/current",
    "title": "Get current values of all currency pair",
    "group": "QUOTES",
    "description": "<p>Get current values of all currency pair</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "QUOTES",
    "name": "Socket_ioV10QuotesCurrent",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/docs",
    "title": "Get index html of apiDoc",
    "group": "Resources",
    "description": "<p>Get index html of apidoc</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "Resources",
    "name": "GeV10Docs"
  },
  {
    "type": "socket.io",
    "url": "v1.0/common-resources",
    "title": "Get common resources",
    "group": "Resources",
    "description": "<p>Returns info about countries, currency and currency pair reductions.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.extID",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.countries",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.currencies",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.symbols",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "Resources",
    "name": "Socket_ioV10CommonResources",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/accounts/check",
    "title": "Checking if clients haven`t any currency-accounts",
    "group": "SYSTEM",
    "description": "<p>This method should be called after adding new currencies into config file. Method get all clients and check: if client hasn`t someone account, then this accounts is created</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.success",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "GeV10AccountsCheck",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/accounts/check-redis-accounts",
    "title": "Create accounts in Redis from MySQL",
    "group": "SYSTEM",
    "description": "<p>Get accounts from MySQL and search them in Redis and app's memory. Adds accounts if necessary. Also check balances and correct them.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.success",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "GeV10AccountsCheckRedisAccounts",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/reimport-private-keys",
    "title": "Import privateKeys from MySQL",
    "group": "SYSTEM",
    "description": "<p>Import BTC, LTC, DASH private keys from MySQL to blockchains.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.success",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "GeV10ReimportPrivateKeys",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/status",
    "title": "Checking status of platform",
    "group": "SYSTEM",
    "description": "<p>This method check connections to databases. Return status 200 if connections to mysql and redis are established, return 500 if one of them has error</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "GeV10Status",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/update_private_keys",
    "title": "Update privateKeys from MySQL",
    "group": "SYSTEM",
    "description": "<p>Import BTC, LTC, DASH private keys from MySQL (from accounts table) to blockchains.</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.success",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "GeV10Update_private_keys",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "POS",
    "url": "v1.0/init",
    "title": "Initialization of system clients (create accounts for them)",
    "group": "SYSTEM",
    "description": "<p>Description This method should be called only once time after the first running of application.Clients are created by migrations, but also it need create accounts for these clients. Current method get all system clients (role 5, 6, 3) and create accounts for them if it not exists. If return empty array then clients already initialized.If array is not empty then initialization was success</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>unique id of request to identify response</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "SYSTEM",
    "name": "PosV10Init",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "System",
    "url": "Error",
    "title": "Codes",
    "group": "System",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "1000",
            "optional": false,
            "field": "VALIDATION_AMOUNT_IS_NOT_FILLED",
            "description": "<p>amount is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1001",
            "optional": false,
            "field": "VALIDATION_TOKEN_IS_NOT_FILLED",
            "description": "<p>token is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1002",
            "optional": false,
            "field": "VALIDATION_ADDRESS_IS_NOT_FILLED",
            "description": "<p>address is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1003",
            "optional": false,
            "field": "VALIDATION_EMAIL_IS_NOT_FILLED",
            "description": "<p>email is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1004",
            "optional": false,
            "field": "VALIDATION_CLIENTID_IS_NOT_FILLED",
            "description": "<p>clientId is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1005",
            "optional": false,
            "field": "VALIDATION_SYMBOL_IS_NOT_FILLED",
            "description": "<p>symbol is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1008",
            "optional": false,
            "field": "VALIDATION_PASSWORD_IS_NOT_FILLED",
            "description": "<p>password is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1009",
            "optional": false,
            "field": "VALIDATION_ACCOUNTID_IS_NOT_FILLED",
            "description": "<p>accountId is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1010",
            "optional": false,
            "field": "VALIDATION_PRICE_IS_NOT_FILLED",
            "description": "<p>price is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1011",
            "optional": false,
            "field": "VALIDATION_TYPE_IS_NOT_FILLED",
            "description": "<p>type is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1012",
            "optional": false,
            "field": "VALIDATION_ORDERID_IS_NOT_FILLED",
            "description": "<p>id of order is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1013",
            "optional": false,
            "field": "VALIDATION_CURRENCY_IS_NOT_FILLED",
            "description": "<p>currency is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1014",
            "optional": false,
            "field": "VALIDATION_PHONE_IS_NOT_FILLED",
            "description": "<p>phone is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1015",
            "optional": false,
            "field": "VALIDATION_COUNTRY_IS_NOT_FILLED",
            "description": "<p>country is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1016",
            "optional": false,
            "field": "VALIDATION_FIRSTNAME_IS_NOT_FILLED",
            "description": "<p>firstname is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1017",
            "optional": false,
            "field": "VALIDATION_LASTNAME_IS_NOT_FILLED",
            "description": "<p>lastname is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1018",
            "optional": false,
            "field": "VALIDATION_CITY_IS_NOT_FILLED",
            "description": "<p>city is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1019",
            "optional": false,
            "field": "VALIDATION_STATE_IS_NOT_FILLED",
            "description": "<p>state is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1020",
            "optional": false,
            "field": "VALIDATION_POSTALCODE_IS_NOT_FILLED",
            "description": "<p>postalCode is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1021",
            "optional": false,
            "field": "VALIDATION_BIRTHDAY_IS_NOT_FILLED",
            "description": "<p>birthday is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1022",
            "optional": false,
            "field": "VALIDATION_PASSPORTID_IS_NOT_FILLED",
            "description": "<p>passportId is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1023",
            "optional": false,
            "field": "VALIDATION_PHOTOPASSPORT_IS_NOT_FILLED",
            "description": "<p>photoPassport is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1024",
            "optional": false,
            "field": "VALIDATION_PHOTOVERIFICATION_IS_NOT_FILLED",
            "description": "<p>photoVerification is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1025",
            "optional": false,
            "field": "VALIDATION_HASH_IS_NOT_FILLED",
            "description": "<p>hash is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1026",
            "optional": false,
            "field": "VALIDATION_INVALID_REFERRER",
            "description": "<p>Invalid referrer id.</p>"
          },
          {
            "group": "Parameter",
            "type": "3000",
            "optional": false,
            "field": "VALIDATION_PHONE_HAS_INCORRECT_TYPE",
            "description": "<p>phone has incorrect type</p>"
          },
          {
            "group": "Parameter",
            "type": "3001",
            "optional": false,
            "field": "VALIDATION_BIRTHDAY_HAS_INVALID_VALUE",
            "description": "<p>birthday has invalid value</p>"
          },
          {
            "group": "Parameter",
            "type": "3005",
            "optional": false,
            "field": "VALIDATION_COUNTRY_HAS_INVALID_VALUE",
            "description": "<p>country has invalid value</p>"
          },
          {
            "group": "Parameter",
            "type": "3006",
            "optional": false,
            "field": "VALIDATION_PRICE_HAS_INVALID_VALUE",
            "description": "<p>price has invalid value</p>"
          },
          {
            "group": "Parameter",
            "type": "3002",
            "optional": false,
            "field": "VALIDATION_PHOTOPASSPORT_HAS_INCORRECT_TYPE",
            "description": "<p>photoPassport should be jpg, png or bmp</p>"
          },
          {
            "group": "Parameter",
            "type": "3003",
            "optional": false,
            "field": "VALIDATION_PHOTOVERIFICATION_HAS_INCORRECT_TYPE",
            "description": "<p>photoVerification should be jpg, png or bmp</p>"
          },
          {
            "group": "Parameter",
            "type": "3004",
            "optional": false,
            "field": "VALIDATION_TOO_BIG_SIZE",
            "description": "<p>max size of images is 2Mb</p>"
          },
          {
            "group": "Parameter",
            "type": "1100",
            "optional": false,
            "field": "VALIDATION_NOT_ENOUGH_TIME_PASSED",
            "description": "<p>not enough time has passed for new faucet</p>"
          },
          {
            "group": "Parameter",
            "type": "1101",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_IS_NOT_PENDING_OR_APPROVED",
            "description": "<p>wanted withdrawal must be pending or approved</p>"
          },
          {
            "group": "Parameter",
            "type": "1101",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_IS_NOT_PENDING",
            "description": "<p>wanted withdrawal must be pending</p>"
          },
          {
            "group": "Parameter",
            "type": "1102",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_STATUS_IS_NOT_PROCESSED",
            "description": "<p>withdrawal status must be processed</p>"
          },
          {
            "group": "Parameter",
            "type": "1103",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_STATUS_IS_NOT_APPROVED_OR_DECLINED",
            "description": "<p>withdrawal status must be approved or declined</p>"
          },
          {
            "group": "Parameter",
            "type": "1104",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_AMOUNT_IS_TOO_SMALL",
            "description": "<p>withdrawal amount is too small</p>"
          },
          {
            "group": "Parameter",
            "type": "1105",
            "optional": false,
            "field": "VALIDATION_INSUFFICIENT_FUNDS",
            "description": "<p>Insufficient funds</p>"
          },
          {
            "group": "Parameter",
            "type": "1106",
            "optional": false,
            "field": "VALIDATION_WRONG_PASSWORD",
            "description": "<p>wrong password</p>"
          },
          {
            "group": "Parameter",
            "type": "1107",
            "optional": false,
            "field": "VALIDATION_WRONG_2FA_CODE",
            "description": "<p>wrong second factor auth code</p>"
          },
          {
            "group": "Parameter",
            "type": "1108",
            "optional": false,
            "field": "VALIDATION_2FA_CODE_IS_NOT_FILLED",
            "description": "<p>second factor auth code is not filled</p>"
          },
          {
            "group": "Parameter",
            "type": "1109",
            "optional": false,
            "field": "VALIDATION_RECAPTCHA_ERROR",
            "description": "<p>reCAPTCHA validation failed.</p>"
          },
          {
            "group": "Parameter",
            "type": "1110",
            "optional": false,
            "field": "VALIDATION_WITHDRAWAL_EXCEEDS_LIMIT",
            "description": "<p>Withdrawal amount exceeds user's day limit.</p>"
          },
          {
            "group": "Parameter",
            "type": "1200",
            "optional": false,
            "field": "VALIDATION_TYPE_HAS_INVALID_VALUE",
            "description": "<p>type should be more than 1</p>"
          },
          {
            "group": "Parameter",
            "type": "1201",
            "optional": false,
            "field": "VALIDATION_SYMBOL_HAS_INVALID_VALUE",
            "description": "<p>symbol should be BTCUSD, LTCUSD or LTCBTC</p>"
          },
          {
            "group": "Parameter",
            "type": "1300",
            "optional": false,
            "field": "VALIDATION_EMAIL_ALREADY_EXIST",
            "description": "<p>this email already exist</p>"
          },
          {
            "group": "Parameter",
            "type": "1301",
            "optional": false,
            "field": "VALIDATION_PHONE_ALREADY_EXIST",
            "description": "<p>this phone already exist</p>"
          },
          {
            "group": "Parameter",
            "type": "1400",
            "optional": false,
            "field": "VALIDATION_CRM_NO_ID",
            "description": "<p>Parameter id is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1401",
            "optional": false,
            "field": "VALIDATION_CRM_NO_ACCOUNTID",
            "description": "<p>Parameter accountId is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1402",
            "optional": false,
            "field": "VALIDATION_CRM_NO_STATUS",
            "description": "<p>Parameter status is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1403",
            "optional": false,
            "field": "VALIDATION_CRM_NO_AMOUNT",
            "description": "<p>Parameter amount is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1404",
            "optional": false,
            "field": "VALIDATION_CRM_NO_PAYMENT_SYSTEM",
            "description": "<p>Parameter payment_system is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1405",
            "optional": false,
            "field": "VALIDATION_CRM_NO_LOGIN",
            "description": "<p>Parameter login is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1406",
            "optional": false,
            "field": "VALIDATION_CRM_NO_EMAIL",
            "description": "<p>Parameter email is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1407",
            "optional": false,
            "field": "VALIDATION_CRM_NO_PHONE",
            "description": "<p>Parameter phone is required</p>"
          },
          {
            "group": "Parameter",
            "type": "1500",
            "optional": false,
            "field": "INSUFFICIENT_FUNDS_IN_SYSTEM_WALLET",
            "description": "<p>Insufficient funds in system wallet</p>"
          },
          {
            "group": "Parameter",
            "type": "1501",
            "optional": false,
            "field": "BAD_SYSTEM_WALLET_BALANCE",
            "description": "<p>Can't parse number from system wallet balance</p>"
          },
          {
            "group": "Parameter",
            "type": "1600",
            "optional": false,
            "field": "VALIDATION_ORDER_BAD_PARAMETER",
            "description": "<p>Bad order parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "1601",
            "optional": false,
            "field": "VALIDATION_ORDER_BAD_OFFSET_PARAMETER",
            "description": "<p>Bad offset parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "1602",
            "optional": false,
            "field": "VALIDATION_ORDER_BAD_DATE_PARAMETER",
            "description": "<p>Bad date parameter (must be datetime).</p>"
          },
          {
            "group": "Parameter",
            "type": "1603",
            "optional": false,
            "field": "VALIDATION_ORDER_BAD_AMOUNT_PARAMETER",
            "description": "<p>Bad amount parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "1604",
            "optional": false,
            "field": "VALIDATION_ORDER_BAD_PRICE_PARAMETER",
            "description": "<p>Bad price parameter</p>"
          },
          {
            "group": "Parameter",
            "type": "1605",
            "optional": false,
            "field": "VALIDATION_BAD_AMOUNT",
            "description": "<p>Bad amount</p>"
          },
          {
            "group": "Parameter",
            "type": "1700",
            "optional": false,
            "field": "USER_ALREADY_HAS_2FA",
            "description": "<p>2FA is already activated on this account.</p>"
          },
          {
            "group": "Parameter",
            "type": "1701",
            "optional": false,
            "field": "USER_HAS_NO_2FA",
            "description": "<p>2FA is deactivated on this account.</p>"
          },
          {
            "group": "Parameter",
            "type": "1702",
            "optional": false,
            "field": "USER_IS_NOT_ACTIVATED",
            "description": "<p>User is not activate. Please, confirm your email.</p>"
          },
          {
            "group": "Parameter",
            "type": "1703",
            "optional": false,
            "field": "USER_ALREADY_ACTIVATED",
            "description": "<p>User already activated.</p>"
          },
          {
            "group": "Parameter",
            "type": "1800",
            "optional": false,
            "field": "CONFIRMATION_TIME_IS_UP",
            "description": "<p>Confirmation time is up.</p>"
          },
          {
            "group": "Parameter",
            "type": "1900",
            "optional": false,
            "field": "INVALID_HASH",
            "description": "<p>Invalid hash.</p>"
          },
          {
            "group": "Parameter",
            "type": "2000",
            "optional": false,
            "field": "SOCIAL_NET_ERROR",
            "description": "<p>social net error</p>"
          },
          {
            "group": "Parameter",
            "type": "2001",
            "optional": false,
            "field": "SOCIAL_NET_INVALID_TOKEN",
            "description": "<p>invalid token</p>"
          },
          {
            "group": "Parameter",
            "type": "2002",
            "optional": false,
            "field": "SOCIAL_NET_TOKEN_EXPIRED",
            "description": "<p>token expired</p>"
          },
          {
            "group": "Parameter",
            "type": "3002",
            "optional": false,
            "field": "FORBIDDEN_CANNOT_PARSE_TOKEN",
            "description": "<p>Can't parse token</p>"
          },
          {
            "group": "Parameter",
            "type": "3003",
            "optional": false,
            "field": "FORBIDDEN_NO_ACCESS_RIGHTS",
            "description": "<p>No access rights</p>"
          },
          {
            "group": "Parameter",
            "type": "3004",
            "optional": false,
            "field": "FORBIDDEN_XTOKEN_EXPIRED",
            "description": "<p>X-Token is expired</p>"
          },
          {
            "group": "Parameter",
            "type": "3005",
            "optional": false,
            "field": "FORBIDDEN_NO_AUTH",
            "description": "<p>You are not authorized</p>"
          },
          {
            "group": "Parameter",
            "type": "4000",
            "optional": false,
            "field": "NOT_FOUND_METHOD",
            "description": "<p>Method is not found</p>"
          },
          {
            "group": "Parameter",
            "type": "4001",
            "optional": false,
            "field": "NOT_FOUND_CLIENT",
            "description": "<p>Client is not found</p>"
          },
          {
            "group": "Parameter",
            "type": "4002",
            "optional": false,
            "field": "NOT_FOUND_ORDER",
            "description": "<p>Order is not found</p>"
          },
          {
            "group": "Parameter",
            "type": "4003",
            "optional": false,
            "field": "NOT_FOUND_ACCOUNT",
            "description": "<p>Account is not found</p>"
          },
          {
            "group": "Parameter",
            "type": "4003",
            "optional": false,
            "field": "NOT_FOUND_ACCOUNTS",
            "description": "<p>Accounts are not found for this client</p>"
          },
          {
            "group": "Parameter",
            "type": "4004",
            "optional": false,
            "field": "NOT_FOUND_WITHDRAWAL",
            "description": "<p>Withdrawal is not found</p>"
          },
          {
            "group": "Parameter",
            "type": "5000",
            "optional": false,
            "field": "SEND_EMAIL_FILE_ERROR",
            "description": "<p>Error with file in send Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "5001",
            "optional": false,
            "field": "SEND_EMAIL_FAILED",
            "description": "<p>Failed send Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "5002",
            "optional": false,
            "field": "SEND_EMAIL_MISSING_PARAMS",
            "description": "<p>Request MUST contains parameters <code>text</code>, <code>to</code>.</p>"
          },
          {
            "group": "Parameter",
            "type": "5003",
            "optional": false,
            "field": "SEND_EMAIL_TOO_LARGE_FILE",
            "description": "<p>File size is too large.</p>"
          },
          {
            "group": "Parameter",
            "type": "5100",
            "optional": false,
            "field": "REDIS_TRANSACTION_ERROR",
            "description": "<p>redis transaction error</p>"
          },
          {
            "group": "Parameter",
            "type": "5101",
            "optional": false,
            "field": "REDIS_INTERNAL_ERROR",
            "description": "<p>internal redis error</p>"
          },
          {
            "group": "Parameter",
            "type": "5102",
            "optional": false,
            "field": "S3_UPLOADING_ERROR",
            "description": "<p>error with s3 uploading</p>"
          },
          {
            "group": "Parameter",
            "type": "5103",
            "optional": false,
            "field": "S3_DELETING_ERROR",
            "description": "<p>error with s3 deleting</p>"
          },
          {
            "group": "Parameter",
            "type": "5120",
            "optional": false,
            "field": "MAIL_SENDING_ERROR",
            "description": "<p>error with mail sending</p>"
          },
          {
            "group": "Parameter",
            "type": "9999",
            "optional": false,
            "field": "UNKNOWN_ERROR",
            "description": "<p>Unknown error</p>"
          },
          {
            "group": "Parameter",
            "type": "7020",
            "optional": false,
            "field": "USER_ALREADY_VERIFIED",
            "description": "<p>user already verified</p>"
          },
          {
            "group": "Parameter",
            "type": "7021",
            "optional": false,
            "field": "WAITING_VERIFICATION_CONFIRMATION",
            "description": "<p>request already is sent. Wait please confirmation</p>"
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "System",
    "name": "SystemError"
  },
  {
    "type": "GE",
    "url": "v1.0/deposits/list",
    "title": "Get list of deposits",
    "group": "TRANSACTIONS",
    "description": "<p>Get list of deposits</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "GeV10DepositsList",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GE",
    "url": "v1.0/withdrawals/list",
    "title": "Get list of withdrawals (for scheduler service)",
    "group": "TRANSACTIONS",
    "description": "<p>Get list of withdrawals (for scheduler service)</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "GeV10WithdrawalsList",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "PU",
    "url": "v1.0/deposits",
    "title": "Update deposit and users balance",
    "group": "TRANSACTIONS",
    "description": "<p>Update deposit and users balance</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "PuV10Deposits",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "PU",
    "url": "v1.0/system_transaction",
    "title": "Add new System Transaction",
    "group": "TRANSACTIONS",
    "description": "<p>Add new System Transaction</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "PuV10System_transaction",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "PU",
    "url": "v1.0/withdrawals",
    "title": "Update withdrawal and users balance",
    "group": "TRANSACTIONS",
    "description": "<p>Update withdrawal and users balance</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "PuV10Withdrawals",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/deposits/me",
    "title": "Get address for deposit",
    "group": "TRANSACTIONS",
    "description": "<p>Get address for deposit</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "Socket_ioV10DepositsMe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/withdrawals/fees",
    "title": "Get withdrawal fees",
    "group": "TRANSACTIONS",
    "description": "<p>Get withdrawal fees</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "Socket_ioV10WithdrawalsFees",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/withdrawals/me",
    "title": "Create request for withdrawal",
    "group": "TRANSACTIONS",
    "description": "<p>Create request for withdrawal</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "extID",
            "description": "<p>External ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Amount withdrawal</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address for withdrawal</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "TRANSACTIONS",
    "name": "Socket_ioV10WithdrawalsMe",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "PU",
    "url": "v1.0/users/:id/limits",
    "title": "Get Users Limits",
    "group": "USER",
    "description": "<p>Get Users Limits</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isSystem"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clientId",
            "description": "<p>client ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>Currency</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "amount",
            "description": "<p>Amount</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "USER",
    "name": "PuV10UsersIdLimits",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/check_referrer_id",
    "title": "Check referrer id",
    "group": "USER",
    "description": "<p>Check referrer id</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "referrer",
            "description": "<p>Referrer ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "USER",
    "name": "Socket_ioV10Check_referrer_id",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/get_referrer_id",
    "title": "Get referrer id",
    "group": "USER",
    "description": "<p>Get referrer id</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAuth"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "USER",
    "name": "Socket_ioV10Get_referrer_id",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  },
  {
    "type": "socket.io",
    "url": "v1.0/register",
    "title": "Register New User",
    "group": "USER",
    "description": "<p>Register New User</p>",
    "version": "1.0.0",
    "permission": [
      {
        "name": "isAll"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>User Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>VALIDATION_2FA_CODE</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>User Address</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "city",
            "description": "<p>User City</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "state",
            "description": "<p>User State</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "postalCode",
            "description": "<p>User PostalCode</p>"
          },
          {
            "group": "Parameter",
            "type": "integer",
            "optional": false,
            "field": "phone",
            "description": "<p>User Phone</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "birthday",
            "description": "<p>User Birthday</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "passportId",
            "description": "<p>User PassportId</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoPassport",
            "description": "<p>User Photo Passport</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "photoVerification",
            "description": "<p>User Photo Verification</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>200</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "apidoc/DOCS/docs.js",
    "groupTitle": "USER",
    "name": "Socket_ioV10Register",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>not 200. Can be 400, 401, 403, 404, 500</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>number of error</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>text description of error</p>"
          }
        ]
      }
    }
  }
] });
