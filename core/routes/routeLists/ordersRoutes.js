
module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/orders/open': {
    access: ['isAuth', 'isBot'],
    tokenRequired: true,
    controllerName: 'ordersController',
    action: 'openOrder',


    name: 'Open sell or buy order',
    group: 'ORDERS',
    description: 'Open sell or buy order',
    params: {
      extID: {
        type: 'String',
        description: 'Externail ID'
      },
      symbol: {
        type: 'String',
        description: 'Symbol order'
      },
      type: {
        type: 'String',
        description: 'Type order buy or sell'
      },
      price: {
        type: 'Number',
        description: 'Price order'
      },
      amount: {
        type: 'Number',
        description: 'Amount order'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/orders/cancel': {
    access: ['isAuth', 'isBot'],
    tokenRequired: true,
    controllerName: 'ordersController',
    action: 'cancelOrder',

    name: 'Cancel order',
    group: 'ORDERS',
    description: 'Cancel order',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      },
      id: {
        type: 'String',
        description: 'Order ID'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/orders/current/opened': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'ordersController',
    action: 'getCurrentOpenedOrders',

    name: 'Get all current opened orders',
    group: 'ORDERS',
    description: 'Get all current opened orders',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      },
      symbol: {
        type: 'String',
        description: 'BTCUSD by default'
      },
      rank: {
        type: 'Number',
        description: '1 by default. It means precision'
      },
      count: {
        type: 'Number',
        description: '20 by default - amount of lines for book'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/orders/me/opened': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'ordersController',
    action: 'getMyOpenedOrders',


    name: 'Get current users opened orders',
    group: 'ORDERS',
    description: 'Get current users opened orders',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/orders/me/closed': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'ordersController',
    action: 'getMyClosedOrders',

    name: 'Get current users closed orders',
    group: 'ORDERS',
    description: 'Get current users closed orders',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/trades/history/current': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'ordersController',
    action: 'getOrdersHistory',

    name: 'Get current trades history',
    group: 'ORDERS',
    description: 'Get current trades history',
    params: {
      extID: {
        type: 'String',
        description: 'External ID'
      },
      symbol: {
        type: 'String',
        description: 'symbol default - BTCUSD'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  }
};
