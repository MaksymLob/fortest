
const constants = __app('libs/constants');
const transactionMngr = __app('managers/transactionsManager');

const TransactionsCtrl = {
  doDeposit(req, res) {
    req.body.clientId = req.access.clientId;
    transactionMngr.doDeposit(req.body)
      .then(result => res.answer({status: 200, data: {address: result}}))
      .catch(err => res.error(err));
  },

  doWithdrawal(req, res) {
    req.body.clientId = req.access.clientId;
    transactionMngr.doWithdrawal(req.body)
      .then(result => {
        delete result.clientId;
        delete result.id;
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  updateDeposit(req, res) {
    transactionMngr.updateDeposit(req.body)
      .then(result => res.answer({status: 200, data: result}))
      .catch(err => {
        res.error(err);
      });
  },

  updateWithdrawal(req, res) {
    transactionMngr.updateWithdrawal(req.body)
      .then(result => res.answer({status: 200, data: result}))
      .catch(err => res.error(err));
  },

  approveWithdrawal(req, res) {
    transactionMngr.handleWithdrawal(req.body)
      .then(() => res.answer({status: 200, data: {code: 200, data: {transactionId: req.body.id}}}))
      .catch(err => res.error(err));
  },

  getAllWithdrawals(req, res) {
    transactionMngr.getAllWithdrawals(req.query)
      .then(result => res.answer({status: 200, data: result}))
      .catch(err => res.error(err));
  },

  getAllDeposits(req, res) {
    transactionMngr.getAllDeposits(req.query)
      .then(result => res.answer({status: 200, data: result}))
      .catch(err => res.error(err));
  },

  getWithdrawalFees(req, res) {
    res.answer({status: 200, data: constants.WITHDRAWAL_FEE});
  },

  addSystemTransaction(req, res) {
    transactionMngr.addSystemTransaction(req.body)
      .then(result => res.answer({status: 200, data: result}))
      .catch(err => res.error(err));
  }
};

module.exports = TransactionsCtrl;
