
const _ = require('lodash');
const Sequelize = require('sequelize');
const Redis = __app('services/redis');
const redis = new Redis();
const Mysql = __app('services/mysql');
const mysql = new Mysql();
const mysqlCon = mysql.getConnection();

class Model {
  constructor(schema) {
    // this.schema = _.cloneDeep(schema);
    this.schema = schema;
    this._database = '';
  }

  defineDataBase(db, table, model) {
    switch (db) {
      case 'redis':
        this._database = 'redis';
        return this._defineRedis(this, table, model);
        break;
      case 'mysql':
        this._database = 'mysql';
        return this._defineMysql(table, model);
        break;
      case 'mysql-redis':
        this._database = 'mysql-redis';
        let newModel = this._defineMysql(table, model);
        return this._defineRedis(newModel, table, model);
        break;
      default:
        throw 'INTERNAL_ERROR_UNDEFINED_DATABASE';
    }
  }

  validateData(data, formats) {
    return new Promise((resolve, reject) => {
      let newData = {};
      let schema = formats ? this.validations[formats] : this.schema;
      for (let field in schema) {
        if (schema.hasOwnProperty(field)) {
          if (!this._hasRequiredField(data, field, schema[field])) {
            return reject('VALIDATION_' + field.toUpperCase().replace(/-/g, '') + '_IS_NOT_FILLED');
          }
          if (!this._hasCorrectTypeField(data, field, schema[field])) {
            return reject('VALIDATION_' + field.toUpperCase().replace(/-/g, '') + '_HAS_INCORRECT_TYPE');
          }
          if (!this._isInRange(data, field, schema[field])) {
            return reject('VALIDATION_' + field.toUpperCase().replace(/-/g, '') + '_HAS_INVALID_VALUE');
          }

          let defaultValue = this._database === 'redis' ? '' : undefined;
          let helper = typeof schema[field].defaultValue !== 'undefined' ? schema[field].defaultValue : defaultValue;
          newData[field] = (data[field] || data[field] === 0 || typeof data[field] === 'boolean') ? data[field] : helper;
        }
      }
      resolve(newData);
    });
  }

  // ****************************  PRIVATE METHODS  ***************************

  _defineRedis(_this, table, model) {
    let options = _this.attributes ? _this.attributes : _this.schema;
    _this.createR = data => {
      return redis.create(data, table, options);
    };
    _this.findAllR = data => {
      return redis.findAll(data, table, options);
    };
    _this.findByIdR = (id, parentId) => {
      if (parentId) {
        return redis.findById(id, table + parentId, options);
      }
      return redis.findById(id, table, options);
    };
    _this.updateR = (data, where) => {
      return redis.update(data, where, table, options);
    };
    _this.deleteR = data => {
      return redis.delete(data, table, options);
    };
    _this.saveForSearchingByParamR = (paramName, param, data) => {
      return redis.saveForSearchingByParam(paramName, param, data, table, options);
    };
    for (let func in model.classMethods) {
      if (model.classMethods.hasOwnProperty(func)) {
        _this[func] = model.classMethods[func];
      }
    }
    return _this;
  }

  _defineMysql(table, model) {
    let schema = _.cloneDeep(this.schema);
    for (let field in this.schema) {
      if (this.schema[field].type === 'string') schema[field].type = Sequelize.STRING;
      if (this.schema[field].type === 'integer') schema[field].type = Sequelize.BIGINT;
      if (this.schema[field].type === 'double') schema[field].type = Sequelize.DOUBLE;
      if (this.schema[field].type === 'boolean') schema[field].type = Sequelize.BOOLEAN;
    }

    let newModel = mysqlCon.define(table, schema, {
      charset: 'utf8',
      timestamps: false,
      classMethods: model && model.classMethods ? model.classMethods : {},
      instanceMethods: model && model.instanceMethods ? model.instanceMethods : {}
    });
    newModel.validations = model && model.validations || {};
    this.validations = model && model.validations || {};
    // schema fell into the attributes
    newModel.validateData = (data, formats) => {
      return this.validateData(data, formats);
    };
    newModel.getAll = data => {
      return newModel.findAll(data)
        .then(res => {
          let results = [];
          for (let i = 0; i < res.length; i++) {
            results.push(res[i] && res[i].get() ? res[i].get() : res[i]);
          }
          return Promise.resolve(results);
        });
    };
    return newModel;
  }

  _hasRequiredField(data, fieldName, field) {
    if (!field.allowNull
      && typeof field.defaultValue === 'undefined'
      && !field.autoIncrement
      && (!_.has(data, fieldName) || data[fieldName] === '' || typeof data[fieldName] === 'undefined')) {
      return false;
    }
    return true;
  }

  _hasCorrectTypeField(data, fieldName, field) {
    if ((field.type === 'integer' || field.type === 'double') && typeof data[fieldName] === 'string' && isFinite(Number(data[fieldName]))) {
      data[fieldName] = Number(data[fieldName]);
    }
    if (field.type === 'string' && typeof data[fieldName] !== 'undefined') {
      data[fieldName] = String(data[fieldName]);
    }
    if (data[fieldName] && typeof data[fieldName] === 'string' && field.type !== 'string') {
      return false;
    }
    if (data[fieldName] && typeof data[fieldName] === 'number' && field.type !== 'integer' && field.type !== 'double') {
      return false;
    }
    return true;
  }

  _isInRange(data, fieldName, field) {
    if (field.validate && field.validate.isIn && typeof data[fieldName] !== 'undefined') {
      if (field.validate.isIn[0].indexOf(data[fieldName]) < 0) {
        return false;
      }
    }
    if (field.validate && typeof field.validate.min !== 'undefined' && typeof data[fieldName] !== 'undefined') {
      if (data[fieldName] < field.validate.min) {
        return false;
      }
    }
    if (field.validate && typeof field.validate.max !== 'undefined' && typeof data[fieldName] !== 'undefined') {
      if (data[fieldName] > field.validate.max) {
        return false;
      }
    }
    if (field.validateData && field.validateData.regexp && typeof data[fieldName] !== 'undefined') {
      if (!field.validateData.regexp.test(data[fieldName])) {
        return false;
      }
    }
    return true;
  }
}

module.exports = Model;
