
const _ = require('lodash');
const config = require(__dirroot + 'config');
const constants = __app('libs/constants');
const GE = __app('libs/GE');
const scheduler = __app('services/scheduler');

const accountMngr = __app('managers/accountsManager');
const accountModel = __app('models/accountModel');
const accountKeyModel = __app('models/accountKeyModel');
const clientModel = __app('models/clientModel');
const systemTrscModel = __app('models/systemTransactionModel');
const withdrawalModel = __app('models/withdrawalModel');
const depositModel = __app('models/depositModel');
const CryptoOrder = __app('services/exchange/cryptoOrders');
const crm = __app('services/crm');
const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();
const cryptoOrder = new CryptoOrder();


const DepositManager = {

  doDeposit(data) {
    return depositModel.validateData(data)
      .then(() => {
        return accountKeyModel.findOne({where: {clientId: data.clientId, currency: data.currency}});
      })
      .then(res => {
        let address;
        if (data.currency === 'XEM' || data.currency === 'ETA') {
          address = res.get('address');
        } else {
          address = res.get('publicKey');
        }

        if (address) {
          return address;
        } else {
          return this._generateKeyPair(data.clientId, data.currency);
        }
      });
  },

  doWithdrawal(data) {
    let newWithdrawal;
    let result;
    let remainedDayLimit;
    let dayUsdLimit;
    let currencyPrice;
    data.timeCreation = Date.now();
    return withdrawalModel.validateData(data)
      .then(res => {
        newWithdrawal = res;
        return this._checkWithdrawalAmount(newWithdrawal.amount, newWithdrawal.currency);
      })
      .then(() => {
        currencyPrice = quotes.getCurrentQuote({symbol: `${data.currency}USD`}).currentPrice;
        return Promise.all([
          clientModel.checkWithdrawalLimit(newWithdrawal.clientId, newWithdrawal.amount, currencyPrice),
          accountModel.checkBalance(newWithdrawal)
        ]);
      })
      .then(res => {
        remainedDayLimit = res[0].remainedDayUsd;
        dayUsdLimit = res[0].dayUsdLimit;
        newWithdrawal.fee = constants.WITHDRAWAL_FEE[data.currency];
        return withdrawalModel.create(newWithdrawal);
      })
      .then(res => {
        result = res.get();
        return Promise.all([
          accountKeyModel.findOne({where: {clientId: data.clientId, currency: data.currency}}),
          clientModel.findById(data.clientId)
        ]);
      })
      .then(res => {
        return crm.getClientInfo({login: res[0].get('id'), email: res[1].get('email')});
      })
      .then(res => {
        return Promise.all([
          this._updateDayLimit(remainedDayLimit - currencyPrice * data.amount, data.clientId, dayUsdLimit),
          crm.sendAddOrUpdateWithdrawal({
            accountId: res.account.id,
            clientId: res.id,
            amount: data.amount,
            status: constants.CRM_STATUSES.WITHDRAWAL.PENDING,
            id: result.id
          })
        ]);
      })
      .then(() => {
        return cryptoOrder._freezeBalance(null, data.clientId, data.currency, data.amount);
      })
      .then(() => {
        return cryptoOrder._getBalanceAndUsed(data.clientId, data.currency);
      })
      .then(res => {
        GE.emit('/balance/me/update', {
          clientId: data.clientId,
          accounts: {
            balance: res.balance,
            used: res.used,
            currency: data.currency
          }
        });
        return result;
      });
  },

  updateDeposit(data) {
    let deposit = {};
    return depositModel.findOne({where: {txId: data.txid, clientId: data.clientId}})
      .then(res => {
        if (res && res.get('status') === constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS) {
          data.status = constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS;
          return;
        }
        deposit.status = (data.confirmations >= this._getMinConfirmations(data.currency))
          ? constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS
          : constants.TRANSACTION_STATUSES.DEPOSIT.PENDING;
        deposit.clientId = data.clientId;
        deposit.currency = data.currency;
        deposit.fee = data.fee;
        deposit.amount = data.amount;
        deposit.txId = data.txid;
        if (res) {
          return depositModel.update({status: deposit.status}, {where: {id: res.get('id')}});
        } else {
          return depositModel.create(deposit);
        }
      })
      .then(() => {
        if (deposit.status === constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS) {
          cryptoOrder.updateBalance({clientId: data.clientId, currency: data.currency, amount: data.amount});
          accountMngr.synchronizeBalance([{clientId: data.clientId, currency: data.currency, amount: data.amount}]);

          return this._sendDepositToCrm({
            clientId: data.clientId,
            currency: data.currency,
            txId: deposit.txId,
            amount: deposit.amount,
            status: constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS
          });
        }
        return data;
      });
  },

  updateWithdrawal(data) {
    let withdrawal = _.cloneDeep(data);
    let account = {};
    return accountKeyModel.findOne({where: {clientId: data.clientId, currency: data.currency}})
      .then(result => {
        if (!result) {
          withdrawal.status = constants.TRANSACTION_STATUSES.WITHDRAWAL.FAILED;
          return withdrawalModel.upsert(withdrawal);
        }
        account = result.get();
        withdrawal.status = constants.TRANSACTION_STATUSES.WITHDRAWAL.SUCCESS;
        return withdrawalModel.upsert(withdrawal)
          .then(res => {
            if (res === true || res === false) {
              cryptoOrder._freezeBalance(null, data.clientId, data.currency, -data.amount);
              cryptoOrder.updateBalance({
                clientId: data.clientId,
                currency: data.currency,
                amount: -data.amount
              });
              return Promise.all([
                clientModel.findById(data.clientId),
                accountMngr.synchronizeBalance([{clientId: data.clientId, currency: data.currency, amount: -data.amount}])
              ]);
            }
          })
          .then(res => {
            return crm.getClientInfo({login: account.id, email: res[0].get('email')});
          })
          .then(res => {
            return crm.sendAddOrUpdateWithdrawal({
              accountId: res.account.id,
              clientId: res.id,
              amount: withdrawal.amount,
              status: constants.CRM_STATUSES.WITHDRAWAL.PROCESSED,
              id: withdrawal.id
            });
          });
      });
  },

  handleWithdrawal(data) {
    return withdrawalModel.findById(data.id)
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_WITHDRAWAL');
        }
        let withdrawal = res.get();
        if (res.get('status') === constants.TRANSACTION_STATUSES.WITHDRAWAL.PENDING) {
          switch (Number(data.status)) {
            case constants.CRM_STATUSES.WITHDRAWAL.APPROVED:
              return this._approveWithdrawal(withdrawal);
            case constants.CRM_STATUSES.WITHDRAWAL.DECLINED:
              return this._declineWithdrawal(withdrawal, data);
            default:
              return Promise.reject('VALIDATION_WITHDRAWAL_STATUS_IS_NOT_APPROVED_OR_DECLINED');
          }
        } else {
          return Promise.reject('VALIDATION_WITHDRAWAL_IS_NOT_PENDING');
        }
      });
  },

  getAllWithdrawals(data) {
    if (data.timeConfirmation) {
      data.timeConfirmation = JSON.parse(data.timeConfirmation);
    }
    let filter = data ? {where: data} : {};
    return withdrawalModel.getAll(filter);
  },

  getAllDeposits(data) {
    let filter = data ? {where: data} : {};
    return depositModel.getAll(filter);
  },

  addSystemTransaction(data) {
    return systemTrscModel.create(data);
  },

  /* ******************************  Private methods  *******************************/

  _approveWithdrawal(withdrawal) {
    return accountKeyModel.findOne({where: {clientId: withdrawal.clientId, currency: withdrawal.currency}})
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_ACCOUNT');
        }
        if (res.get('balance') < withdrawal.amount) {
          return Promise.reject('VALIDATION_INSUFFICIENT_FUNDS');
        }
        return scheduler.createWithdrawalTransaction(withdrawal);
      })
      .then(res => {
        if (withdrawal.currency === 'XEM') {
          withdrawal.fee = res.fee;
        }
        log.i('sendTransaction response: ', res);
        withdrawal.status = constants.TRANSACTION_STATUSES.WITHDRAWAL.APPROVED;
        withdrawal.timeConfirmation = Date.now();
        withdrawal.txId = res;
        return withdrawalModel.upsert(withdrawal);
      });
  },

  _declineWithdrawal(withdrawal, data) {
    const currencyPrice = quotes.getCurrentQuote({symbol: `${withdrawal.currency}USD`}).currentPrice;
    return clientModel.findOne({where: {id: withdrawal.clientId}})
      .then(res => {
        // TODO вот тут неправильно, котировка могла смениться. Поэтому возвращать надо ровно столько,
        // TODO сколько было на момент создания виздровала. Где-то надо будет хранить котировку
        const newAmount = res.get('remainedDayUsd') + currencyPrice * withdrawal.amount;
        withdrawal.status = constants.TRANSACTION_STATUSES.WITHDRAWAL.REJECTED;
        return Promise.all([
          withdrawalModel.upsert(withdrawal),
          this._updateDayLimit(newAmount, withdrawal.clientId, res.get('dayUsdLimit'))
        ]);
      })
      .then(() => {
        return cryptoOrder._freezeBalance(null, withdrawal.clientId, withdrawal.currency, -withdrawal.amount);
      })
      .then(() => {
        return cryptoOrder._getBalanceAndUsed(withdrawal.clientId, withdrawal.currency);
      })
      .then(res => {
        GE.emit('/balance/me/update', {
          clientId: withdrawal.clientId,
          accounts: {
            balance: res.balance,
            used: res.used,
            currency: withdrawal.currency
          }
        });
      });
  },

  _generateKeyPair(clientId, currency) {
    let key;
    return scheduler.generateKeyPair(currency, clientId)
      .then(res => {
        key = res;
        return accountKeyModel.update(key, {where: {clientId: clientId, currency: currency}});
      })
      .then(() => {
        return key.publicKey;
      });
  },

  _checkWithdrawalAmount(amount, currency) {
    return new Promise((resolve, reject) => {
      const fee = constants.WITHDRAWAL_FEE[currency.toUpperCase()];
      if (fee * constants.WITHDRAWAL_EXCESS_COEFFICIENT > amount) {
        return reject('VALIDATION_WITHDRAWAL_AMOUNT_IS_TOO_SMALL');
      } else {
        return resolve();
      }
    });
  },

  _getMinConfirmations(currency) {
    if (config.currencies[currency] && typeof config.currencies[currency].confirmations !== 'undefined') {
      return config.currencies[currency].confirmations;
    } else {
      return constants.DEFAULT_CONFIRM_NUMBER;
    }
  },

  _sendDepositToCrm(data) {
    return Promise.all([
      accountKeyModel.findOne({where: {clientId: data.clientId, currency: data.currency}}),
      clientModel.findById(data.clientId),
      depositModel.findOne({where: {txId: data.txId}})
    ])
      .then(res => {
        data.id = res[2].get('id');
        data.accountId = res[0].get('id');
        return crm.getClientInfo({login: data.accountId, email: res[1].get('email')});
      })
      .then(res => {
        return crm.sendAddOrUpdateDeposit({
          id: data.id,
          amount: data.amount,
          clientId: res.id,
          accountId: res.account.id,
          payment_system: constants.PAYMENT_SYSTEM
        });
      })
      .catch(err => {
        log.e('Failed send deposit to CRM: Stack', err.message);
      });
  },

  _updateDayLimit(newValue, id, limit) {
    return clientModel.update({remainedDayUsd: newValue}, {where: {id: id}})
      .then(() => {
        GE.emit('/limits/me/update', {
          dayUsdLimit: limit,
          remainedDayUsd: newValue,
          clientId: id
        });
      });
  }

};

module.exports = DepositManager;
