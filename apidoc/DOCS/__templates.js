/**
 * @apiDefine ListParams
 * @apiParam {Array} [where] Where conditions
 * @apiParam {Array} [order] Ordering params [field, direction]
 * @apiParam {integer} [limit] Limit of list
 * @apiParam {integer} [offset] Offset of list
 *
 * @apiSuccess {Array} list Data list
 */

 /**
 * @apiDefine defErrorResp
 * @apiError {Number} status not 200. Can be 400, 401, 403, 404, 500
 * @apiError {Object} data
 * @apiError {Number} data.code number of error
 * @apiError {String} data.message text description of error
 */