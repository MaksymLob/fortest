
module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/register': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'addNewUser',

    name: 'Register New User',
    group: 'USER',
    description: 'Register New User',
    params: {
      country: {
        type: 'String',
        description: 'User Country'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      },
      address: {
        type: 'string',
        description: 'User Address'
      },
      city: {
        type: 'string',
        description: 'User City'
      },
      state: {
        type: 'string',
        description: 'User State'
      },
      postalCode: {
        type: 'string',
        description: 'User PostalCode'
      },
      phone: {
        type: 'integer',
        description: 'User Phone'
      },
      birthday: {
        type: 'string',
        description: 'User Birthday'
      },
      passportId: {
        type: 'string',
        description: 'User PassportId'
      },
      photoPassport: {
        type: 'string',
        description: 'User Photo Passport'
      },
      photoVerification: {
        type: 'string',
        description: 'User Photo Verification'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/get_referrer_id': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getReferrerId',

    name: 'Get referrer id',
    group: 'USER',
    description: 'Get referrer id',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/check_referrer_id': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'checkReferrerId',

    name: 'Check referrer id',
    group: 'USER',
    description: 'Check referrer id',
    params: {
      referrer: {
        type: 'String',
        description: 'Referrer ID'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/register/confirm': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'confirmRegistration',

    name: 'Confirm Registration New User',
    group: 'AUTH',
    description: 'Register New User',
    params: {
      hash: {
        type: 'String',
        description: 'Confirm Hash'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/users/me': {
    access: ['isAuth', 'isBot'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getUserInfo',

    name: 'Get User Info',
    group: 'AUTH',
    description: 'Get User Info',
    params: {
      clientId: {
        type: 'String',
        description: 'Client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/users/me/details': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getUsersDetailInfo',


    name: 'Get User Detail',
    group: 'AUTH',
    description: 'Get User Detail',
    params: {
      clientId: {
        type: 'String',
        description: 'Client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/users/me/verify': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'verifyUser',

    name: 'Get User Verify',
    group: 'AUTH',
    description: 'Get User Verify',
    params: {
      clientId: {
        type: 'String',
        description: 'Client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/users/me/token': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getToken',

    name: 'Get User Token',
    group: 'AUTH',
    description: 'Get User Token',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/users/addresses': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getPublicKeys',

    name: 'Get Users Public Key',
    group: 'AUTH',
    description: 'Get Users Public Key',
    params: {
      currency: {
        type: 'String',
        description: 'currency'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/subscribe': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'subscribeToSymbol',

    name: 'Subscribe to Symbol',
    group: 'AUTH',
    description: 'Get subscribe',
    params: {
      symbol: {
        type: 'String',
        description: 'Symbol pair'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/unsubscribe': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'usersController',
    action: 'unsubscribeToSymbol',

    name: 'Unset Subscribe to Symbol',
    group: 'AUTH',
    description: 'Unset Subscribe',
    params: {
      symbol: {
        type: 'String',
        description: 'Symbol pair'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/balances/me': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getAccountBalance',

    name: 'Get Account Balances',
    group: 'AUTH',
    description: 'Get Account Balances',
    params: {
      clientId: {
        type: 'String',
        description: 'client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/balance/me/total': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getTotalBalance',

    name: 'Get Total Balance',
    group: 'AUTH',
    description: 'Get Total Balance',
    params: {
      clientId: {
        type: 'String',
        description: 'client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  '/v1.0/limits/me': {
    access: ['isAuth'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getDayLimits',

    name: 'Get My Limits',
    group: 'AUTH',
    description: 'Get My Limits',
    params: {
      clientId: {
        type: 'String',
        description: 'client ID'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'PUT/v1.0/users/:id/limits': {
    access: ['isSystem'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'updateDailyLimits',

    name: 'Get Users Limits',
    group: 'USER',
    description: 'Get Users Limits',
    params: {
      clientId: {
        type: 'String',
        description: 'client ID'
      },
      currency: {
        type: 'String',
        description: 'Currency'
      },
      amount: {
        type: 'String',
        description: 'Amount'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },
};
