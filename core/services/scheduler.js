const request = require('request');
const config = require(__dirroot + 'config');

const schedulerClient = {
  importPrivKey(currency, privateKey, id) {
    return this._sendRequest('crypto/import_private_key', {currency, privateKey, id});
  },

  generateKeyPair(currency, id) {
    return this._sendRequest('crypto/generate_keys', {currency, id});
  },

  createWithdrawalTransaction(withdrawal) {
    return this._sendRequest('crypto/create_withdrawal_transaction', {withdrawal});
  },

  hashPrivateKeys(data) {
    return this._sendRequest('crypto/hash_private_keys', {data});
  },

  _sendRequest(path, data) {
    return new Promise((resolve, reject) => {
      const url = `${config.connections.scheduler.protocol}://${config.connections.scheduler.host}:${config.connections.scheduler.port}/`;
      const options = {
        url: url + path,
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'X-Token': config.connections.scheduler.token
        },
        json: data
      };
      request(options, (err, res, body) => {
        if (err) {
          log.e('Error from SCHEDULER:', err);
          return reject(err);
        }
        if (res.statusCode !== 200) {
          log.e('Error from SCHEDULER:', res.statusMessage);
          return reject(body);
        }
        if (body && typeof body === 'string') {
          try {
            body = JSON.parse(body);
          } catch (err) {
            log.e('Error from SCHEDULER:', err);
            return reject(body);
          }
        }
        return resolve(body.data ? body.data : body);
      });
    });
  }
};

module.exports = schedulerClient;
