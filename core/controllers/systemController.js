const accountMngr = __app('managers/accountsManager');
const clientMngr = __app('managers/clientsManager');

const multer = require('multer');
const Mailer = __app('services/mailer');
const mailerClent = new Mailer();

const MAX_FILE_SIZE = 20 * 1024 * 1024;

const Redis = __app('services/redis');
const redis = new Redis();

const Mysql = __app('services/mysql');
const mysql = new Mysql();


const SystemCtrl = {
  getPlatformStatus(req, res) {
    if (!redis.connection.connected) {
      return res.answer({status: 500, data: 'redis is not connected'});
    }
    return mysql.checkStatus()
      .then(() => {
        return res.answer({status: 200, data: 'OK'});
      })
      .catch(() => {
        return res.answer({status: 500, data: 'mysql is not connected'});
      });
  },

  sendEmail(req, res) {
    const storage = multer.memoryStorage();
    const upload = multer({storage: storage}).single('file');
    upload(req, res, err => {
      if (err) {
        log.e('Error with file in sendEmail', err);
        return res.error('SEND_EMAIL_FILE_ERROR');
      }

      if (!req.body.text || !req.body.to) {
        return res.error('SEND_EMAIL_MISSING_PARAMS');
      }

      if (req.file && req.file.size && req.file.size > MAX_FILE_SIZE) {
        return res.error('SEND_EMAIL_TOO_LARGE_FILE');
      }

      mailerClent.sendCustomEmail(req.body.text, req.body.subject, req.body.to, req.body.from, req.file)
        .catch(err => {
          log.e('Failed sendEmail', err);
        });

      res.answer({
        status: 200,
        data: {
          code: 200,
          data: {success: true}
        }
      });
    });
  },

  createRedisAccountsFromMysql(req, res) {
    accountMngr.createRedisAccountsFromMysql()
      .then(() => {
        return res.answer({status: 200, data: {success: true}});
      })
      .catch(() => {
        return res.answer({status: 500, data: {success: false}});
      });
  },

  updateAccountsPrivateKeys(req, res) {
    accountMngr.updateAccountsPrivateKeys()
      .then(() => {
        return res.answer({status: 200, data: {success: true}});
      })
      .catch(() => {
        return res.answer({status: 500, data: {success: false}});
      });
  },

  reimportPrivateKeys(req, res) {
    clientMngr.reimportPrivateKeys()
      .then(() => {
        res.answer({status: 200, data: {success: 'You should wait a bit before finish.'}});
      })
      .catch(err => res.error(err));
  }
};

module.exports = SystemCtrl;
