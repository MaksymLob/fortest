const path = require('path');
global.__dirroot = path.dirname(path.dirname(__dirname)) + '/';
const fs = require('fs');
const _path = require(__dirroot + 'core/libs/path');

let dbm;
let type;
let seed;
let Promise;
let migrationName;

exports.setName = function ($name) {
  $name = path.parse($name).name;
  migrationName = $name;
};

exports.setup = (options, seedLink) => {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
  Promise = options.Promise;
};

exports.up = function (db) {
  const filePath = _path.locate('migrations/' + migrationName + '/up.sql');
  if (!fs.existsSync(filePath)) {
    console.warn('[WARN]', 'MIGRATION NOT FOUND', migrationName);
    return Promise.resolve();
  }
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, {encoding: 'utf-8'}, (err, data) => {
      if (err) return reject(err);
      console.log('received data: ' + data);
      resolve(data);
    });
  })
    .then(data => {
      return db.runSql(data);
    });
};

exports.down = function (db) {
  const filePath = _path.locate('migrations/' + migrationName + '/down.sql');

  if (!fs.existsSync(filePath)) {
    console.warn('[WARN]', 'MIGRATION NOT FOUND', migrationName);
    return Promise.resolve();
  }
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, {encoding: 'utf-8'}, (err, data) => {
      if (err) return reject(err);
      console.log('received data: ' + data);
      resolve(data);
    });
  })
    .then(data => {
      return db.runSql(data);
    });
};

exports._meta = {
  'version': 1
};
