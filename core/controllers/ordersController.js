
const _ = require('lodash');
const orderMngr = __app('managers/ordersManager');

const OrderCtrl = {
  openOrder(req, res) {
    req.body.clientId = req.access.clientId;
    orderMngr.openOrder(req.body)
      .then(result => {
        res.answer({status: 200, data: {success: true, id: result.id}});
      })
      .catch(err => res.error(err));
  },

  cancelOrder(req, res) {
    req.body.clientId = req.access.clientId;
    orderMngr.cancelOrder(req.body)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  getOrderBooks(req, res) {
    orderMngr.getOrderBooks(req.body)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  getCurrentOpenedOrders(req, res) {
    orderMngr.getCurrentOpenedOrders(req.body)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  getMyOpenedOrders(req, res) {
    orderMngr.getMyOpenedOrders(_.assign(req.body, req.access))
      .then(result => {
        res.answer({status: 200, data: {orders: result}});
      })
      .catch(err => res.error(err));
  },

  getMyClosedOrders(req, res) {
    orderMngr.getMyClosedOrders(_.assign(req.body, req.access))
      .then(result => {
        res.answer({status: 200, data: {orders: result}});
      })
      .catch(err => res.error(err));
  },

  getAllClosedOrders(req, res) {
    orderMngr.getAllClosedOrders(_.assign(req.body, req.access))
      .then(result => {
        res.answer({status: 200, data: {code: 200, data: result}});
      })
      .catch(err => res.error(err));
  },

  getOrdersHistory(req, res) {
    orderMngr.getOrdersHistory(req.body)
      .then(result => {
        res.answer({status: 200, data: {history: result}});
      })
      .catch(err => res.error(err));
  }
};

module.exports = OrderCtrl;
