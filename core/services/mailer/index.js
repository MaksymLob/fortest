const nodemailer = require('nodemailer');
const sparkPostTransport = require('nodemailer-sparkpost-transport');
const mustache = require('mustache');
const fs = require('fs');
const config = require(__dirroot + 'config.json');

class Mailer {
  constructor() {
    if (Mailer.__instance) {
      return Mailer.__instance;
    }

    this.transporter = this._selectTransport();

    Mailer.__instance = this;
  }

  _selectTransport() {
    switch (config.connections.mail.type) {
      case 'sparkpost':
        return nodemailer.createTransport(sparkPostTransport({
          sparkPostApiKey: config.connections.mail.sparkPostApiKey
        }));
      default:
        return nodemailer.createTransport({
          host: config.connections.mail.host,
          port: config.connections.mail.port,
          secure: true,
          auth: {
            user: config.connections.mail.user,
            pass: config.connections.mail.password
          }
        });
    }
  }

  sendConfirmationMsg(email, hash, url, template) {
    return this._renderTemplate(`${__dirname}/templates/confirmEmail.mst`, {
      URL: url ? `${url}/#/confirm_email/` : `${config.clientSide.protocol}://${config.clientSide.url}/#/confirm_email/`,
      HASH: hash
    }, template)
      .then(res => {
        return this._sendMsg({
          from: config.connections.mail.users.noreply,
          to: email,
          subject: 'Confirm your Email address',
          html: res
        });
      });
  }

  sendRecoveryPassword(email, hash) {
    return this._renderTemplate(`${__dirname}/templates/recoveryPassword.mst`, {
      URL: `${config.clientSide.protocol}://${config.clientSide.url}/#/recovery/`,
      HASH: hash
    })
      .then(res => {
        return this._sendMsg({
          from: config.connections.mail.users.noreply,
          to: email,
          subject: 'Reset password',
          html: res
        });
      });
  }

  sendNotificationOfLogging({email, loginTime, ip, userAgent}) {
    return this._renderTemplate(`${__dirname}/templates/loginNotification.mst`, {
      EMAIL: email,
      LOGIN_TIME: loginTime,
      IP_ADDRESS: ip,
      USER_AGENT: userAgent,
      SUPPORT: config.connections.mail.users.support
    })
      .then(res => {
        return this._sendMsg({
          from: config.connections.mail.users.noreply,
          to: email,
          subject: 'Terminal Login',
          html: res
        });
      });
  }

  rejectVerification({email, reason}) {
    return this._renderTemplate(`${__dirname}/templates/rejectVerification.mst`, {
      EMAIL: email,
      REASON: reason
    })
      .then(res => {
        return this._sendMsg({
          from: config.connections.mail.users.noreply,
          to: email,
          subject: 'Terminal Login',
          html: res
        });
      });
  }

  approveVerification({email}) {
    return this._renderTemplate(`${__dirname}/templates/approveVerification.mst`, {
      EMAIL: email
    })
      .then(res => {
        return this._sendMsg({
          from: config.connections.mail.users.noreply,
          to: email,
          subject: 'Terminal Login',
          html: res
        });
      });
  }

  sendCustomEmail(text, subject = `Message from ${config.companyName}`, to, from = config.connections.mail.users.noreply, file) {
    return this._renderTemplate(`${__dirname}/templates/customEmail.mst`, {
      TEXT: text
    })
      .then(res => {
        return this._sendMsg({
          from: from,
          to: to,
          subject: subject,
          html: res,
          attachments: this._addAttachment(file)
        });
      });
  }

  _addAttachment(file) {
    if (file && file.originalname && file.buffer) {
      return {
        filename: file.originalname,
        content: file.buffer
      };
    }
    return [];
  }

  _renderTemplate(path, params, template = null) {
    return new Promise((resolve, reject) => {
      if (template) {
        try {
          return resolve(mustache.render(template.toString(), params));
        } catch (err) {
          log.e('Failed render custom template', err);
        }
      }
      fs.readFile(path, (err, data) => {
        if (err) return reject(err);
        resolve(mustache.render(data.toString(), params));
      });
    });
  }

  _sendMsg(mailOptions) {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          log.e('Error with mailer', err);
          return reject('MAIL_SENDING_ERROR');
        }
        resolve(info);
      });
    });
  }
}

module.exports = Mailer;
