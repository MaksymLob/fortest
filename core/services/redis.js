

const _ = require('lodash');
const redis = require('redis');
const Redlock = require('redlock');

const config = require(__dirroot+'config');
const redisConf = config.connections.REDIS;

class Redis {
  constructor(opts) {
    if (Redis._instance) {
      return Redis._instance;
    }

    this.opts = opts || redisConf;
    this.connectData = {
      db: this.opts.database,
      host: this.opts.host,
      port: this.opts.port
    };

    Redis._instance = this;
  }

  connect() {
    return new Promise((resolve, reject) => {
      if (this.connection) {
        return resolve(true);
      }
      this.connection = redis.createClient(this.connectData);
      this.connection.on('error', err => {
        log.e('Failed connect to REDIS:', err);
        return reject(err);
      });

      this.connection.on('connect', () => {
        log.i('Connect to REDIS:', 'http://' + redisConf.host + ':' + redisConf.port, 'database: ' + redisConf.database);
        resolve(true);
      });
      this.connection.on('disconnect', () => {
        log.i('Connect to REDIS:', 'http://' + redisConf.host + ':' + redisConf.port, 'database: ' + redisConf.database);
      });
      this.redlock = new Redlock([this.connection], {
        driftFactor: 0.01, // time in ms // the expected clock drift; for more details
        retryCount:  25,  // the max number of times Redlock will attempt // to lock a resource before erroring
        retryDelay:  100, // time in ms  // the time in ms between attempts
        retryJitter:  200 // time in ms  // the max time in ms randomly added to retries // to improve performance under high contention
      });
      this.redlock.on('clientError', function(err) {
        log.e('A redis error has occurred:', err);
      });
    });
  }

  create(data, table, options) {
    let keys = this._getKeys(options);
    let autoIncrement = options[keys.primary].autoIncrement;

    if (keys.parent) {
      table = table + data[keys.parent];
    }
    return this._getNewId(table, autoIncrement, data, keys.primary)
      .then(() => {
        return this._saveHash(table, data, keys.primary);
      })
      .then(() => {
        return this._saveAdditionalLinks(keys, data, table);
      })
      .then(() => {
        return data;
      });
  }

  update(data, searchParams, table, options) {
    const key = 'currency';
    data = _.assign(data, searchParams.where);
    if (table[table.length - 1] === ':') {
      table += data.clientId;
    }
    return this._saveHash(table, data, key)
      .then(() => {
        return data;
      });
  }

  delete(data, table, options) {
    let keys = this._getKeys(options);
    return this._deleteHash(table, data, keys.primary)
      .then(() => {
        return this._deleteAdditionalLinks(keys, data, table);
      })
      .then(() => {
        return data;
      });
  }

  findById(id, table, options) {
    return new Promise((resolve, reject) => {
      let keys = this._getKeys(options);

      this.connection.hgetall(table + ':' + keys.primary + ':' + id, (err, res) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve(res);
      });
    });
  }

  findAll(data, table, options) {
    data = data.where;
    return new Promise((resolve, reject) => {
      let key = Object.keys(data);
      this.connection.smembers(table + ':' + key[0] + ':' + data[key[0]], (err, list) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        if (!list.length) {
          return resolve([]);
        }
        let result = [];
        for (let i = 0; i < list.length; i++) {
          this.findById(list[i], table, options)
            .then(res => {
              if (!key[1] || res[key[1]] == data[key[1]]) {
                result.push(res);
              }
              if (i === list.length - 1) {
                resolve(result);
              }
            });
        }
      });
    });
  }

  execTransaction(multi) {
    return new Promise((resolve, reject) => {
      multi.exec(err => {
        if (err) {
          log.e('redis error with transaction', err);
          return reject('REDIS_TRANSACTION_ERROR');
        }
        resolve(true);
      });
    });
  }

  deleteDb() {
    return new Promise((resolve, reject) => {
      this.connection.flushdb(err => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve(true);
      });
    });
  }

  saveForSearchingByParam(paramName, param, data, table, options) {
    return this._saveList(table + ':' + paramName + ':' + param, data);
  }

  destruct() {
    return new Promise((resolve, reject) => {
      this.connection.quit((err, res) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        delete Redis._instance;
        resolve(true);
      });
    });
  }

  // ****************************  PRIVATE METHODS  ***************************

  _getNewId(table, autoIncrement, data, key) {
    return new Promise((resolve, reject) => {
      if (data[key]) {
        return resolve(data);
      }
      this.connection.incr(table + ':count', (err, count) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        if (autoIncrement) {
          data[key] = count;
        }
        resolve(data);
      });
    });
  }

  _getKeys(options) {
    let keys = {};
    for (let opt in options) {
      if (options[opt].primaryKey) {
        keys.primary = opt;
        if (options[opt].additionalKey) keys.additional = options[opt].additionalKey;
        if (options[opt].parentKey) keys.parent = options[opt].parentKey;
        break;
      }
    }
    return keys;
  }

  _saveHash(table, data, key) {
    return new Promise((resolve, reject) => {
      for (let prop in data) {
        if (data.hasOwnProperty(prop) && typeof data[prop] === 'undefined') {
          data[prop] = '';
        }
      }
      this.connection.hmset(table + ':' + key + ':' + data[key], data, err => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve();
      });
    });
  }

  _deleteHash(table, data, key) {
    return new Promise((resolve, reject) => {
      this.connection.del(table + ':' + key + ':' + data[key], err => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve();
      });
    });
  }

  _deleteFromList(table, data, key) {
    return new Promise((resolve, reject) => {
      this.connection.lrem(table, -1, data, err => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve();
      });
    });
  }

  _deleteFromSet(table, data, key) {
    return new Promise((resolve, reject) => {
      this.connection.srem(table, data, (err, res) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        resolve();
      });
    });
  }

  _saveList(table, data) {
    return new Promise((resolve, reject) => {
      this.connection.rpush(table, data, (err, res) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        return resolve();
      });
    });
  }

  _saveSet(table, data) {
    return new Promise((resolve, reject) => {
      this.connection.sadd(table, data, (err, res) => {
        if (err) {
          log.e('INTERNAL_REDIS_ERROR', err);
          return reject('INTERNAL_REDIS_ERROR');
        }
        return resolve();
      });
    });
  }

  _saveAdditionalLinks(keys, data, table) {
    if (keys.additional) {
      for (let key in keys.additional) {
        if (keys.additional[key] === 'list') {
          this._saveList(table + ':' + key + ':' + data[key], data[keys.primary]);
        }
        if (keys.additional[key] === 'set') {
          this._saveSet(table + ':' + key + ':' + data[key], data[keys.primary]);
        }
      }
      return Promise.resolve();
    } else {
      return Promise.resolve(data);
    }
  }

  _deleteAdditionalLinks(keys, data, table) {
    if (keys.additional) {
      for (let key in keys.additional) {
        if (keys.additional[key] === 'list') {
          this._deleteFromList(table + ':' + key + ':' + data[key], data[keys.primary]);
        }
        if (keys.additional[key] === 'set') {
          this._deleteFromSet(table + ':' + key + ':' + data[key], data[keys.primary]);
        }
      }
      return Promise.resolve();
    } else {
      return Promise.resolve();
    }
  }
}

module.exports = Redis;
