

module.exports = {
  /* **************************  Common controller  *************************/

  '/v1.0/common-resources': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'resourcesController',
    action: 'getCommonResources',

    name: 'Get common resources',
    group: 'Resources',
    description: 'Returns info about countries, currency and currency pair reductions.',
    params: {
      extID: {
        type: 'String',
        description: 'unique id of request to identify response'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      },
      'data.extID': {
        type: 'String',
      },
      'data.countries': {
        type: 'Array',
      },
      'data.currencies': {
        type: 'Object',
      },
      'data.symbols': {
        type: 'Object',
      },
    },
    apiuse: ['defErrorResp']
  },

  'GET/v1.0/docs': {
    access: ['isAll'],
    tokenRequired: false,
    controllerName: 'resourcesController',
    action: 'getDocs',

    name: 'Get index html of apiDoc',
    group: 'Resources',
    description: 'Get index html of apidoc' // TODO
  }
};
