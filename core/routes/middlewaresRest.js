

const jwt = require('jwt-simple');
const config = require(__dirroot+'config');

const Middlewares = __app('routes/middlewares');
const routes = __app('routes/routesList');

class Rest extends Middlewares {
  constructor() {
    super();
  }

  setCommonMiddlewares(req, res, next) {
    this._setHeaders(res);
    if (req.method === 'OPTIONS') {
      return res.status(200).send();
    }
    this._setRequestFormat(req, res);
    this._setResponseFormat(req, res);
    next();
  }

  checkToken(req, res, next) {
    let token = req.header('X-Token');
    super.checkToken(token, req, res, next);
  }

  // ****************************  PRIVATE METHODS  ***************************

  _setHeaders(res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, X-Token');
    res.setHeader('Access-Control-Expose-Headers', 'X-Token');
    res.setHeader('Access-Control-Max-Age', config.maxAge);
  }

  _getParams(template, str) {
    let params = {};
    template = template.replace(/:/g, '').split('/');
    str = str.split('/');
    for (let i = 0; i < template.length; i++) {
      if (str[i] && str[i] !== template[i]) {
        params[template[i]] = str[i];
      }
    }
    return params;
  }

  _isMatchWithTemplate(template, str) {
    let regexpParams = /:[^\/]+/g;
    template = '^' + template.replace(regexpParams, '[^\/]+') + '$';
    let regexpUrl = new RegExp(template);

    return regexpUrl.test(str);
  }

  _getCompleteMethodName(method, path) {
    let fullName = method + path;
    for (let url in routes) {
      if (this._isMatchWithTemplate(url, fullName)) {
        return url;
      }
    }
    return '';
  }

  _setRequestFormat(req) {
    req.methodFullName = this._getCompleteMethodName(req.method, req.path);
    req.paramsUrl = this._getParams(req.methodFullName, req.path);
    let reqForLog = super._prepareRequestForLogging(req);
    log.writeRequest(req.methodFullName, reqForLog);
  }

  _setResponseFormat(req, res) {
    res.answer = ({status: status, data: data}) => {
      if (req.access && req.access.needUpdateToken) {
        res.setHeader('X-Token', jwt.encode({
          clientId: req.access.clientId,
          nickname: req.access.nickname,
          role: req.access.role,
          expiry_time: Date.now() + config.maxAge * 1000
        }, config.secret_salt_api_tokens));
      }
      res.status(status).send(data);

      let resForLog = super._prepareResponseForLogging(req.methodFullName, data);
      log.writeResponse(req.methodFullName, resForLog);
    };
    super.setErrorResponseFormat(res);
  }
}

module.exports = Rest;
