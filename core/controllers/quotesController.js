

const config = require(__dirroot+'config');
const quotesMngr = __app('managers/quotesManager');
const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();

config.chart.config.supportedResolutions = Object.keys(config.chart.resolutions); //TODO: temp fix
config.chart.settings.supported_resolutions = config.chart.config.supportedResolutions; //TODO: temp fix

let QuotesCtrl = {
  getCurrentQuotes(req, res) {
    let result = [];
    for (let symbol of config.symbols) {
      result.push(quotes.getCurrentQuote({symbol: symbol}));
    }
    res.answer({status: 200, data: {quotes: result}});
  },

  getConfigForChart(req, res) {
    res.answer({status: 200, data: JSON.stringify(config.chart.config)});
  },

  getQuotesHistory(req, res) {
    quotesMngr.getQuotesHistory(req.query)
      .then(result => res.answer({status: 200, data: JSON.stringify(result)}))
      .catch(err => res.error(err));
  },

  getAllSymbols(req, res) {
    let result = [];
    for (let symbol of config.symbols) {
      result.push({symbol: symbol});
    }
    res.answer({status: 200, data: JSON.stringify(result)});
  },

  getSymbolDetail(req, res) {
    if (!req.query.symbol) req.query.symbol = 'BTCUSD';
    let result = config.chart.settings;
    result.name = req.query.symbol;
    result.ticker = req.query.symbol;
    res.answer({status: 200, data: JSON.stringify(result)});
  }
};

module.exports = QuotesCtrl;
