

const OrderModel = {
  classMethods: {
    checkExisting(data) {
      return this.findAll({where: {email: data.email}})
        .then(user => {
          if (user.length) {
            return Promise.reject('VALIDATION_EMAIL_ALREADY_EXIST');
          }
          return Promise.resolve(data);
        });
    }
  }
};

const orderSchema = __app('models/schemas/order');
const Model = __app('models/model');
const schema = new Model(orderSchema.schema);
OrderModel.validations = orderSchema.validations;
const orderModel = schema.defineDataBase('mysql-redis', 'orders', OrderModel);

module.exports = orderModel;
