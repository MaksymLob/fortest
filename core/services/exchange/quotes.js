
let CronJob = require('cron').CronJob;
const config = require(__dirroot+'config');
const GE = __app('libs/GE');

let Redis = __app('services/redis');
Redis = new Redis();
const redis = Redis.connection;
// [time, priceOpen, priceClose, lowest, highest, baseVolume, quoteVolume, percentChange]

class Quotes {

  constructor(opts) {
    if (Quotes._instance) {
      return Quotes._instance;
    }

    this.opts = opts || {};
    this.quotes = {};

    this.init();
    Quotes._instance = this;
  }

  init() {
    for (let i = 0; i < config.symbols.length; i++) {
      let symbol = config.symbols[i];

      this.quotes[symbol] = {};
      for (let frame in config.frames) {
        this.quotes[symbol][frame] = [0, 0, 0, 0, 0, 0, 0, 0];
      }
      redis.hgetall('quotes:' + symbol, (err, res) => {
        if (!err && res) {
          let time = Math.round(Date.now() / 1000) * 1000;
          for (let frame in res) {
            let candle = JSON.parse(res[frame]);
            let delta = Math.round((time - Number(candle[0])) / 1000);
            if (delta < frame || frame == 1) {
              this.quotes[symbol][frame] = candle;
            } else {
              redis.zadd('quotes:history:' + symbol + ':' + frame, candle[0], '' + candle);
              this.quotes[config.symbols[i]][frame] = [ // начинаем новую свечу
                Number(candle[0]) + Math.round(delta / frame) * frame * 1000,  // time
                candle[2], // priceOpen
                candle[2], // priceClose
                candle[2], // lowest
                candle[2], // highest
                0, // baseVolume (second place in symbol)
                0, // quoteVolume (first place in symbol)
                0 // percentChange
              ];
            }
          }
        }
      });

      new CronJob('* * * * * *', () => {  // every day at 6am and 6pm
        this._saveCandle(config.symbols[i]);
      }, null, true, 'UTC');
    }
  }

  completeCandle(order) {
    let time = Math.floor(Date.now() / 1000);
    let symbol = order.symbol;

    for (let frame in config.frames) {
      if (this.quotes[symbol][frame][0]/* && time - this.quotes[symbol][frame][0] < config.frames[frame]*/) {
        this.quotes[symbol][frame] = [
          this.quotes[symbol][frame][0], // time
          this.quotes[symbol][frame][1], // priceOpen
          order.price, // priceClose
          this.quotes[symbol][frame][3] > order.price ? order.price : this.quotes[symbol][frame][3], // lowest
          this.quotes[symbol][frame][4] < order.price ? order.price : this.quotes[symbol][frame][4], // highest
          this.quotes[symbol][frame][5] + order.volume, // baseVolume (second place in symbol)
          this.quotes[symbol][frame][6] + order.amount, // quoteVolume (first place in symbol)
          ((order.price - this.quotes[symbol][frame][1]) / this.quotes[symbol][frame][1] * 100).toFixed(2) // percentChange
        ];
      } else {  // началась новая свеча
        this.quotes[symbol][frame] = [ // начинаем новую свечу
          time * 1000, // time
          order.price, // priceOpen
          order.price, // priceClose
          order.price, // lowest
          order.price, // highest
          order.volume, // baseVolume (second place in symbol)
          order.amount, // quoteVolume (first place in symbol)
          0 // percentChange
        ];
      }
      redis.hset('quotes:' + symbol, frame, JSON.stringify(this.quotes[symbol][frame]));
    }
    GE.emit('/quotes/update', {
      symbol: symbol,
      currentPrice: order.price,
      low: this.quotes[symbol][86400][3],
      high: this.quotes[symbol][86400][4],
      baseVolume: this.quotes[symbol][86400][5],
      quoteVolume: this.quotes[symbol][86400][6],
      percentChange: this.quotes[symbol][86400][7]
    });
  }

  getCurrentQuote({symbol: symbol}) {
    return {
      symbol: symbol,
      currentPrice: this.quotes[symbol][1][1] || 0,
      low: this.quotes[symbol][86400][3] || 0,
      high: this.quotes[symbol][86400][4] || 0,
      baseVolume: this.quotes[symbol][86400][5] || 0,
      quoteVolume: this.quotes[symbol][86400][6] || 0,
      percentChange: this.quotes[symbol][86400][7] || 0
    };
  }

  getHistory({symbol, frame, from, to}) {
    return new Promise((resolve, reject) => {
      redis.zrangebyscore('quotes:history:' + symbol + ':' + frame, '-inf', 'inf', (err, res) => {
        if (err) {
          log.i('no data. err: ' + err + ';  ret: ' + res);
          return reject('ERROR');
        }
        if (this.quotes[symbol][frame] && this.quotes[symbol][frame].join(',')[0] != 0) {
          res.push(this.quotes[symbol][frame].join(','));
        }
        return resolve(res);
      });
    });
  }

  _saveCandle(symbol) {
    let time = Math.floor(Date.now() / 1000);

    for (let frame in config.frames) {
      if (!(time % frame)) {
        if (this.quotes[symbol][frame][0]) {
          let stamp = this.quotes[symbol][frame][0];
          let candle = '' + this.quotes[symbol][frame].slice(0, 8);

          if (frame != 1) {
            redis.zadd('quotes:history:' + symbol + ':' + frame, stamp, candle);
          }

          this.quotes[symbol][frame] = [ // начинаем новую свечу
            this.quotes[symbol][frame][0] + frame * 1000,  // time
            this.quotes[symbol][frame][2], // priceOpen
            this.quotes[symbol][frame][2], // priceClose
            this.quotes[symbol][frame][2], // lowest
            this.quotes[symbol][frame][2], // highest
            0, // baseVolume (second place in symbol)
            0, // quoteVolume (first place in symbol)
            0 // percentChange
          ];
        }
      }
    }
  }

}

module.exports = Quotes;
