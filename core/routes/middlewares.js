

const _ = require('lodash');
const jwt = require('jwt-simple');
const config = require(__dirroot + 'config');
const constants = __app('libs/constants');
const AppError = __app('libs/errors');

const routes = __app('routes/routesList');
const controllers = { // TODO: scan controllers directory
  usersController: __app('controllers/usersController'),
  ordersController: __app('controllers/ordersController'),
  transactionsController: __app('controllers/transactionsController'),
  quotesController: __app('controllers/quotesController'),
  resourcesController: __app('controllers/resourcesController'),
  systemController: __app('controllers/systemController')
};

class Middlewares {
  constructor() {}

  existMethod(req, res, next) {
    if (!req.methodFullName || !routes[req.methodFullName]) {
      return res.error('NOT_FOUND_METHOD');
    }
    next(null);
  }

  checkToken(token, req, res, next) {
    this._parseToken(token, req);
    if (req.access.error) {
      let response = this._prepareResponseError(req.access.error);
      return res.answer(response);
    }

    if (!routes[req.methodFullName].tokenRequired) {
      req.access.needUpdateToken = true;
      return next();
    }

    let expired = this._isTokenTimeExpired(req.access);
    if (expired) {
      req.access.needUpdateToken = true;
    }
    next();
  }

  checkPermissions(req, res, next) {
    let accessList = {
      isNoAuth: constants.ROLE_NOT_AUTH,
      isAuth: constants.ROLE_IS_AUTH,
      isAdmin: constants.ROLE_IS_ADMIN,
      isCrm: constants.ROLE_IS_CRM,
      isPartAuth: constants.ROLE_IS_PART_AUTH,
      isSystem: constants.ROLE_IS_SYSTEM,
      isBot: constants.ROLE_IS_BOT
    };
    const roles = new Set();

    if (!routes[req.methodFullName].access) {
      return next(null);
    }

    for (let i = 0; i < routes[req.methodFullName].access.length; i++) {
      if (routes[req.methodFullName].access[i] === 'isAll') {
        for (let name in accessList)
          roles.add(accessList[name]);
        continue;
      }
      roles.add(accessList[routes[req.methodFullName].access[i]]);
    }

    if (roles.has(req.access.role)) {
      return next(null);
    } else {
      log.d('access', req.access);
      return res.error('FORBIDDEN_NO_ACCESS_RIGHTS');
    }
  }

  callController(req, res, next) {
    req.params = req.paramsUrl;
    let ctrl = controllers[routes[req.methodFullName].controllerName];
    ctrl[routes[req.methodFullName].action](req, res, next);
  }

  setErrorResponseFormat(res) {
    res.error = err => {
      let error = this._errorHandler(err);
      let response = this._prepareResponseError(error);
      res.answer(response);
    };
  }

  // ****************************  PRIVATE METHODS  ***************************

  _setDefaultAccess(req) {
    req.access = {};
    for (let key in config.access) {
      // if (config.access.hasOwnProperty(key)) {
      //   req.access[key] = config.access[key];
      // }
      req.access[key] = config.access[key];
    }
    return req.access;
  }

  _parseToken(token, req) {
    if (!token) return this._setDefaultAccess(req);
    try {
      req.access = jwt.decode(token, config.secret_salt_api_tokens);
    } catch (err) {
      req.access = {error: new AppError('FORBIDDEN_CANNOT_PARSE_TOKEN')};
    }
    return req.access;
  }

  _isTokenTimeExpired(token) {
    if (!token.expiry_time) return true;
    return Date.now() >= token.expiry_time;
  }

  _prepareResponseError(data) {
    return {
      status: data.status ? data.status : 500,
      data: {
        code: data.errorCode ? data.errorCode : 0,
        message: data.message ? data.message : data
      }
    };
  }

  _errorHandler(err) {
    if (typeof err === 'string' && err.indexOf(' ') == -1) {
      return new AppError(err);
    }
    log.e('UNKNOWN_ERROR', err);
    return new AppError('UNKNOWN_ERROR', err);
  }

  _prepareRequestForLogging(req) {
    let data = (req.access && req.access.email) ? {email: req.access.email} : {};
    data = _.assign(data, req.body);
    data = _.assign(data, req.params);
    data = _.assign(data, req.query);

    delete data.password;
    delete data.tempPassword;
    delete data.token;
    delete data['X-Token'];
    delete data.userId;

    return data;
  }

  _prepareResponseForLogging(method, data) {
    if (method == 'GET/quotes_history') {
      return {
        length: data.length ? data.length : 0,
        lastValue: data.length ? data[data.length - 1] : {}
      };
    }
    if (method == 'GET/assets' && data.assets && data.leverages) {
      let data2 = _.cloneDeep(data);
      for (let asset in data2.assets) {
        delete data2.assets[asset].assetDescription;
        delete data2.assets[asset].assetIconImagePath;
        delete data2.assets[asset].assetId;
        delete data2.assets[asset].assetName;
        delete data2.assets[asset].assetPromotionalImagePath;
        delete data2.assets[asset].assetType;
        delete data2.assets[asset].assetDescription;
        delete data2.assets[asset].assetDescription;
      }
      return data2;
    }
    for (let prop in data) {
      if (prop == 'password' || prop == 'tempPassword' || prop == 'token' || prop == 'X-Token' || prop == 'userId') {
        delete data[prop];
      }
    }
    return data;
  }
}

module.exports = Middlewares;
