
const config = require(__dirroot+'config.json');
const currencies = Object.keys(config.currencies);

const account = {
  currency: {
    type: 'string',
    allowNull: false,
    primaryKey: true,
    validate: {isIn: [currencies]},
    defaultValue: 'BTC',
    parentKey: 'clientId'
  },
  clientId: {
    type: 'integer',
    allowNull: false
  },
  balance: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  },
  used: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  }
};

module.exports = {
  schema: account
};
