

const symbol = {
  name: {
    type: 'string',
    primaryKey: true,
    unique: true,
    allowNull: false
  },
  ticker: {
    type: 'string',
    allowNull: false
  },
  supported_resolutions: {
    type: 'string',
    defaultValue: `['5', '15', '30', '120', '240', '1D']`
  },
  pricescale: {
    type: 'integer',
    defaultValue: 100
  },
  pointvalue: {
    type: 'integer',
    defaultValue: 1
  },
  minmov2: {
    type: 'integer',
    defaultValue: 0
  },
  minmov: {
    type: 'integer',
    defaultValue: 1
  },
  description: {
    type: 'string',
    defaultValue: ''
  },
  has_intraday: {
    type: 'boolean',
    defaultValue: true
  },
  has_no_volume: {
    type: 'boolean',
    defaultValue: false
  }
};

module.exports = {
  schema: symbol
};
