
module.exports = {
  VALIDATION_AMOUNT_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1000,
    errorMessage: 'amount is not filled'
  },
  VALIDATION_TOKEN_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1001,
    errorMessage: 'token is not filled'
  },
  VALIDATION_ADDRESS_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1002,
    errorMessage: 'address is not filled'
  },
  VALIDATION_EMAIL_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1003,
    errorMessage: 'email is not filled'
  },
  VALIDATION_CLIENTID_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1004,
    errorMessage: 'clientId is not filled'
  },
  VALIDATION_SYMBOL_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1005,
    errorMessage: 'symbol is not filled'
  },
  VALIDATION_PASSWORD_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1008,
    errorMessage: 'password is not filled'
  },
  VALIDATION_ACCOUNTID_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1009,
    errorMessage: 'accountId is not filled'
  },
  VALIDATION_PRICE_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1010,
    errorMessage: 'price is not filled'
  },
  VALIDATION_TYPE_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1011,
    errorMessage: 'type is not filled'
  },
  VALIDATION_ORDERID_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1012,
    errorMessage: 'id of order is not filled'
  },
  VALIDATION_CURRENCY_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1013,
    errorMessage: 'currency is not filled'
  },
  VALIDATION_PHONE_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1014,
    errorMessage: 'phone is not filled'
  },
  VALIDATION_COUNTRY_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1015,
    errorMessage: 'country is not filled'
  },
  VALIDATION_FIRSTNAME_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1016,
    errorMessage: 'firstname is not filled'
  },
  VALIDATION_LASTNAME_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1017,
    errorMessage: 'lastname is not filled'
  },
  VALIDATION_CITY_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1018,
    errorMessage: 'city is not filled'
  },
  VALIDATION_STATE_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1019,
    errorMessage: 'state is not filled'
  },
  VALIDATION_POSTALCODE_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1020,
    errorMessage: 'postalCode is not filled'
  },
  VALIDATION_BIRTHDAY_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1021,
    errorMessage: 'birthday is not filled'
  },
  VALIDATION_PASSPORTID_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1022,
    errorMessage: 'passportId is not filled'
  },
  VALIDATION_PHOTOPASSPORT_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1023,
    errorMessage: 'photoPassport is not filled'
  },
  VALIDATION_PHOTOVERIFICATION_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1024,
    errorMessage: 'photoVerification is not filled'
  },
  VALIDATION_HASH_IS_NOT_FILLED: {
    status: 400,
    errorCode: 1025,
    errorMessage: 'hash is not filled'
  },
  VALIDATION_INVALID_REFERRER: {
    status: 403,
    errorCode: 1026,
    errorMessage: 'Invalid referrer id.'
  },
  VALIDATION_PHONE_HAS_INCORRECT_TYPE: {
    status: 400,
    errorCode: 3000,
    errorMessage: 'phone has incorrect type'
  },
  VALIDATION_BIRTHDAY_HAS_INVALID_VALUE: {
    status: 400,
    errorCode: 3001,
    errorMessage: 'birthday has invalid value'
  },
  VALIDATION_COUNTRY_HAS_INVALID_VALUE: {
    status: 400,
    errorCode: 3005,
    errorMessage: 'country has invalid value'
  },
  VALIDATION_PRICE_HAS_INVALID_VALUE: {
    status: 400,
    errorCode: 3006,
    errorMessage: 'price has invalid value'
  },
  VALIDATION_PHOTOPASSPORT_HAS_INCORRECT_TYPE: {
    status: 400,
    errorCode: 3002,
    errorMessage: 'photoPassport should be jpg, png or bmp'
  },
  VALIDATION_PHOTOVERIFICATION_HAS_INCORRECT_TYPE: {
    status: 400,
    errorCode: 3003,
    errorMessage: 'photoVerification should be jpg, png or bmp'
  },
  VALIDATION_TOO_BIG_SIZE: {
    status: 400,
    errorCode: 3004,
    errorMessage: 'max size of images is 2Mb'
  },
  VALIDATION_NOT_ENOUGH_TIME_PASSED: {
    status: 400,
    errorCode: 1100,
    errorMessage: 'not enough time has passed for new faucet'
  },
  VALIDATION_WITHDRAWAL_IS_NOT_PENDING_OR_APPROVED: {
    status: 400,
    errorCode: 1101,
    errorMessage: 'wanted withdrawal must be pending or approved'
  },
  VALIDATION_WITHDRAWAL_IS_NOT_PENDING: {
    status: 400,
    errorCode: 1101,
    errorMessage: 'wanted withdrawal must be pending'
  },
  VALIDATION_WITHDRAWAL_STATUS_IS_NOT_PROCESSED: {
    status: 400,
    errorCode: 1102,
    errorMessage: 'withdrawal status must be processed'
  },
  VALIDATION_WITHDRAWAL_STATUS_IS_NOT_APPROVED_OR_DECLINED: {
    status: 400,
    errorCode: 1103,
    errorMessage: 'withdrawal status must be approved or declined'
  },
  VALIDATION_WITHDRAWAL_AMOUNT_IS_TOO_SMALL: {
    status: 422,
    errorCode: 1104,
    errorMessage: 'withdrawal amount is too small'
  },
  VALIDATION_INSUFFICIENT_FUNDS: {
    status: 422,
    errorCode: 1105,
    errorMessage: 'Insufficient funds'
  },
  VALIDATION_WRONG_PASSWORD: {
    status: 403,
    errorCode: 1106,
    errorMessage: 'wrong password'
  },
  VALIDATION_WRONG_2FA_CODE: {
    status: 403,
    errorCode: 1107,
    errorMessage: 'wrong second factor auth code'
  },
  VALIDATION_2FA_CODE_IS_NOT_FILLED: {
    status: 403,
    errorCode: 1108,
    errorMessage: 'second factor auth code is not filled'
  },
  VALIDATION_RECAPTCHA_ERROR: {
    status: 400,
    errorCode: 1109,
    errorMessage: 'reCAPTCHA validation failed.'
  },
  VALIDATION_WITHDRAWAL_EXCEEDS_LIMIT: {
    status: 400,
    errorCode: 1110,
    errorMessage: 'Withdrawal amount exceeds user\'s day limit.'
  },
  VALIDATION_TYPE_HAS_INVALID_VALUE: {
    status: 400,
    errorCode: 1200,
    errorMessage: 'type should be more than 1'
  },
  VALIDATION_SYMBOL_HAS_INVALID_VALUE: {
    status: 400,
    errorCode: 1201,
    errorMessage: 'symbol should be BTCUSD, LTCUSD or LTCBTC'
  },
  VALIDATION_EMAIL_ALREADY_EXIST: {
    status: 400,
    errorCode: 1300,
    errorMessage: 'this email already exist'
  },
  VALIDATION_PHONE_ALREADY_EXIST: {
    status: 400,
    errorCode: 1301,
    errorMessage: 'this phone already exist'
  },
  VALIDATION_CRM_NO_ID: {
    status: 400,
    errorCode: 1400,
    errorMessage: 'Parameter id is required'
  },
  VALIDATION_CRM_NO_ACCOUNTID: {
    status: 400,
    errorCode: 1401,
    errorMessage: 'Parameter accountId is required'
  },
  VALIDATION_CRM_NO_STATUS: {
    status: 400,
    errorCode: 1402,
    errorMessage: 'Parameter status is required'
  },
  VALIDATION_CRM_NO_AMOUNT: {
    status: 400,
    errorCode: 1403,
    errorMessage: 'Parameter amount is required'
  },
  VALIDATION_CRM_NO_PAYMENT_SYSTEM: {
    status: 400,
    errorCode: 1404,
    errorMessage: 'Parameter payment_system is required'
  },
  VALIDATION_CRM_NO_LOGIN: {
    status: 400,
    errorCode: 1405,
    errorMessage: 'Parameter login is required'
  },
  VALIDATION_CRM_NO_EMAIL: {
    status: 400,
    errorCode: 1406,
    errorMessage: 'Parameter email is required'
  },
  VALIDATION_CRM_NO_PHONE: {
    status: 400,
    errorCode: 1407,
    errorMessage: 'Parameter phone is required'
  },
  INSUFFICIENT_FUNDS_IN_SYSTEM_WALLET: {
    status: 422,
    errorCode: 1500,
    errorMessage: 'Insufficient funds in system wallet'
  },
  BAD_SYSTEM_WALLET_BALANCE: {
    status: 422,
    errorCode: 1501,
    errorMessage: 'Can\'t parse number from system wallet balance'
  },
  VALIDATION_ORDER_BAD_PARAMETER: {
    status: 400,
    errorCode: 1600,
    errorMessage: 'Bad order parameter.'
  },
  VALIDATION_ORDER_BAD_OFFSET_PARAMETER: {
    status: 400,
    errorCode: 1601,
    errorMessage: 'Bad offset parameter.'
  },
  VALIDATION_ORDER_BAD_DATE_PARAMETER: {
    status: 400,
    errorCode: 1602,
    errorMessage: 'Bad date parameter (must be datetime).'
  },
  VALIDATION_ORDER_BAD_AMOUNT_PARAMETER: {
    status: 400,
    errorCode: 1603,
    errorMessage: 'Bad amount parameter'
  },
  VALIDATION_ORDER_BAD_PRICE_PARAMETER: {
    status: 400,
    errorCode: 1604,
    errorMessage: 'Bad price parameter'
  },
  VALIDATION_BAD_AMOUNT: {
    status: 400,
    errorCode: 1605,
    errorMessage: 'Bad amount'
  },
  USER_ALREADY_HAS_2FA: {
    status: 400,
    errorCode: 1700,
    errorMessage: '2FA is already activated on this account.'
  },
  USER_HAS_NO_2FA: {
    status: 400,
    errorCode: 1701,
    errorMessage: '2FA is deactivated on this account.'
  },
  USER_IS_NOT_ACTIVATED: {
    status: 403,
    errorCode: 1702,
    errorMessage: 'User is not activate. Please, confirm your email.'
  },
  USER_ALREADY_ACTIVATED: {
    status: 400,
    errorCode: 1703,
    errorMessage: 'User already activated.'
  },
  CONFIRMATION_TIME_IS_UP: {
    status: 400,
    errorCode: 1800,
    errorMessage: 'Confirmation time is up.'
  },
  INVALID_HASH: {
    status: 400,
    errorCode: 1900,
    errorMessage: 'Invalid hash.'
  },
  SOCIAL_NET_ERROR: {
    status: 500,
    errorCode: 2000,
    errorMessage: 'social net error'
  },
  SOCIAL_NET_INVALID_TOKEN: {
    status: 500,
    errorCode: 2001,
    errorMessage: 'invalid token'
  },
  SOCIAL_NET_TOKEN_EXPIRED: {
    status: 500,
    errorCode: 2002,
    errorMessage: 'token expired'
  },
  FORBIDDEN_CANNOT_PARSE_TOKEN: {
    status: 403,
    errorCode: 3002,
    errorMessage: 'Can\'t parse token'
  },
  FORBIDDEN_NO_ACCESS_RIGHTS: {
    status: 403,
    errorCode: 3003,
    errorMessage: 'No access rights'
  },
  FORBIDDEN_XTOKEN_EXPIRED: {
    status: 403,
    errorCode: 3004,
    errorMessage: 'X-Token is expired'
  },
  FORBIDDEN_NO_AUTH: {
    status: 401,
    errorCode: 3005,
    errorMessage: 'You are not authorized'
  },
  NOT_FOUND_METHOD: {
    status: 404,
    errorCode: 4000,
    errorMessage: 'Method is not found'
  },
  NOT_FOUND_CLIENT: {
    status: 404,
    errorCode: 4001,
    errorMessage: 'Client is not found'
  },
  NOT_FOUND_ORDER: {
    status: 404,
    errorCode: 4002,
    errorMessage: 'Order is not found'
  },
  NOT_FOUND_ACCOUNT: {
    status: 404,
    errorCode: 4003,
    errorMessage: 'Account is not found'
  },
  NOT_FOUND_ACCOUNTS: {
    status: 404,
    errorCode: 4003,
    errorMessage: 'Accounts are not found for this client'
  },
  NOT_FOUND_WITHDRAWAL: {
    status: 404,
    errorCode: 4004,
    errorMessage: 'Withdrawal is not found'
  },
  SEND_EMAIL_FILE_ERROR: {
    status: 500,
    errorCode: 5000,
    errorMessage: 'Error with file in send Email.'
  },
  SEND_EMAIL_FAILED: {
    status: 500,
    errorCode: 5001,
    errorMessage: 'Failed send Email.'
  },
  SEND_EMAIL_MISSING_PARAMS: {
    status: 500,
    errorCode: 5002,
    errorMessage: 'Request MUST contains parameters `text`, `to`.'
  },
  SEND_EMAIL_TOO_LARGE_FILE: {
    status: 500,
    errorCode: 5003,
    errorMessage: 'File size is too large.'
  },
  REDIS_TRANSACTION_ERROR: {
    status: 500,
    errorCode: 5100,
    errorMessage: 'redis transaction error'
  },
  REDIS_INTERNAL_ERROR: {
    status: 500,
    errorCode: 5101,
    errorMessage: 'internal redis error'
  },
  S3_UPLOADING_ERROR: {
    status: 500,
    errorCode: 5102,
    errorMessage: 'error with s3 uploading'
  },
  S3_DELETING_ERROR: {
    status: 500,
    errorCode: 5103,
    errorMessage: 'error with s3 deleting'
  },
  MAIL_SENDING_ERROR: {
    status: 500,
    errorCode: 5120,
    errorMessage: 'error with mail sending'
  },
  UNKNOWN_ERROR: {
    status: 500,
    errorCode: 9999,
    errorMessage: 'Unknown error'
  },
  USER_ALREADY_VERIFIED: {
    status: 400,
    errorCode: 7020,
    errorMessage: 'user already verified'
  },
  WAITING_VERIFICATION_CONFIRMATION: {
    status: 400,
    errorCode: 7021,
    errorMessage: 'request already is sent. Wait please confirmation'
  }
};
