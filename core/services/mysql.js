

const Sequelize = require('sequelize');
const config = require(__dirroot+'config');
const mysql = config.connections.MYSQL;

class MySqlConnection {
  constructor() {
    if (MySqlConnection._instance) {
      return MySqlConnection._instance;
    }

    this.connection = new Sequelize(mysql.database, mysql.user, mysql.password, {
      host: mysql.host,
      dialect: 'mysql',
      logging: false
    });

    MySqlConnection._instance = this;
  }

  getConnection() {
    return this.connection;
  }

  connect() {
    return this.connection.authenticate()
      .then(() => {
        return this.connection.sync();
      })
      .then(() => {
        log.i('Connect to MYSQL:', 'http://' + mysql.host);
      })
      .catch(err => {
        log.e('Failed connect to MYSQL:', err);
        // this._reconnect();
        throw err;
      });
  }

  checkStatus() {
    return this.connection.authenticate()
      .then(() => {
        return true;
      });
  }

  _reconnect() {
    setTimeout(() => {
      this.connect();
    }, config.reconnectTimeout);
  }
}

module.exports = MySqlConnection;
