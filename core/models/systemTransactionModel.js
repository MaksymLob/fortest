

const SystemTransactionModel = {};

const systemTransactionSchema = __app('models/schemas/systemTransaction');
const Model = __app('models/model');
const schema = new Model(systemTransactionSchema.schema);
const systemTransactionModel = schema.defineDataBase('mysql', 'systemTransaction', SystemTransactionModel);

module.exports = systemTransactionModel;
