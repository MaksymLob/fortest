
const _ = require('lodash');
const path = require('path');
const config = require(__dirroot+'config');
const constants = __app('libs/constants');
const clientMngr = __app('managers/clientsManager');
const accountMngr = __app('managers/accountsManager');

const UserCtrl = {

  addNewUser(req, res) {
    clientMngr.addNewReferral(req.body)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  confirmRegistration(req, res) {
    clientMngr.confirmRegistration(req.query)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  addNewUserFromCrm(req, res) {
    clientMngr.addNewUserFromCrm(req.body)
      .then(result => res.answer({status: 200, data: {data: result, code: 200}}))
      .catch(err => res.error(err));
  },

  updateUserFromCrm(req, res) {
    clientMngr.updateUserFromCrm(req.body)
      .then(result => res.answer({status: 200, data: {data: result, code: 200}}))
      .catch(err => res.error(err));
  },

  createSystemAccounts(req, res) {
    clientMngr.createSystemAccounts(req.body)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  login(req, res) {
    req.body.ip = res.sock.client.request.headers['x-real-ip'] || res.sock.client.request.connection.remoteAddress;
    req.body.userAgent = res.sock.client.request.headers['user-agent'];
    clientMngr.login(req.body)
      .then(result => {
        if (result.authType === '1fa') {
          const access = {
            role: constants.ROLE_IS_AUTH,
            type: result.authType,
            clientId: result.id,
            needUpdateToken: true
          };
          req.access = _.cloneDeep(access);
          delete result.id;
          delete result.password;
          delete result.key2fa;
          res.sock.join('userId=' + req.access.clientId);
          res.sock.join(constants.ROOMS.QUOTES);
          res.answer({status: 200, data: result});
        } else {
          const access = {
            role: constants.ROLE_IS_PART_AUTH,
            type: result.authType,
            clientId: result.id,
            needUpdateToken: true
          };
          req.access = _.cloneDeep(access);
          res.answer({status: 203, data: {success: true}});
        }
      })
      .catch(err => res.error(err));
  },

  login2FA(req, res) {
    req.body.ip = res.sock.client.request.connection.remoteAddress;
    req.body.userAgent = res.sock.client.request.headers['user-agent'];
    clientMngr.login2FA(req.body, req.access)
      .then(result => {
        const access = {
          role: constants.ROLE_IS_AUTH,
          type: result.authType,
          clientId: result.id,
          needUpdateToken: true
        };
        req.access = _.cloneDeep(access);

        delete result.id;
        delete result.password;
        delete result.key2fa;
        res.sock.join('userId=' + req.access.clientId);
        res.sock.join(constants.ROOMS.QUOTES);
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  verifyUser(req, res) {
    req.body.clientId = req.access.clientId;
    req.body.role = req.access.role;
    clientMngr.verifyUser(req.body)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  confirmVerification(req, res) {
    clientMngr.confirmVerification(req.body)
      .then(() => res.answer({status: 200, data: {code: 200, data: {success: true}}}))
      .catch(err => res.error(err));
  },

  getUsersDocs(req, res) {
    clientMngr.getUsersDocs(req.body)
      .then(result => res.answer({status: 200, data: {code: 200, data: result}}))
      .catch(err => res.error(err));
  },

  logout(req, res) {
    delete req.access;
    req.access = {};
    req.access.role = constants.ROLE_NOT_AUTH;
    req.access.needUpdateToken = true;
    res.answer({status: 200, data: {success: true}});
  },

  activate2FA(req, res) {
    clientMngr.activate2FA(req.body, req.access)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  confirmActivate2FA(req, res) {
    clientMngr.confirmActivate2FA(req.body, req.access)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => {
        res.error(err);
      });
  },

  deactivate2FA(req, res) {
    clientMngr.deactivate2FA(req.body, req.access)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  getAccountsBalanceCRM(req, res) {
    accountMngr.getBalanceByAccountId(req.body.login)
      .then(result => res.answer({status: 200, data: {data: result, code: 200}}))
      .catch(err => res.error(err));
  },

  getAccountBalance(req, res) {
    accountMngr.getAccountBalances(req.access)
      .then(result => {
        res.answer({
          status: 200, data: {
            balances: result
          }
        });
      })
      .catch(err => res.error(err));
  },

  getTotalBalance(req, res) {
    accountMngr.getTotalBalance(req.access)
      .then(result => {
        res.answer({
          status: 200, data: {
            balanceUSD: result.usd,
            balanceBTC: result.btc,
          }
        });
      })
      .catch(err => res.error(err));
  },

  getUsersDetailInfo(req, res) {
    clientMngr.getUsersDetailInfo(req.access.clientId)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => res.error(err));
  },

  getUserInfo(req, res) {
    req.body.clientId = req.access.clientId;
    clientMngr.getUserInfo(req.body)
      .then(result => {
        req.access = {};
        req.access.role = 1;
        req.access.clientId = result.id;
        req.access.needUpdateToken = true;
        delete result.id;
        res.sock.join('userId=' + req.access.clientId);
        res.sock.join(constants.ROOMS.QUOTES);
        res.answer({status: 200, data: result});
      })
      .catch(err => {
        res.error(err);
      });
  },

  getDayLimits(req, res) {
    clientMngr.getDayLimits(req.access)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => {
        res.error(err);
      });
  },

  updateDailyLimits(req, res) {
    clientMngr.updateDailyLimits(req.body)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => {
        res.error(err);
      });
  },

  subscribeToSymbol(req, res) {
    let symbol = req.body.symbol && config.symbols.indexOf(req.body.symbol) > -1 ? req.body.symbol : 'BTCUSD';
    res.sock.join(constants.ROOMS.ORDERBOOKS + ':' + symbol);
    res.sock.join(constants.ROOMS.ORDERS + ':' + symbol);
    res.sock.join(constants.ROOMS.TRADESHISTORY + ':' + symbol);
    if (req.access.clientId) {
      res.sock.join('userId=' + req.access.clientId + ':' + symbol);
    }
    res.answer({status: 200, data: {success: true}});
  },

  unsubscribeToSymbol(req, res) {
    let symbol = config.symbols.indexOf(req.body.symbol) > -1 ? req.body.symbol : 'BTCUSD';
    res.sock.leave(constants.ROOMS.ORDERBOOKS + ':' + symbol);
    res.sock.leave(constants.ROOMS.ORDERS + ':' + symbol);
    res.sock.leave(constants.ROOMS.TRADESHISTORY + ':' + symbol);
    if (req.access.clientId) {
      res.sock.leave('userId=' + req.access.clientId + ':' + symbol);
    }
    res.answer({status: 200, data: {success: true}});
  },

  resetPassword(req, res) {
    let p = path.resolve('./views/password.html');
    res.sendFile(p);
  },

  checkAllAccounts(req, res) {
    accountMngr.checkAllAccounts()
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  getPublicKeys(req, res) {
    accountMngr.getPublicKeys(req.query)
      .then(result => {
        res.answer({status: 200, data: result});
      })
      .catch(err => {
        res.error(err);
      });
  },

  getToken(req, res) {
    req.access.needUpdateToken = true;
    res.answer({status: 200, data: {}});
  },

  forgotPassword(req, res) {
    clientMngr.forgotPassword(req.body)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  resetPassword(req, res) {
    clientMngr.resetPassword(req.body)
      .then(() => {
        res.answer({status: 200, data: {success: true}});
      })
      .catch(err => res.error(err));
  },

  getReferrerId(req, res) {
    clientMngr.getReferrerId(req.access.clientId)
      .then(result => {
        res.answer({status: 200, data: {referrer: result}});
      })
      .catch(err => res.error(err));
  },

  checkReferrerId(req, res) {
    clientMngr.checkReferrerId(req.body.referrer)
      .then(result => {
        res.answer({status: 200, data: {permitted: !!result}});
      })
      .catch(err => res.error(err));
  }
};

module.exports = UserCtrl;
