module.exports = {

  VERIFICATION: {
    YES: 'verified',
    NOT: 'not verified',
    AWAITING: 'awaiting'
  },

  CRM: {
    VERIFICATION: {
      VERIFIED_NEW: 0,
      VERIFIED_PENDING: 1,
      VERIFIED_APPROVED: 2,
      VERIFIED_DECLINE: 3
    }
  },

  ORDER_TYPE: {
    BUY: 1,
    SELL: 2,
    LIMIT: 4,
    STOP: 8
  },

  WITHDRAWAL_LIMITS: {
    NOT_VERIFIED: 1000,
    VERIFIED: 20000
  },

  DEFAULT_CONFIRM_NUMBER: 100,

  // ************************    Transaction statuses   ******************************

  TRANSACTION_STATUSES: {
    DEPOSIT: {
      FAILED: 'failed',
      PENDING: 'pending',
      REJECTED: 'rejected',
      SUCCESS: 'success'
    },
    WITHDRAWAL: {
      APPROVED: 'approved',
      FAILED: 'failed',
      PENDING: 'pending',
      REJECTED: 'rejected',
      SUCCESS: 'success'
    }
  },

  CRM_STATUSES: {
    DEPOSIT: {
      PENDING: 1,
      PROCESSED: 2,
      DECLINED: 3,
      EXPIRED: 4
    },
    WITHDRAWAL: {
      PENDING: 0,
      PROCESSED: 1,
      APPROVED: 2,
      DECLINED: 3
    },
  },

  ROOMS: {
    ORDERBOOKS: 'orderBooks',
    TRADESHISTORY: 'tradesHistory',
    QUOTES: 'quotes',
    ORDERS: 'orders'
  },

  PAYMENT_METHOD: {
    BTC: 'BTC blockchain',
    DSH: 'DASH blockchain',
    ETH: 'ETH blockchain',
    LTC: 'LTC blockchain'
  },
  PAYMENT_SYSTEM: 'BLOCKCHAIN',

  // ************************    ORDER    ******************************

  ORDER: {
    DEFAULT_LIMIT: 19,
    MAX_LIMIT: 1000
  },

  // ************************    FEE    ******************************

  MIN_FEE_BLOCKCHAIN: {
    BAT: 0.1,
    BTC: 0.001,
    BZT: 0.1,
    CVC: 0.1,
    DSH: 0.001,
    ETA: 1,
    ETC: 0.00315,
    ETH: 0.00105,
    EOS: 0.1,
    GNO: 0.1,
    GNT: 0.1,
    LTC: 0.001,
    REP: 0.1,
    STJ: 0.1,
    XEM: 0.05,
    XRP: 0.01,
    ZRX: 0.1
  },
  WITHDRAWAL_FEE: {
    BAT: 0.1,
    BTC: 0.0004,
    BZT: 0.1,
    CVC: 0.1,
    DSH: 0.004,
    ETA: 2,
    ETC: 0.01,
    ETH: 0.005,
    EOS: 0.1,
    GNO: 0.1,
    GNT: 0.1,
    LTC: 0.004,
    REP: 0.1,
    STJ: 0.1,
    XEM: 0.25,
    XRP: 0.015,
    ZRX: 0.1
  },
  WITHDRAWAL_EXCESS_COEFFICIENT: 1.2,

  XRP_MIN_BALANCE: 20,

  GAS: {
    ETC: {
      PRICE_IN_WEI: 150000000000,
      LIMIT: 21000,
      LIMIT_CONTRACT: 55000
    },
    ETH: {
      PRICE_IN_WEI: 50000000000,
      LIMIT: 21000,
      LIMIT_CONTRACT: 55000
    }
  },

  // ************************    STATUS RESPONSE    ******************************
  STATUS_RESPONSE_OK: 200,
  STATUS_RESPONSE_INCORRECT_DATA: 400,
  STATUS_RESPONSE_NOT_AUTH: 401,
  STATUS_RESPONSE_FORBIDDEN: 403,
  STATUS_RESPONSE_NOT_FOUND: 404,
  STATUS_RESPONSE_UNPROCESSABLE_ENTITY: 422,
  STATUS_RESPONSE_INTERNAL_SERVER: 500,

  // ************************    ROLES OF USERS    ******************************
  ROLE_NOT_AUTH: 0,
  ROLE_IS_AUTH: 1,
  ROLE_IS_ADMIN: 2,
  ROLE_IS_CRM: 3,
  ROLE_IS_PART_AUTH: 4,
  ROLE_IS_SYSTEM: 5,
  ROLE_IS_BOT: 6,
  ROLE_IS_PLATFORM: 7,

  // ***************************    ERROR CODES    ******************************
  COMMON_CODE: 1000,
  RESPONSE_SERVER_DOESNT_ANSWER: 1001,
  RESPONSE_FIELD_IS_NOT_FILLED: 1002,
  RESPONSE_FIELD_HAS_INCORRECT_TYPE: 1003,
  RESPONSE_FIELD_VALUE_OUT_OF_RANGE: 1004,
  RESPONSE_NOT_FOUND: 1005,
  RESPONSE_ERR_RETHINKDB: 1006,
  RESPONSE_WRONG_PASSWORD: 1007,
  RESPONSE_INSUFFICIENT_FUNDS: 1008,
  RESPONSE_ERROR_FOREX: 1009,
  RESPONSE_ERR_REDIS: 1010,
  RESPONSE_EMAIL_ALREADY_EXISTS: 1011,
  RESPONSE_FORBIDDEN: 3000,
  RESPONSE_NOT_AUTH: 3001,
  RESPONSE_ERR_CRM: 1013
};
