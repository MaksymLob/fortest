
const _ = require('lodash');
const config = require(__dirroot + 'config');
const constants = __app('libs/constants');
const cryptoCurrencies = __app('libs/cryptoDefinitions/names');
const scheduler = __app('services/scheduler');

const accountModel = __app('models/accountModel');
const clientModel = __app('models/clientModel');
const accountKeyModel = __app('models/accountKeyModel');
const depositModel = __app('models/depositModel');
const orderModel = __app('models/orderModel');
const withdrawalModel = __app('models/withdrawalModel');

const CryptoOrder = __app('services/exchange/cryptoOrders');
const cryptoOrder = new CryptoOrder();
const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();
const crm = __app('services/crm');

const AccountManager = {
  createAccounts(data) {
    return accountModel.validateData(data)
      .then(res => {
        const currencies = Object.keys(config.currencies);
        const actions = currencies.map(currency => {
          let balance = data.role == constants.ROLE_IS_SYSTEM || data.role == constants.ROLE_IS_BOT
            ? config.systemBalance : config.currencies[currency].initialBalance;
          return this.createCurrencyAccount(_.cloneDeep(res), currency, balance);
        });
        return Promise.all(actions);
      });
  },

  createCurrencyAccount(data, currency, balance) {
    let accountId;
    data.currency = currency;
    data.balance = balance;
    return accountKeyModel.create(data)
      .then(res => {
        accountId = res.get('id');
        return accountModel.createR({
          currency: data.currency,
          clientId: data.clientId,
          balance: data.balance,
          used: 0
        });
      })
      .then(res => {
        cryptoOrder.addAccount({clientId: res.clientId, balance: res.balance, currency: res.currency});
        return {balance: res.balance, currency: res.currency, accountId: accountId, role: data.role};
      });
  },

  getBalance(data) {
    return accountModel.findByIdR(data.currency, data.clientId)
      .then(res => {
        if (data.currency === 'BTC') {
          return {
            balance: res.balance,
            currency: res.currency,
            accountId: data.accountId
          };
        } else {
          return {
            balance: res.balance,
            btcValue: Number(res.balance) * Number(quotes.getCurrentQuote({symbol: `${data.currency}BTC`}).currentPrice),
            currency: res.currency,
            accountId: data.accountId
          };
        }
      });
  },

  getTotalBalance(data) {
    let balances;
    let usd = 0;
    let btc = 0;
    return accountKeyModel.getAll({where: {clientId: data.clientId}})
      .then(res => {
        balances = res.map(item => {
          return {balance: item.balance, currency: item.currency};
        });
        balances = balances.map(this._getCurrencyBalanceInBTC);
        balances.forEach(item => {
          usd += item.usd;
          btc += item.btc;
        });
        return {
          usd: usd,
          btc: btc
        };
      });
  },

  _getCurrencyBalanceInBTC(data) {
    if (data.currency === 'BTC') {
      data.btc = Number(data.balance);
      data.usd = Number(data.balance) * Number(quotes.getCurrentQuote({symbol: 'BTCUSD'}).currentPrice);
    } else if (data.currency === 'USD') {
      let quote = Number(quotes.getCurrentQuote({symbol: 'BTCUSD'}).currentPrice);
      data.btc = quote ? Number(data.balance) / quote : 0;
      data.usd = Number(data.balance);
    } else {
      data.btc = Number(data.balance) * Number(quotes.getCurrentQuote({symbol: `${data.currency}BTC`}).currentPrice);
      data.usd = Number(data.balance) * Number(quotes.getCurrentQuote({symbol: `${data.currency}USD`}).currentPrice);
    }
    return data;
  },

  getBalanceByAccountId(accountId) {
    return accountKeyModel.findById(accountId)
      .then(res => {
        if (!res) {
          return Promise.reject('NOT_FOUND_ACCOUNT');
        }
        return this.getBalance({currency: res.get('currency'), clientId: res.get('clientId'), accountId: accountId});
      });
  },

  getAccountBalances(data) {
    const currencies = Object.keys(config.currencies);
    let balances;
    return accountKeyModel.getAll({where: {clientId: data.clientId, currency: {$or: currencies}}})
      .then(res => {
        balances = res.map(item => {
          return {balance: item.balance, currency: item.currency};
        });
        balances = balances.map(this._getCurrencyBalanceInBTC);

        const actions = currencies.map(item => accountModel.findByIdR(item, data.clientId));
        return Promise.all(actions);
      })
      .then(res => {
        let response = [];
        balances.forEach(item => {
          let onOrder = res.filter(item2 => item2.currency === item.currency);
          response.push({
            coin: item.currency,
            name: typeof cryptoCurrencies[item.currency] === 'undefined' ? item.currency : cryptoCurrencies[item.currency],
            totalBalance: item.balance,
            onOrders: Number(onOrder[0].used),
            btcValue: item.btc
          });
        });
        return response;
      });
  },

  synchronizeBalance(data) {
    let actions = data.map(item => {
      return accountKeyModel.find({where: {clientId: item.clientId, currency: item.currency}})
        .then(res => {
          let balance = Number((res.get('balance') + item.amount).toFixed(8));
          return accountKeyModel.update({balance: balance}, {where: {clientId: item.clientId, currency: item.currency}});
        });
      // return accountKeyModel.update(
      //   {balance: sequelize.literal(`balance + ${item.amount}`)},
      //   {where: {clientId: item.clientId, currency: item.currency}}
      // )
    });
    return Promise.all(actions).catch(err => {
      log.e('Error with updating balance: ', err);
    });
  },

  checkAllAccounts() {
    const currencies = Object.keys(config.currencies);
    return clientModel.getAll({})
      .then(clients => {
        const loop = i => {
          if (clients[i].active) {
            Promise.all(currencies.map(item => {
              return accountKeyModel.getAll({where: {clientId: clients[i].id, currency: item}})
                .then(res => {
                  let account;
                  if (res.length === 0) {
                    account = {
                      clientId: clients[i].id,
                      email: clients[i].email,
                      phone: clients[i].phone,
                      currency: item,
                      balance: config.currencies[item].initialBalance,
                      role: clients[i].role
                    };
                    return this.createCurrencyAccount(account, item, account.balance)
                      .then(res => {
                        _.assign(res, account);
                        return this._sendToCrm([res], 0);
                      });
                  } else if (res.length === 1) {
                    return Promise.resolve();
                  } else {
                    let ids = [];
                    for (let num = 1; num < res.length; num++) {
                      ids.push(res[num].id);
                    }
                    if (ids.length > 0) {
                      return accountKeyModel.destroy({where: {id: {$or: ids}}});
                    }
                  }
                })
                .catch(err => {
                  log.e('checkAllAccounts error: ', err);
                });
            }))
              .then(() => i === clients.length - 1 || loop(i + 1));
          } else {
            accountKeyModel.getAll({where: {clientId: clients[i].id, currency: {$or: currencies}}})
              .then(accounts => {
                if (accounts.length > 0) {
                  const ids = accounts.map(item => {
                    return item.id;
                  });
                  return accountKeyModel.destroy({where: {id: {$or: ids}}});
                }
              })
              .then(() => i === clients.length - 1 || loop(i + 1));
          }
        };
        return loop(0);
      });
  },

  createRedisAccountsFromMysql() {
    let mysqlAccounts;
    return accountKeyModel.getAll()
      .then(res => {
        mysqlAccounts = res;
        const actions = res.map(item => {
          return this._checkAccountExistence(item);
        });
        return Promise.all(actions);
      })
      .then(() => {
        const actions = mysqlAccounts.map(item => {
          return this._checkAccountBalance(item);
        });
        return Promise.all(actions);
      })
      .catch(err => {
        log.e('Failed createRedisAccountsFromMysql:', err);
        return Promise.reject();
      });
  },

  _checkAccountExistence(data) {
    const appBalance = cryptoOrder._getBalanceAndUsed(data.clientId, data.currency);
    return accountModel.findByIdR(data.currency, data.clientId)
      .then(res => {
        if (appBalance.balance === null) {
          cryptoOrder.addAccount({
            clientId: data.clientId,
            balance: data.balance,
            currency: data.currency
          });
        }
        if (res === null) {
          return accountModel.createR({
            currency: data.currency,
            clientId: data.clientId,
            balance: data.balance,
            used: 0
          });
        }
      })
      .catch(err => {
        log.e('Failed create account', err);
      });
  },

  _checkAccountBalance(data) {
    const appBalance = cryptoOrder._getBalanceAndUsed(data.clientId, data.currency);
    const initBalance = config.currencies[data.currency].initialBalance;
    return Promise.all([
      accountModel.findByIdR(data.currency, data.clientId),
      withdrawalModel.findAll({where: {clientId: data.clientId, currency: data.currency}}),
      depositModel.findAll({where: {clientId: data.clientId, currency: data.currency}}),
      orderModel.findAll({where: {clientId: data.clientId, symbol: {'$like': `%${data.currency}%`}}}),
      orderModel.findAllR({where: {clientId: data.clientId, status: 'open'}})
    ])
      .then(res => {
        const redisBalance = Number(res[0].balance);
        const redisUsed = Number(res[0].used);
        const mysqlBalance = Number(data.balance);
        const amounts = this._calculateAmounts(res[1], res[2], res[3], res[4], data.currency);
        const finalAMount = initBalance + amounts.depositsAmount - amounts.withdrawalsAmount + amounts.buyAmount - amounts.sellAmount;
        if (finalAMount !== appBalance.balance || amounts.onOrders !== appBalance.used) {
          cryptoOrder.balanceAdjustment({clientId: data.clientId, currency: data.currency, newBalance: finalAMount, newUsed: amounts.onOrders});
        }
        if (finalAMount !== mysqlBalance && (finalAMount !== redisBalance || amounts.onOrders !== redisUsed)) {
          return accountKeyModel.update({balance: finalAMount}, {where: {clientId: data.clientId, currency: data.currency}})
            .then(() => {
              return accountModel.updateR({
                balance: finalAMount,
                used: amounts.onOrders
              }, {where: {clientId: data.clientId, currency: data.currency}});
            });
        } else if (finalAMount !== mysqlBalance && finalAMount === redisBalance && amounts.onOrders === redisUsed) {
          return accountKeyModel.update({balance: finalAMount}, {where: {clientId: data.clientId, currency: data.currency}});
        } else if (finalAMount === mysqlBalance && (finalAMount !== redisBalance || amounts.onOrders !== redisUsed)) {
          return accountModel.updateR({
            balance: finalAMount,
            used: amounts.onOrders
          }, {where: {clientId: data.clientId, currency: data.currency}});
        }
      })
      .catch(err => {
        log.e('Failed check account\'s balance', err);
      });
  },

  _calculateAmounts(withdrawals, deposits, closedOrders, openOrders, currency) {
    let withdrawalsAmount = 0;
    let depositsAmount = 0;
    let buyAmount = 0;
    let sellAmount = 0;
    let onOrders = 0;
    withdrawals.forEach(item => {
      if (item.get('status') === constants.TRANSACTION_STATUSES.WITHDRAWAL.SUCCESS) {
        withdrawalsAmount += item.get('amount');
      } else if (item.get('status') === constants.TRANSACTION_STATUSES.WITHDRAWAL.PENDING) {
        onOrders += item.get('amount');
      }
    });
    deposits.forEach(item => {
      if (item.get('status') === constants.TRANSACTION_STATUSES.DEPOSIT.SUCCESS) {
        depositsAmount += item.get('amount');
      }
    });
    closedOrders.forEach(item => {
      const value = item.get();
      if (value.symbol.indexOf(currency) === 0) {
        if (value.type === 'buy') {
          buyAmount += value.amount;
        } else {
          sellAmount += value.amount;
        }
      } else {
        if (value.type === 'buy') {
          sellAmount += value.amount * value.price;
        } else {
          buyAmount += value.amount * value.price;
        }
      }
    });
    openOrders.forEach(item => {
      if (item.currency === currency) {
        if (item.type === 'sell') {
          onOrders += Number(item.amount);
        } else {
          onOrders += Number(item.price) * Number(item.amount);
        }
      }
    });
    return {withdrawalsAmount, depositsAmount, buyAmount, sellAmount, onOrders};
  },

  _sendToCrm(accounts, index) {
    let account = {
      login: accounts[index].accountId,
      email: accounts[index].email,
      currency: accounts[index].currency,
      country: accounts[index].country,
      phone: accounts[index].phone,
      source: accounts[index].role
    };
    return crm.getClientInfo({email: accounts[index].email})
      .then(res => {
        return crm.sendAddOrUpdateClient(_.assign(res, {login: account.login, currency: account.currency}));
      })
      .then(() => {
        if (index === accounts.length - 1) return;
        return this._sendToCrm(accounts, index + 1);
      })
      .catch(err => {
        if (err.status === 404 && err.statusText === 'Client not found') {
          return crm.sendAddOrUpdateClient(account)
            .then(() => {
              if (index === accounts.length - 1) return;
              return this._sendToCrm(accounts, index + 1);
            });
        }
        if (index === accounts.length - 1) return;
        return this._sendToCrm(accounts, index + 1);
      });
  },

  updateAccountsPrivateKeys() {
    return accountKeyModel.findAll({where: {privateKey: {$ne: null}}})
      .then(res => {
        let data = [];
        res.forEach(item => {
          data.push({
            privateKey: item.get('privateKey'),
            clientId: item.get('clientId'),
            id: item.get('id')
          });
        });
        return scheduler.hashPrivateKeys(data);
      })
      .then(res => {
        const loop = i => {
          accountKeyModel.update({privateKey: res[i].privateKey}, {where: {id: res[i].id}})
            .then(() => i === res.length - 1 || loop(i + 1));
        };
        return loop(0);
      });
  },

  getPublicKeys(data) {
    if (!data.currency) {
      return Promise.reject('VALIDATION_CURRENCY_IS_NOT_FILLED');
    }
    if (data.currency === 'ETA') {
      return accountKeyModel.getAll({where: {currency: data.currency, address: {$ne: null}}})
        .then(res => {
          for (let i = 0; i < res.length; i++) {
            res[i].publicKey = res[i].address;
          }
          return res;
        });
    }
    return accountKeyModel.getAll({where: {currency: data.currency, publicKey: {$ne: null}}});
  }
};

module.exports = AccountManager;
