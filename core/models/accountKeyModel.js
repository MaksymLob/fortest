
const AccountModel = {
  classMethods: {}
};

const accountSchema = __app('models/schemas/accountKey');
const Model = __app('models/model');
const schema = new Model(accountSchema.schema);
AccountModel.validations = accountSchema.validations;
const accountModel = schema.defineDataBase('mysql', 'accounts', AccountModel);

module.exports = accountModel;
