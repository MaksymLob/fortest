
module.exports = {
  /* **************************  Common controller  *************************/

  'POST/v1.0/crm/register': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'addNewUserFromCrm',

    name: 'Add new user from crm',
    group: 'CRM',
    description: 'Add new user from crm',
    params: {
      country: {
        type: 'String',
        description: 'User Country'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      },
      address: {
        type: 'string',
        description: 'User Address'
      },
      city: {
        type: 'string',
        description: 'User City'
      },
      state: {
        type: 'string',
        description: 'User State'
      },
      postalCode: {
        type: 'string',
        description: 'User PostalCode'
      },
      phone: {
        type: 'integer',
        description: 'User Phone'
      },
      birthday: {
        type: 'string',
        description: 'User Birthday'
      },
      passportId: {
        type: 'string',
        description: 'User PassportId'
      },
      photoPassport: {
        type: 'string',
        description: 'User Photo Passport'
      },
      photoVerification: {
        type: 'string',
        description: 'User Photo Verification'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/user/update': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'updateUserFromCrm',

    name: 'Update user from crm',
    group: 'CRM',
    description: 'Update user from crm',
    params: {
      email: {
        type: 'String',
        description: 'Email client'
      },
      country: {
        type: 'String',
        description: 'User Country'
      },
      code: {
        type: 'String',
        description: 'VALIDATION_2FA_CODE'
      },
      address: {
        type: 'string',
        description: 'User Address'
      },
      city: {
        type: 'string',
        description: 'User City'
      },
      state: {
        type: 'string',
        description: 'User State'
      },
      postalCode: {
        type: 'string',
        description: 'User PostalCode'
      },
      phone: {
        type: 'integer',
        description: 'User Phone'
      },
      birthday: {
        type: 'string',
        description: 'User Birthday'
      },
      passportId: {
        type: 'string',
        description: 'User PassportId'
      },
      photoPassport: {
        type: 'string',
        description: 'User Photo Passport'
      },
      photoVerification: {
        type: 'string',
        description: 'User Photo Verification'
      },
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/user/balance': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getAccountsBalanceCRM',

    name: 'Get balance of user',
    group: 'CRM',
    description: 'Get balance of user',
    params: {
      login: {
        type: 'String',
        description: 'login id of account'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/user/verified': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'confirmVerification',

    name: 'Verify or decline user',
    group: 'CRM',
    description: 'Verify or decline user',
    params: {
      success: {
        type: 'String',
        description: '0 - reject users verification, 1 - approved'
      },
      email: {
        type: 'String',
        description: 'Email client'
      },
      reason: {
        type: 'String',
        description: 'if rejected'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/user/docs': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'usersController',
    action: 'getUsersDocs',

    name: 'Get documents for specific user(for crm)',
    group: 'CRM',
    description: 'Get documents for specific user(for crm)',
    params: {
      email: {
        type: 'String',
        description: 'Email client'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/orders': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'ordersController',
    action: 'getAllClosedOrders',

    name: 'Get all closed orders(for crm)',
    group: 'CRM',
    description: 'Get all closed orders(for crm)',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/withdrawals/approve': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'transactionsController',
    action: 'approveWithdrawal',

    name: 'Approve withdrawal(for crm)',
    group: 'CRM',
    description: 'Approve withdrawal(for crm)',
    params: {
      id: {
        type: 'String',
        description: 'id  of withdrawal'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/currencies': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'resourcesController',
    action: 'getCurrencies',

    name: 'Get list of all platforms currencies',
    group: 'CRM',
    description: 'Get list of all platforms currencies',
    params: {},
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  },

  'POST/v1.0/crm/email/send': {
    access: ['isCrm'],
    tokenRequired: true,
    controllerName: 'resourcesController',
    action: 'sendEmail',

    name: 'Send email to user through platform',
    group: 'CRM',
    description: 'Send email to user through platform',
    params: {
      text: {
        type: 'String',
        description: 'text for client'
      },
      to: {
        type: 'String',
        description: 'Email to send'
      },
      file: {
        type: 'String',
        description: 'File to send'
      },
      subject: {
        type: 'String',
        description: 'Subject email'
      }
    },
    success: {
      'status': {
        type: 'Number',
        description: '200'
      },
      'data': {
        type: 'Object',
      }
    },
    apiuse: ['defErrorResp']
  }
};
