
const extend = require('util')._extend;
const fs = require('fs');
const docs = __app('libs/apidoc');
const path = __app('libs/path');
const config = require(__dirroot + 'config');

const routesDir = path.locate('routes/routeLists/');

const items = fs.readdirSync(routesDir);
if (!items.length) {
  return log.e('CAN`T READ routes', items);
}

let routes = {};
items.forEach(item => {
  extend(routes, require(routesDir + item));
});

if (config.docs) {
  docs(routes);
}

module.exports = routes;
