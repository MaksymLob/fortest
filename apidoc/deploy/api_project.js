define({
  "name": "CryptoPlatform",
  "version": "1.0.0",
  "description": "This API is for socket.io methods",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
