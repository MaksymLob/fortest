
const config = require(__dirroot+'config');
const Quotes = __app('services/exchange/quotes');
const quotes = new Quotes();

const OrderManager = {
  getQuotesHistory(data) {
    data.from = (data.from * 1000);
    data.to = (data.to * 1000);

    data.resolution = (data.resolution=='D' ? '1D' : data.resolution); //TODO: temp fix for values from FRONTEND
    data.frame = (config.chart.resolutions[data.resolution] || 300);
    return quotes.getHistory(data)
      .then(res => {
        let result = {
          c: [],
          h: [],
          l: [],
          o: [],
          t: [],
          v: [],
          s: 'ok'
        };
        this._convertCandleFormat(result, res, data);
        return result;
      });
  },

  _convertCandleFormat(result, list, data) {
    // log.d('_convertCandleFormat', data);
    for (let candle of list) {
      candle = candle.split(',');
      result.c.push(Number(candle[2]));
      result.h.push(Number(candle[4]));
      result.l.push(Number(candle[3]));
      result.o.push(Number(candle[1]));
      result.v.push(Number(candle[6]));
      result.t.push(Math.floor(candle[0] / 1000 / data.frame) * data.frame);
    }
    return result;
  }
};

module.exports = OrderManager;
