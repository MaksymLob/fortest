
const config = require(__dirroot+'config.json');
const constants = __app('libs/constants');
const currencies = Object.keys(config.currencies);
currencies.splice(currencies.indexOf('USD'), 1);

let transactionStatuses = [];
Object.keys(constants.TRANSACTION_STATUSES.WITHDRAWAL).forEach(item => {
  transactionStatuses.push(constants.TRANSACTION_STATUSES.WITHDRAWAL[item]);
});

const withdrawal = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  clientId: {
    type: 'integer',
    allowNull: false
  },
  currency: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [currencies]}
  },
  address: {
    type: 'string',
    allowNull: false
  },
  amount: {
    type: 'double',
    allowNull: false
  },
  fee: {
    type: 'double',
    allowNull: false,
    defaultValue: 0.0001
  },
  status: {
    type: 'string',
    allowNull: false,
    defaultValue: constants.TRANSACTION_STATUSES.WITHDRAWAL.PENDING,
    validate: {isIn: [transactionStatuses]}
  },
  txId: {
    type: 'string',
    allowNull: true
  },
  timeCreation: {
    type: 'integer',
    allowNull: false
  },
  timeConfirmation: {
    type: 'integer',
    allowNull: true
  }
};

module.exports = {
  schema: withdrawal
};
