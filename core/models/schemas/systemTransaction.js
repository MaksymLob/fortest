
const config = require(__dirroot+'config.json');
const currencies = Object.keys(config.currencies);
currencies.splice(currencies.indexOf('USD'), 1);

const transaction = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  currency: {
    type: 'string',
    allowNull: false,
    validate: {isIn: [currencies]}
  },
  amount: {
    type: 'double',
    allowNull: false
  },
  fee: {
    type: 'double',
    allowNull: false,
    defaultValue: 0.0001
  },
  status: {
    type: 'string',
    allowNull: false,
    defaultValue: 'waiting'
  },
  txId: {
    type: 'string',
    allowNull: true,
    unique: true
  },
  fromAddress: {
    type: 'string',
    allowNull: false
  },
  timeCreated: {
    type: 'integer',
    defaultValue: Date.now()
  },
  timeConfirmed: {
    type: 'integer',
    allowNull: true
  }
};

module.exports = {
  schema: transaction
};
