const codes = __app('libs/errorCodes');

class AppError extends Error {
  constructor(errorName, additionalInfo) {
    super();

    if (!codes[errorName]) log.e('', errorName);

    let error = codes[errorName] ? codes[errorName] : codes['UNKNOWN_ERROR'];

    this.status = error.status;
    this.errorCode = error.errorCode;
    this.message = additionalInfo ? error.errorMessage + '  ' + additionalInfo : error.errorMessage;

    Error.captureStackTrace(this, AppError);
  }
}

module.exports = AppError;
