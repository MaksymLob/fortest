
const system = {
  id: {
    type: 'integer',
    autoIncrement: true,
    primaryKey: true
  },
  balanceBTC: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  },
  balanceLTC: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  },
  balanceUSD: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  },
  balanceETH: {
    type: 'double',
    allowNull: false,
    defaultValue: 0
  }
};

module.exports = {
  schema: system
};
